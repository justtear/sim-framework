package com.yao2san.sim.framework.cache.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
@ConditionalOnProperty(prefix = "spring.cache", name = "type", havingValue = "redis",matchIfMissing = true)
public class RedisCacheConfig {
    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
        LettuceConnectionFactory factory = new LettuceConnectionFactory();
        return factory;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory factory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);
        StringRedisSerializer ks = new StringRedisSerializer();
        GenericJackson2JsonRedisSerializer vs = new GenericJackson2JsonRedisSerializer();
        redisTemplate.setKeySerializer(ks);
        redisTemplate.setValueSerializer(vs);
        redisTemplate.setHashKeySerializer(ks);
        redisTemplate.setHashValueSerializer(vs);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }
}
