package com.yao2san.sim.framework.cache.reatelimit;

import com.yao2san.sim.framework.cache.exception.TooManyRequestsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class DefaultFallbackPolicy implements FallbackPolicy {
    @Override
    public Object fallback(String key) throws IOException {
        log.warn("Too many requests of key:" + key);
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            HttpServletResponse response = servletRequestAttributes.getResponse();
            if (response != null) {
                response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
                response.sendError(429, "Too many requests");
            }
        }
        return null;
    }
}
