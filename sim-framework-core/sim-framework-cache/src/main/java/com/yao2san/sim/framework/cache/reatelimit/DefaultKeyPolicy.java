package com.yao2san.sim.framework.cache.reatelimit;

import org.aspectj.lang.JoinPoint;


public class DefaultKeyPolicy implements KeyPolicy {
    @Override
    public String getKey(JoinPoint joinPoint) {
        String typeName = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        return typeName + "." + methodName;
    }

    @Override
    public String getName() {
        return "default";
    }
}
