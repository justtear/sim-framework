package com.yao2san.sim.framework.cache.reatelimit;

import org.aspectj.lang.JoinPoint;

public interface KeyPolicy {
    String getKey(JoinPoint joinPoint);
    String getName();
}
