package com.yao2san.sim.framework.cache.cache;

public interface Subscriber {
    void handleMessage(String message);

    String getTopic();
}
