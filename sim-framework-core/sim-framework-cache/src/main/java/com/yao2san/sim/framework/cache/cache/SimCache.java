package com.yao2san.sim.framework.cache.cache;

import java.util.concurrent.TimeUnit;

public interface SimCache {
    <T> T get(String key);

    <T> T hget(String key, String hashKey);

    void set(String key, Object value);

    void set(String key, Object value, long exp, TimeUnit timeUnit);

    void hset(String key,String hashKey, Object value);

    Boolean delete(String key);

    void publish(String channel, Object message);

    void subscribe(Subscriber subscriber);


}
