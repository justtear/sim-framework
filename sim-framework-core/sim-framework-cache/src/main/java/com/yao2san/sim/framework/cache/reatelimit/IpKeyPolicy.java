package com.yao2san.sim.framework.cache.reatelimit;

import com.yao2san.sim.framework.cache.utils.IpUtil;
import org.aspectj.lang.JoinPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class IpKeyPolicy implements KeyPolicy {
    @Override
    public String getKey(JoinPoint joinPoint) {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            HttpServletRequest request = servletRequestAttributes.getRequest();
            return IpUtil.getClientIp(request);
        }
        throw new RuntimeException("The current environment is not a web environment");
    }

    @Override
    public String getName() {
        return "ip";
    }
}
