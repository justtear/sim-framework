package com.yao2san.sim.framework.cache.utils;

import com.yao2san.sim.framework.cache.cache.SimCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class CacheUtil {


    private static SimCache simCache;

    @Autowired
    public void setSimCache(SimCache simCache) {
        CacheUtil.simCache = simCache;
    }

    public static <T> T get(String key) {
        return simCache.get(key);
    }

    public static <T> T hget(String key, String hashKey) {
        return simCache.hget(key, hashKey);
    }

    public static void set(String key, Object value) {
        simCache.set(key, value);
    }

    public static void hset(String key, String hashKey, Object value) {
        simCache.hset(key, hashKey, value);
    }

    public static void set(String key, Object value, long exp, TimeUnit timeUnit) {
        simCache.set(key, value, exp, timeUnit);
    }

    public static Boolean delete(String key) {
        return simCache.delete(key);
    }
}
