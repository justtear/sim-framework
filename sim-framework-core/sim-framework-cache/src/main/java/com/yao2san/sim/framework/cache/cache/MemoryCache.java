package com.yao2san.sim.framework.cache.cache;

import com.yao2san.sim.framework.cache.config.MemoryCacheProperties;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by wxg on 2019/2/1 14:04
 */
public class MemoryCache implements SimCache {
    private static final Map<String, Object> CACHE_MAP = new ConcurrentHashMap<>();
    private static final Map<String, Long> TIME_MAP = new ConcurrentHashMap<>();
    private static final AtomicLong MOD_COUNT = new AtomicLong(0);

    private final MemoryCacheProperties properties;


    public MemoryCache(MemoryCacheProperties properties) {
        this.properties = properties;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        T obj = (T) CACHE_MAP.get(key);
        if (isTimeout(key)) {
            remove(key);
            return null;
        }
        MOD_COUNT.incrementAndGet();
        if (MOD_COUNT.get() >= properties.getCleanThreshold()) {
            clean(true);
        }
        return obj;
    }

    @Override
    public <T> T hget(String key, String hashKey) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void set(String key, Object value) {
        set(key, value, -1, null);
    }

    @Override
    public void set(String key, Object value, long exp, TimeUnit timeUnit) {
        if (exp == -1L) {
            TIME_MAP.put(key, -1L);
        } else {
            TIME_MAP.put(key, System.nanoTime() + timeUnit.toNanos(exp));
        }
        MOD_COUNT.incrementAndGet();
        CACHE_MAP.put(key, value);
    }

    @Override
    public void hset(String key, String hashKey, Object value) {
        throw new UnsupportedOperationException();
    }


    @Override
    public Boolean delete(String key) {
        remove(key);
        return true;
    }

    @Override
    public void publish(String channel, Object message) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        throw new UnsupportedOperationException();
    }


    public boolean isTimeout(String key) {
        long nowTime = System.nanoTime();
        long maxTime = TIME_MAP.get(key) == null ? Long.MAX_VALUE : TIME_MAP.get(key);
        return maxTime != -1L && nowTime > maxTime;
    }

    public Object remove(String key) {
        TIME_MAP.remove(key);
        MOD_COUNT.incrementAndGet();
        return CACHE_MAP.remove(key);
    }

    public void clean(boolean keep) {
        if (keep) {
            for (String key : CACHE_MAP.keySet()) {
                if (isTimeout(key)) {
                    CACHE_MAP.remove(key);
                    TIME_MAP.remove(key);
                }
            }
        } else {
            CACHE_MAP.clear();
            TIME_MAP.clear();
        }
        MOD_COUNT.set(0L);
    }

    public synchronized long increment(String key) {
        Long old = get(key);
        long v;
        if (old == null) {
            v = 1L;
        } else {
            v = old + 1;
        }
        set(key, v);
        return v;
    }
}
