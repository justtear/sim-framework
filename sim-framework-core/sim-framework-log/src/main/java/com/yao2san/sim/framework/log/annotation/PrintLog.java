package com.yao2san.sim.framework.log.annotation;

import com.yao2san.sim.framework.log.core.LogAppender;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PrintLog {
    String value() default "";

    boolean useTimer() default false;

    Class<LogAppender> appender() default LogAppender.class;
}
