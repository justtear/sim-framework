package com.yao2san.sim.framework.log.aspect;

import com.yao2san.sim.framework.log.annotation.PrintLog;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;


@Aspect
@Slf4j
public class PrintLogAspect {
    @Value("${spring.application.name:'application'}")
    private String appName;

    @Pointcut("@annotation(com.yao2san.sim.framework.log.annotation.PrintLog)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        Signature signature = joinPoint.getSignature();
        Method method = ((MethodSignature) signature).getMethod();
        PrintLog annotation = method.getAnnotation(PrintLog.class);
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();

        log.info("name={},url={},method={},ip={},class_method={},args={}",
                annotation.value(),
                request.getRequestURI(),
                request.getMethod(),
                request.getRemoteAddr(),
                signature.getDeclaringTypeName() + "#" + signature.getName(),
                joinPoint.getArgs());
        Long start = System.currentTimeMillis();

        Object proceed;
        proceed = joinPoint.proceed();

        Long end = System.currentTimeMillis();

        if (annotation.useTimer()) {
            log.info("name={},time={}ms", annotation.value(), (end - start));
        }

        return proceed;
    }
}
