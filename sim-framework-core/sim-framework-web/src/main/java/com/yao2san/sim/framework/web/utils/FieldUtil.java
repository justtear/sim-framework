package com.yao2san.sim.framework.web.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FieldUtil {
    public static List<Field> getFields(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        getFields(clazz, fields);
        return fields;
    }

    private static void getFields(Class<?> clazz, List<Field> fields) {
        if (clazz != null) {
            if (clazz != Object.class) {
                getFields(clazz.getSuperclass(), fields);
            }
            Collections.addAll(fields, clazz.getDeclaredFields());
        }
    }

}
