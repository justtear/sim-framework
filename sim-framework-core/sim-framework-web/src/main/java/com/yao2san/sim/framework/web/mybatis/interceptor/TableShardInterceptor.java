package com.yao2san.sim.framework.web.mybatis.interceptor;

import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.mysql.parser.MySqlStatementParser;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.alibaba.druid.stat.TableStat;
import com.yao2san.sim.framework.web.mybatis.sharding.ShardingConfigProperties;
import com.yao2san.sim.framework.web.mybatis.sharding.ShardingStrategy;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.ReflectorFactory;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
@ConditionalOnProperty(prefix = "sim.database.table-sharding", name = "enable", havingValue = "true")
@Slf4j
public class TableShardInterceptor implements Interceptor {

    private static final ReflectorFactory DEFAULT_REFLECTOR_FACTORY = new DefaultReflectorFactory();

    private static final Map<String, ShardingStrategy> TABLE_SHARDING_STRATEGY = new HashMap<>();

    private static final String BOUND_SQL_KEY = "delegate.boundSql.sql";
    private final ShardingConfigProperties properties;

    public TableShardInterceptor(ShardingConfigProperties properties) throws InstantiationException, IllegalAccessException {
        this.properties = properties;
        this.convert();
    }

    private void convert() throws InstantiationException, IllegalAccessException {
        List<ShardingConfigProperties.ShardingTableStrategy> mapping = properties.getMapping();
        if (mapping == null) {
            return;
        }
        for (ShardingConfigProperties.ShardingTableStrategy strategy : mapping) {
            List<String> tables = strategy.getTables();
            ShardingStrategy s = strategy.getStrategy().newInstance();
            for (String table : tables) {
                TABLE_SHARDING_STRATEGY.put(table, s);
            }
        }
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        if (TABLE_SHARDING_STRATEGY.isEmpty()) {
            return invocation.proceed();
        }
        log.debug("starting sharding table");
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
        MetaObject metaObject = MetaObject.forObject(statementHandler,
                SystemMetaObject.DEFAULT_OBJECT_FACTORY,
                SystemMetaObject.DEFAULT_OBJECT_WRAPPER_FACTORY,
                DEFAULT_REFLECTOR_FACTORY
        );

        String sql = (String) metaObject.getValue(BOUND_SQL_KEY);
        log.debug("original sql:{}", sql);

        MySqlStatementParser parser = new MySqlStatementParser(sql);
        SQLStatement statement = parser.parseStatement();
        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
        statement.accept(visitor);
        Map<TableStat.Name, TableStat> tables = visitor.getTables();
        Set<TableStat.Name> names = tables.keySet();
        for (TableStat.Name name : names) {
            Set<String> shardingTableNames = TABLE_SHARDING_STRATEGY.keySet();
            String tableName = name.getName();
            if (shardingTableNames.contains(tableName)) {
                ShardingStrategy shardingStrategy = TABLE_SHARDING_STRATEGY.get(tableName);
                String newTableName = shardingStrategy.getTableName(tableName);
                sql = sql.replaceAll(tableName, newTableName);
            }
        }
        log.debug("sharding sql:{}", sql);

        metaObject.setValue(BOUND_SQL_KEY, sql);

        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        if (target instanceof StatementHandler && properties.isEnable()) {
            return Plugin.wrap(target, this);
        } else {
            return target;
        }
    }
}