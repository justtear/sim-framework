package com.yao2san.sim.framework.web.mybatis.sharding;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "sim.database.table-sharding")
@Configuration
public class ShardingConfigProperties {
    private boolean enable = false;
    private List<ShardingTableStrategy> mapping;

    @Data
    public static class ShardingTableStrategy {
        private List<String> tables;
        private Class<? extends ShardingStrategy> strategy;
    }
}
