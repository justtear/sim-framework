package com.yao2san.sim.framework.web.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class ServletUtil {
    public static final String TOKEN = "token";
    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static HttpServletRequest getHttpServletRequest() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            return servletRequestAttributes.getRequest();
        } else {
            throw new RuntimeException("Current request is not http request");
        }

    }
    public static HttpServletResponse getHttpServletResponse() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            return servletRequestAttributes.getResponse();
        } else {
            throw new RuntimeException("Current request is not http request");
        }

    }
    public static String getToken() {
        String token;
        HttpServletRequest request = getHttpServletRequest();
        token = request.getHeader(TOKEN);
        token = StringUtils.isEmpty(token) ? request.getParameter("token") : token;
        return token;
    }

    public static String getAuthorization() {
        HttpServletRequest request = getHttpServletRequest();
        String header = request.getHeader(AUTHORIZATION_HEADER);
        String[] s = header.split(" ");
        if (s.length < 2) {
            return null;
        }
        return s[1];
    }
}
