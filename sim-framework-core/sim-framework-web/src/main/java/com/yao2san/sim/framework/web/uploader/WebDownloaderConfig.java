package com.yao2san.sim.framework.web.uploader;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Deprecated
public class WebDownloaderConfig {
    protected boolean isDownload = false;
    protected boolean isDelete = false;

}

