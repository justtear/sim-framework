package com.yao2san.sim.framework.web.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.naming.ldap.PagedResultsControl;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "sim.web")
@Data
public class WebConfigProperties {
    /**
     * 测试配置
     */
    private Test test;

    @Configuration
    @ConfigurationProperties(prefix = "sim.web.test")
    @Data
    public static class Test {
        private boolean enabled = false;
        private Map<String, Boolean> test = new HashMap<>();
    }
}
