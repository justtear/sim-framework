package com.yao2san.sim.framework.web.uploader;

import com.yao2san.sim.framework.utils.CommonUtil;
import org.apache.commons.lang.time.DateFormatUtils;

import java.util.Date;

/**
 * @author wxg
 **/
public class DefaultFileNameRewriter implements FileNameRewriter {
    private static final String FORMAT = "yyyyMMddHHmmssSSS";

    @Override
    public String rewriter(String fileName) {
        String s = DateFormatUtils.format(new Date(), FORMAT) + CommonUtil.getRandomInt(4);
        String suffix = CommonUtil.getFileSuffix(fileName);
        if ("".equals(suffix)) {
            return s;
        }
        return s + "." + suffix;
    }
}
