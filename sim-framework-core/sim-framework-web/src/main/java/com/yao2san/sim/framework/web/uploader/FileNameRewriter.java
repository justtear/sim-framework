package com.yao2san.sim.framework.web.uploader;

/**
 * @author wxg
 **/
public interface FileNameRewriter {
    String rewriter(String fileName);
}
