package com.yao2san.sim.framework.web.bean;

import com.yao2san.sim.framework.web.annotation.CreateUser;
import com.yao2san.sim.framework.web.annotation.UpdateUser;
import lombok.Data;

/**
 * @author wxg
 * Base bean
 */
@Data
public class BaseBean {

    private String createDate;
    private String updateDate;
    @CreateUser
    private Long createUser;
    @UpdateUser
    private Long updateUser;

    private String status;

    private String remark;
}
