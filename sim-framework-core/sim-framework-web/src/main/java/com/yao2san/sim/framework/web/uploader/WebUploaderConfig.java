package com.yao2san.sim.framework.web.uploader;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class WebUploaderConfig {
    protected String fileName;

    protected boolean overwrite = false;

    protected boolean rewriteFileName = false;
}
