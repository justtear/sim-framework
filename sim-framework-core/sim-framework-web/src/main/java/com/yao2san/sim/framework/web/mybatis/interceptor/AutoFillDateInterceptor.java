package com.yao2san.sim.framework.web.mybatis.interceptor;

import com.yao2san.sim.framework.web.annotation.CreateDate;
import com.yao2san.sim.framework.web.annotation.UpdateDate;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.List;

import static com.yao2san.sim.framework.web.utils.FieldUtil.getFields;

@Component
@Intercepts({@Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})})
public class AutoFillDateInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
        Object parameter = invocation.getArgs()[1];
        if(parameter==null){
            return invocation.proceed();
        }
        List<Field> fields = getFields(parameter.getClass());
        for (Field field : fields) {
            if (field.getAnnotation(CreateDate.class) != null) {
                if (SqlCommandType.INSERT.equals(sqlCommandType)) {
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    field.set(parameter, DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd HH:mm:ss"));
                    field.setAccessible(accessible);
                }
            }
            if (field.getAnnotation(UpdateDate.class) != null) {
                if (SqlCommandType.INSERT.equals(sqlCommandType) || SqlCommandType.UPDATE.equals(sqlCommandType)) {
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    field.set(parameter, DateFormatUtils.format(System.currentTimeMillis(),"yyyy-MM-dd HH:mm:ss"));
                    field.setAccessible(accessible);
                }
            }
        }
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        if (target instanceof org.apache.ibatis.executor.Executor) {
            return Plugin.wrap(target, this);
        }
        return target;
    }


}