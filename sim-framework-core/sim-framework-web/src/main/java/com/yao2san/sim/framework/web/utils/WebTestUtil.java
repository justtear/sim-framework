package com.yao2san.sim.framework.web.utils;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.yao2san.sim.framework.utils.BeanContextUtil;
import com.yao2san.sim.framework.web.config.WebConfigProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class WebTestUtil {

    private static final String TEST_JSON = "/test.json";
    private static Map<String, String> cache = null;

    private static WebConfigProperties webConfigProperties;


    @Autowired(required = false)
    public void setWebConfigProperties(WebConfigProperties webConfigProperties) {

        WebTestUtil.webConfigProperties = webConfigProperties;
    }


    public static boolean isTest(String key) {
        if (webConfigProperties != null) {
            WebConfigProperties.Test test = webConfigProperties.getTest();
            if(test==null){
                return false;
            }
            Map<String, Boolean> map = test.getTest();
            return map.getOrDefault(key, false);
        }

        return false;
    }

    @SuppressWarnings("all")
    public static <T> T getTestData(String key,Class<T> clazz) {
        if (cache == null) {
            synchronized (WebTestUtil.class) {
                try (InputStream inputStream = WebTestUtil.class.getResourceAsStream(TEST_JSON)) {
                    if (inputStream == null) {
                        log.error("You have enabled the test function, but the test file is not found. Please add the file test.json to the resources directory");
                        return null;
                    }
                    String s = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);

                    cache = new HashMap<>();
                    cache.putAll(JSONObject.parseObject(s, Map.class));
                } catch (IOException e) {
                    log.error("", e);
                }
            }
        }
        return JSONObject.parseObject(JSONObject.toJSONString(cache.get(key)), clazz);
    }
}
