package com.yao2san.sim.framework.web.mybatis.interceptor;

import com.yao2san.sim.auth.common.UserPrincipal;
import com.yao2san.sim.framework.cache.utils.CacheUtil;
import com.yao2san.sim.framework.web.annotation.CreateUser;
import com.yao2san.sim.framework.web.annotation.DisabelAutoFillFiled;
import com.yao2san.sim.framework.web.annotation.UpdateUser;
import com.yao2san.sim.framework.web.bean.OpenToken;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.utils.ServletUtil;
import com.yao2san.sim.framework.web.utils.WebTestUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import static com.yao2san.sim.auth.common.Constants.TOKEN_KEY_PREFIX;
import static com.yao2san.sim.framework.web.utils.FieldUtil.getFields;

@Component
@Intercepts({
        @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
        @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}),
        @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})
})
@Slf4j
public class AutoFillUserInterceptor implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        Object parameter = invocation.getArgs()[1];

        if (!enabled(parameter)) {
            return invocation.proceed();

        }
        List<Field> fields = getFields(parameter.getClass());
        for (Field field : fields) {
            if (field.getAnnotation(CreateUser.class) != null) {
                //if (SqlCommandType.INSERT.equals(sqlCommandType)) {
                boolean accessible = field.isAccessible();
                field.setAccessible(true);
                field.set(parameter, getUserId());
                field.setAccessible(accessible);
                //}
            }
            if (field.getAnnotation(UpdateUser.class) != null) {
                //if (SqlCommandType.INSERT.equals(sqlCommandType) || SqlCommandType.UPDATE.equals(sqlCommandType)) {
                boolean accessible = field.isAccessible();
                field.setAccessible(true);
                field.set(parameter, getUserId());
                field.setAccessible(accessible);
                //}
            }
        }
        return invocation.proceed();
    }

    private boolean enabled(Object param) {
        if (param == null) {
            return false;
        }
        boolean isDisable = param.getClass().isAnnotationPresent(DisabelAutoFillFiled.class);
        return !(isDisable || param instanceof Map);
    }

    @Override
    public Object plugin(Object target) {
        if (target instanceof org.apache.ibatis.executor.Executor) {
            return Plugin.wrap(target, this);
        }
        return target;
    }


    private Long getUserId() {

        if (WebTestUtil.isTest("user")) {
            UserPrincipal userPrincipal = WebTestUtil.getTestData("user", UserPrincipal.class);
            if (userPrincipal == null) {
                throw new BusiException("User not found!");
            }
            return Long.valueOf(userPrincipal.getUserId());
        }

        UserPrincipal userPrincipal = (UserPrincipal) SecurityUtils.getSubject().getPrincipal();
        if (userPrincipal != null) {
            return Long.valueOf(userPrincipal.getUserId());
        }

        OpenToken info = CacheUtil.get(TOKEN_KEY_PREFIX + ServletUtil.getToken());
        if (info == null || info.getUserId() == null) {
            throw new BusiException("User not found!");
        }
        return info.getUserId();
    }

}