package com.yao2san.sim.framework.web.uploader;

import com.yao2san.sim.framework.utils.BeanContextUtil;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * @author wxg
 **/
@Deprecated
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true)
public class WebDownloader extends WebDownloaderConfig {
    private Boolean delete = false;

    public void download(String file, String fileName, HttpServletResponse response) {
        DefaultWebDownloader downloader = BeanContextUtil.getBean(DefaultWebDownloader.class);
        WebDownloaderConfig config = WebDownloaderConfig.builder().isDownload(isDownload).isDelete(isDelete).build();
        downloader.download(file, fileName, response, config);
    }

    @SneakyThrows
    public static void fillDownloadResponse(String fileName, HttpServletResponse response, boolean isDownload) {
        response.reset();
        response.setCharacterEncoding("utf-8");
        if (isDownload) {
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName,"utf-8"));
        } else {
            response.setContentType(URLConnection.guessContentTypeFromName(fileName));
        }
    }
}
