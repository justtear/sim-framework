package com.yao2san.sim.framework.web.uploader;

import com.yao2san.sim.framework.utils.CommonUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Data
@Slf4j
public class DefaultWebUploader {
    private WebUpDownloadProperties properties;
    private FolderPolicy policy;

    private FileNameRewriter fileNameRewriter;


    public UploadResult upload(MultipartFile file) throws IOException {
        return upload(file, new WebUploaderConfig());
    }

    public UploadResult upload(MultipartFile file, WebUploaderConfig config) throws IOException {
        String folder = policy.create();
        folder = CommonUtil.formatPath(folder);
        if (properties.getStoreType() == WebUpDownloadProperties.StoreType.LOCAL) {
            return uploadByLocal(file, folder, config);
        } else {
            throw new RuntimeException("Just support store type:LOCAL");
        }
    }

    private UploadResult uploadByLocal(MultipartFile file, String folder, WebUploaderConfig config) throws IOException {
        UploadResult uploadResult = new UploadResult();
        String path = folder;
        path = CommonUtil.formatPath(path);
        String name = file.getOriginalFilename();
        if (config.getFileName() != null) {
            name = config.getFileName();
        }
        if (config.isRewriteFileName()) {
            name = fileNameRewriter.rewriter(name);
        }
        String pathName = path + "/" + name;
        pathName = CommonUtil.formatPath(pathName);
        String relativePath = getRelativePath(pathName);
        File f = new File(pathName);

        uploadResult
                .setSuccess(false)
                .setOriginalFileName(file.getOriginalFilename())
                .setPathName(relativePath);
        if (config.isRewriteFileName()) {
            uploadResult.setFileName(fileNameRewriter.rewriter(name));
        } else {
            uploadResult.setFileName(name);
        }
        if (!config.isOverwrite() && f.exists()) {
            uploadResult.setSuccess(false).setMessage("file exist");
            return uploadResult;
        }
        String p = CommonUtil.getPath(pathName);
        File pp = new File(p);
        if (!pp.exists()) {
            pp.mkdirs();
        }
        file.transferTo(f);
        uploadResult.setSuccess(true).setMessage("ok");
        return uploadResult;
    }

    private String getRelativePath(String pathName) {
        String s = pathName.replace(CommonUtil.formatPath(properties.getRootPath()), "");
        return s.startsWith("/") ? s : ("/" + s);
    }


    private void uploadByRemote(MultipartFile file) {

    }

    private void uploadByFtp(MultipartFile file) {

    }

    private void uploadBySftp(MultipartFile file) {

    }
}
