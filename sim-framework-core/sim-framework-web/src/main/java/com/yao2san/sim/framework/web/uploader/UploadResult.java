package com.yao2san.sim.framework.web.uploader;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author wxg
 **/
@Data
@Accessors(chain = true)
public class UploadResult {
    private boolean success;
    private String message;
    private String originalFileName;
    private String fileName;
    private String pathName;
}
