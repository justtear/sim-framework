package com.yao2san.sim.framework.web.uploader;

import com.yao2san.sim.framework.utils.CommonUtil;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author wxg
 **/
@Data
@Slf4j
public class DefaultWebDownloader {
    private WebUpDownloadProperties properties;

    public void download(String file, String fileName, HttpServletResponse response) {
        download(file, fileName, response, new WebDownloaderConfig());
    }

    public void download(String file, String fileName, HttpServletResponse response, WebDownloaderConfig config) {
        if (WebUpDownloadProperties.StoreType.LOCAL.equals(properties.getStoreType())) {
            downloadByLocal(file, fileName, response, config);
        }
    }

    private void downloadByLocal(String file, String fileName, HttpServletResponse response, WebDownloaderConfig config) {
        String rootPath = properties.getRootPath();
        file = rootPath + file;
        file = CommonUtil.formatPath(file);
        File ff = new File(file);
        response.reset();
        response.setCharacterEncoding("utf-8");
        response.setContentLength(Integer.parseInt(String.valueOf(ff.length())));
        if (config.isDownload()) {
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
        } else {
            response.setContentType(URLConnection.guessContentTypeFromName(file));
        }

        try (BufferedInputStream bis = new BufferedInputStream(Files.newInputStream(Paths.get(file)))) {
            byte[] buff = new byte[1024];
            OutputStream os = response.getOutputStream();
            int i;
            while ((i = bis.read(buff)) != -1) {
                os.write(buff, 0, i);
                os.flush();
            }
        } catch (IOException e) {
            log.error("", e);
        }

        if (config.isDelete()) {
            if (ff.isFile()) {
                ff.delete();
            }
        }

    }
}
