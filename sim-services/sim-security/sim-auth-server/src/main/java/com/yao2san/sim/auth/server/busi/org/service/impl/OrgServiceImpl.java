package com.yao2san.sim.auth.server.busi.org.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.lang.tree.parser.NodeParser;
import cn.hutool.core.map.MapUtil;
import com.github.pagehelper.PageInfo;
import com.yao2san.sim.auth.server.busi.org.bean.request.OrgAddReq;
import com.yao2san.sim.auth.server.busi.org.bean.request.OrgListReq;
import com.yao2san.sim.auth.server.busi.org.bean.request.OrgUpdateReq;
import com.yao2san.sim.auth.server.busi.org.bean.response.OrgDetailRes;
import com.yao2san.sim.auth.server.busi.org.bean.response.OrgListRes;
import com.yao2san.sim.auth.server.busi.org.bean.response.OrgTreeRes;
import com.yao2san.sim.auth.server.busi.org.service.OrgService;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import lombok.SneakyThrows;
import org.apache.commons.configuration.tree.TreeUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author wxg
 */
@Service
public class OrgServiceImpl extends BaseServiceImpl implements OrgService {
    private static final TreeNodeConfig TREE_NODE_CONFIG;

    static {
        TREE_NODE_CONFIG = new TreeNodeConfig();
        TREE_NODE_CONFIG.setIdKey("orgId");
        TREE_NODE_CONFIG.setParentIdKey("parentId");
    }

    @Override
    public ResponseData<PageInfo<OrgListRes>> list(OrgListReq req) {
        PageInfo<OrgListRes> list = this.qryList("org.list", req);
        return ResponseData.success(list);
    }

    @Override
    public ResponseData<List<Tree<Long>>> tree(OrgListReq req) {
        List<OrgListRes> list = this.sqlSession.selectList("org.list", req);
        List<OrgTreeRes> treeRes = new ArrayList<>();
        for (OrgListRes res : list) {
            OrgTreeRes orgTreeRes = new OrgTreeRes();
            BeanUtils.copyProperties(res, orgTreeRes);
            treeRes.add(orgTreeRes);
        }
        List<Tree<Long>> tree = TreeUtil.build(treeRes, -1L, TREE_NODE_CONFIG, new NodeParser<OrgTreeRes, Long>() {
            @SneakyThrows
            @Override
            public void parse(OrgTreeRes treeNode, Tree<Long> tree) {
                tree.setId(treeNode.getOrgId());
                tree.setParentId(treeNode.getParentId());

                Field[] fields = treeNode.getClass().getDeclaredFields();
                for (Field field : fields) {
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    Object o = field.get(treeNode);
                    tree.put(field.getName(), o);
                    field.setAccessible(accessible);
                }
            }
        });
        return ResponseData.success(tree);
    }

    @Override
    public ResponseData<OrgDetailRes> detail(Long orgId) {
        return null;
    }

    @Override
    @Transactional
    public ResponseData<Void> add(OrgAddReq req) {
        Integer count = sqlSession.selectOne("org.isOrgCodeExist", req.getOrgCode());
        if (count > 0) {
            return ResponseData.error("机构编码已存在");
        }
        count = sqlSession.selectOne("org.isOrgNameExist", req.getOrgName());
        if (count > 0) {
            return ResponseData.error("机构名称已存在");
        }
        sqlSession.insert("org.add", req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> update(OrgUpdateReq req) {
        this.sqlSession.update("org.updateOrg", req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> delete(List<Long> orgIds) {
        this.sqlSession.delete("org.deleteOrg", orgIds);
        return ResponseData.success();
    }
}
