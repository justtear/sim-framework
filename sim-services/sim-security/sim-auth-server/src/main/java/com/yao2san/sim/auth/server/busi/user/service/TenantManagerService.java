package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.auth.server.busi.user.bean.request.TenantServiceReq;
import com.yao2san.sim.framework.web.response.ResponseData;

public interface TenantManagerService {
    ResponseData services(TenantServiceReq req);
    ResponseData serviceAuthorization(TenantServiceReq req);

}
