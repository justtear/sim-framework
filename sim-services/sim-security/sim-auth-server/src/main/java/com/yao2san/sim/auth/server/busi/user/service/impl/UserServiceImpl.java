package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.yao2san.sim.auth.common.AuthProperties;
import com.yao2san.sim.auth.common.UserPrincipal;
import com.yao2san.sim.auth.common.UserUtil;
import com.yao2san.sim.auth.server.busi.user.bean.*;
import com.yao2san.sim.auth.server.busi.user.service.PermissionService;
import com.yao2san.sim.auth.server.busi.user.service.RoleService;
import com.yao2san.sim.auth.server.busi.user.service.UserService;
import com.yao2san.sim.framework.utils.BeanContextUtil;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl extends BaseServiceImpl implements UserService {

    private final AuthProperties properties;

    public UserServiceImpl(AuthProperties properties) {
        this.properties = properties;
    }

    @Override
    public ResponseData<Object> getUserInfo(String token, String scope) {
        switch (scope) {
            case "":
            case "all":
                return ResponseData.success(getAllUserInfo());
            case "roles":
                return ResponseData.success(getUserRoles());
            case "purview":
                return ResponseData.success(getUserPermissions());
        }
        return ResponseData.success();
    }

    private Map<String, Object> getAllUserInfo() {
        Map<String, Object> baseInfo = new HashMap<>();
        UserPrincipal userPrincipal = (UserPrincipal) SecurityUtils.getSubject().getPrincipal();
        baseInfo.put("username", userPrincipal.getUserName());
        //baseInfo.put("token", UserUtil.getCurrUserToken());
        baseInfo.put("avatar", userPrincipal.getAvatar());

        baseInfo.put("roles", getUserRoles());
        baseInfo.put("permissions", getUserPermissions());
        baseInfo.put("baseInfo", userPrincipal);
        return baseInfo;
    }

    private List<RoleSimplify> getUserRoles() {
        RoleService roleService = BeanContextUtil.getBean(RoleService.class);
        Long userId = UserUtil.getCurrUserId();
        List<Role> userRoles = roleService.getUserRoles(userId);
        //简化角色属性 去除无用字段
        return userRoles.stream().map(v -> {
            RoleSimplify res = new RoleSimplify();
            BeanUtils.copyProperties(v, res);
            return res;
        }).collect(Collectors.toList());
    }

    private List<PermissionSimplify> getUserPermissions() {
        PermissionService permissionService = BeanContextUtil.getBean(PermissionService.class);
        Long userId = UserUtil.getCurrUserId();
        List<Permission> userPermissions = permissionService.getUserPermissions(userId);
        //简化权限属性 去除无用字段
        return userPermissions.stream().map(v -> {
            PermissionSimplify res = new PermissionSimplify();
            BeanUtils.copyProperties(v, res);
            return res;
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ResponseData updateCurrUserInfo(User user) {
        user.setUserId(UserUtil.getCurrUserId());
        this.sqlSession.update("user.updateUser", user);
        //更新shiro中的用户信息
        //TODO
        //UserUtil.updateCurrUserSessionInfo(user);

        return ResponseData.success();
    }


    //TODO 待优化
    @SuppressWarnings("all")
    public String upsertUserAttr(Long userId, Map<String, List<Object>> urlAttr) {
        //校验属性编码是否存在
        if (urlAttr != null) {
            AtomicBoolean isCheck = new AtomicBoolean(true);
            urlAttr.keySet().forEach(item -> {
                Integer count = this.sqlSession.selectOne("common.checkAttrCode", item);
                if (count <= 0) {
                    isCheck.set(false);
                }
            });
            if (!isCheck.get()) {
                return "扩展属性不存在,请检查属性编码";
            }
            urlAttr.forEach((key, value) -> {
                Map<String, Object> params = new HashMap();
                params.put("userId", userId);
                params.put("attrCode", key);
                params.put("attrValueList", value);
                int count = this.sqlSession.selectOne("user.checkUserAttr", params);
                if (count > 0) {
                    this.sqlSession.delete("user.deleteUserAttr", params);

                    this.sqlSession.insert("user.addUserAttr", params);
                } else {
                    this.sqlSession.insert("user.addUserAttr", params);
                }

            });

        }
        return "";
    }

    public void updateUserAttrValue(Long userId, String attrCode, Object attrValue) {
        Map<String, Object> params = new HashMap();
        params.put("userId", userId);
        params.put("attrCode", attrCode);
        params.put("attrValue", attrValue);
        this.sqlSession.update("user.updateUserAttr", params);
    }

    public List<Map> qryUserAttr(String attrCode) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("userId", UserUtil.getCurrUserId());
        params.put("attrCode", attrCode);
        List<Map> list = this.sqlSession.selectList("user.qryUserAttr", params);
        return list;
    }

    @SuppressWarnings("unchecked")
    public String qryCurrUserAttrSingleValue(String attrCode) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("userId", UserUtil.getCurrUserId());
        params.put("attrCode", attrCode);
        List<Map> list = this.sqlSession.selectList("user.qryUserAttr", params);
        if (list != null && list.size() > 0) {
            return (String) list.get(0).get("attrValue");
        }
        return null;
    }

    @Override
    public String qryUserAttrSingleValue(Long userId, String attrCode) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("userId", userId);
        params.put("attrCode", attrCode);
        List<Map> list = this.sqlSession.selectList("user.qryUserAttr", params);
        if (list != null && list.size() > 0) {
            return (String) list.get(0).get("attrValue");
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<String> qryUserAttrListValue(String attrCode) {
        Map<String, Object> params = new HashMap<>(2);
        params.put("userId", UserUtil.getCurrUserId());
        params.put("attrCode", attrCode);
        List<Map> list = this.sqlSession.selectOne("user.qryUserAttr", params);
        if (list != null && list.size() > 0) {
            return list.stream().map(item -> (String) item.get("attrValue")).collect(Collectors.toList());
        }
        return null;
    }
}
