package com.yao2san.sim.auth.server.busi.user.bean.request;

import lombok.Data;

@Data
public class ResetPwdReq {

    private Long userId;

    private String pwd;
}
