package com.yao2san;

import com.yao2san.sim.storage.client.annotations.SimStorageClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@Slf4j
@SimStorageClient
public class SimAuthServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimAuthServerApplication.class, args);
        log.info("sim-auth-server start success!");
    }
}
