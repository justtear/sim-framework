package com.yao2san.sim.auth.server.busi.user.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户角色
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleSimplify {
    private Long roleId;
    private String roleName;
    private String roleCode;
    private Integer notUpdate;
}
