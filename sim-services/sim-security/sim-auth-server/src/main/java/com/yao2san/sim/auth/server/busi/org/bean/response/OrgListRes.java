package com.yao2san.sim.auth.server.busi.org.bean.response;

import com.yao2san.sim.auth.server.busi.org.bean.request.OrgAttr;
import lombok.Data;

import java.util.List;

/**
 * @author wxg
 */
@Data
public class OrgListRes {
    private Long orgId;
    private Long parentId;
    private String orgName;
    private String orgCode;
    private String orgType;
    private String description;
    private String status;

    private List<OrgAttr> attrs;
}
