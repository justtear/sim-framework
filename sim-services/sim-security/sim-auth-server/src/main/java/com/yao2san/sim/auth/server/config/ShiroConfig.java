package com.yao2san.sim.auth.server.config;

import com.yao2san.sim.auth.common.AuthProperties;
import com.yao2san.sim.auth.common.AuthType;
import com.yao2san.sim.auth.server.shiro.filter.PermissionFilter;
import com.yao2san.sim.auth.server.shiro.realm.JwtRealm;
import com.yao2san.sim.auth.server.shiro.realm.RealmAuthenticator;
import com.yao2san.sim.auth.server.shiro.realm.UsernamePasswordRealm;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.Authenticator;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.mgt.*;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.MemorySessionDAO;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.authc.BearerHttpAuthenticationFilter;
import org.apache.shiro.web.filter.session.NoSessionCreationFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.cache.CacheType;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.annotation.Resource;
import javax.servlet.Filter;
import java.util.*;

import static com.yao2san.sim.auth.common.Constants.*;

@Configuration
@Slf4j
@ConditionalOnProperty(prefix = "sim.auth", name = "model", havingValue = "server")
@Import(CacheProperties.class)
public class ShiroConfig {

    @Resource
    private AuthProperties properties;
    @Resource
    private RedisSessionDAO redisSessionDAO;

    @Resource
    private RedisCacheManager redisCacheManager;

    @Autowired
    private CacheProperties cacheProperties;


    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        shiroFilterFactoryBean.setLoginUrl(properties.getLoginUrl());
        shiroFilterFactoryBean.setUnauthorizedUrl(properties.getUnauthorizedUrl());
        shiroFilterFactoryBean.setSuccessUrl(properties.getSuccessUrl());
        Map<String, Filter> filters = new HashMap<>();
        filters.put(BEARER_FILTER, new BearerHttpAuthenticationFilter());
        filters.put(NO_SESSION_FILTER, new NoSessionCreationFilter());
        filters.put(PERMISSION_FILTER, new PermissionFilter());
        shiroFilterFactoryBean.setFilters(filters);

        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        //默认白名单 不做认证
        for (String s : DEFAULT_WHITE_LIST) {
            filterChainDefinitionMap.put(s, ANYONE_FILTER);
        }

        //白名单
        List<String> whiteList = properties.getWhiteList();
        Map<String, String> whiteMap = new HashMap<>();
        whiteList.forEach(v -> whiteMap.put(v, ANYONE_FILTER));
        filterChainDefinitionMap.putAll(whiteMap);

        //如果包含*，则全部放行
        if (whiteList.contains("*")) {
            filterChainDefinitionMap.put("/**", "anon");
            log.warn("The white-list config contain '*', all services will be accessible without authentication!");
        } else {
            //session认证 使用内置的authc过滤器
            if (properties.getAuthType() == AuthType.SESSION) {
                filterChainDefinitionMap.put("/**", AUTH_FILTER);
            }
            //JWT认证使用内置的Bearer过滤器
            if (properties.getAuthType() == AuthType.JWT) {
                filterChainDefinitionMap.put("/**", String.join(",", NO_SESSION_FILTER, BEARER_FILTER,PERMISSION_FILTER));
            }
        }

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);

        return shiroFilterFactoryBean;

    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "session", matchIfMissing = true)
    public UsernamePasswordRealm usernamePasswordRealm() {
        return new UsernamePasswordRealm();
    }


    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "jwt")
    public JwtRealm jwtRealm() {
        if (properties.getJwt() == null) {
            throw new NullPointerException("You used auth type with 'jwt', but jwt config is null");
        }
        return new JwtRealm(properties.getJwt().isVerify());
    }

    @Bean
    public Authenticator authenticator(List<Realm> realms) {
        RealmAuthenticator authenticator = new RealmAuthenticator();
        authenticator.setRealms(realms);
        authenticator.setAuthenticationStrategy(new AtLeastOneSuccessfulStrategy());
        return authenticator;
    }

    @Bean
    public SessionsSecurityManager securityManager(SessionManager sessionManager, List<Realm> realms, Authenticator authenticator, SubjectDAO subjectDAO) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setSessionManager(sessionManager);
        securityManager.setRealms(realms);
        securityManager.setAuthenticator(authenticator);

        AuthType authType = properties.getAuthType();
        if (authType == AuthType.SESSION) {
            if(CacheType.SIMPLE.equals(cacheProperties.getType())){
                securityManager.setCacheManager(new MemoryConstrainedCacheManager());
            }else {
                securityManager.setCacheManager(redisCacheManager);
            }
        } else if (authType == AuthType.JWT) {
            securityManager.setSubjectDAO(subjectDAO);
            securityManager.setRememberMeManager(null);
            securityManager.setCacheManager(null);
        }

        return securityManager;
    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "jwt")
    public SessionManager sessionManagerForJwt() {
        return new DefaultWebSessionManager();
    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "session", matchIfMissing = true)
    public SessionManager sessionManagerForSession() {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        if(CacheType.SIMPLE.equals(cacheProperties.getType())){
            sessionManager.setSessionDAO(new MemorySessionDAO());
        }else {
            sessionManager.setGlobalSessionTimeout(-1);
            sessionManager.setSessionDAO(redisSessionDAO);
        }
        return sessionManager;
    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "jwt")
    public SubjectDAO subjectDAO(SessionStorageEvaluator sessionStorageEvaluator) {
        DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
        subjectDAO.setSessionStorageEvaluator(sessionStorageEvaluator);
        return subjectDAO;
    }

    @Bean
    public SimpleMappingExceptionResolver resolver() {
        SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
        Properties properties = new Properties();
        properties.setProperty("org.apache.shiro.authz.UnauthorizedException", "/403");
        resolver.setExceptionMappings(properties);
        return resolver;
    }

    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("SHA-256");
        hashedCredentialsMatcher.setHashIterations(1024);
        return hashedCredentialsMatcher;
    }

}
