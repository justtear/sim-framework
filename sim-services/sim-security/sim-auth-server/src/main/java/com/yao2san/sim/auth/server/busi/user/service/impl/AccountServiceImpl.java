package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.yao2san.sim.auth.common.UserPrincipal;
import com.yao2san.sim.auth.server.busi.user.bean.Permission;
import com.yao2san.sim.auth.server.busi.user.bean.Role;
import com.yao2san.sim.auth.server.busi.user.service.AccountService;
import com.yao2san.sim.auth.server.busi.user.service.PermissionService;
import com.yao2san.sim.auth.server.busi.user.service.RoleService;
import com.yao2san.sim.auth.server.utils.EncodeUtil;
import com.yao2san.sim.framework.utils.BeanContextUtil;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import org.apache.commons.collections4.MapUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.shiro.authc.AccountException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl extends BaseServiceImpl implements AccountService {

    @Override
    public UserPrincipal login(String username, String password) {

        //从数据库获取用户信息
        Map<String, Object> info = this.getUserBaseInfoByUsername(username);
        if (info == null) {
            throw new AccountException("用户不存在");
        }
        //用户名和密码
        String uid = MapUtils.getString(info, "userId");
        String un = MapUtils.getString(info, "userName");
        String pwd = MapUtils.getString(info, "password");
        String avatar = MapUtils.getString(info, "avatar");
        //对用户输入的密码加密
        String encodePwd = EncodeUtil.encodePwd(un, password);
        //校验
        if (username == null) {
            throw new AccountException("用户名不正确");
        } else if (!encodePwd.equals(pwd)) {
            throw new AccountException("密码不正确");
        }

        //用户基本信息
        UserPrincipal user = new UserPrincipal();
        user.setId(uid);
        user.setUserId(uid);
        user.setUserName(un);
        user.setType("USER");
        user.setAvatar(avatar);
        //设置角色和权限
        RoleService roleService = BeanContextUtil.getBean(RoleService.class);
        PermissionService permissionService = BeanContextUtil.getBean(PermissionService.class);
        //用户角色
        List<Role> userRoles = roleService.getUserRoles(Long.valueOf(user.getUserId()));
        //用户权限
        List<Permission> userPermissions = permissionService.getUserPermissions(Long.valueOf(user.getUserId()));

        user.setRoles(userRoles.stream().map(Role::getRoleCode).collect(Collectors.toSet()));
        user.setPermissions(userPermissions.stream().map(Permission::getPurviewCode).collect(Collectors.toSet()));

        return user;
    }

    private Map<String, Object> getUserBaseInfoByUsername(String userName) {
        SqlSession sqlSession = BeanContextUtil.getBean(SqlSession.class);
        return sqlSession.selectOne("user.qryUserBaseInfoByUsername", userName);
    }

}
