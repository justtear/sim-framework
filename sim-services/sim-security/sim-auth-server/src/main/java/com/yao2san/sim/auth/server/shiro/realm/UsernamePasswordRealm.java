package com.yao2san.sim.auth.server.shiro.realm;

import com.yao2san.sim.auth.common.UserPrincipal;
import com.yao2san.sim.auth.server.busi.user.service.AccountService;
import com.yao2san.sim.auth.server.utils.EncodeUtil;
import com.yao2san.sim.framework.utils.BeanContextUtil;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;


public class UsernamePasswordRealm extends AuthorizingRealm {
    private final static String REALM_NAME = "user";

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof UsernamePasswordToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        UserPrincipal userPrincipal = (UserPrincipal) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(userPrincipal.getRoles());
        authorizationInfo.setStringPermissions(userPrincipal.getPermissions());
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String userName = token.getUsername();
        String userPwd = new String(token.getPassword());

        AccountService accountService = BeanContextUtil.getBean(AccountService.class);
        UserPrincipal user = accountService.login(userName, userPwd);
        return new SimpleAuthenticationInfo(user, userPwd, EncodeUtil.getSalt(userName), getName());
    }


    @Override
    public String getName() {
        return REALM_NAME;
    }

    @Override
    public boolean isCachingEnabled() {
        return true;
    }
}
