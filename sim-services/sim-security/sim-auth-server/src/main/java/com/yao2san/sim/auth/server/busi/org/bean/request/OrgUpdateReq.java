package com.yao2san.sim.auth.server.busi.org.bean.request;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrgUpdateReq extends BaseBean {
    @NotNull(message = "orgId不能为空")
    private Long orgId;
    private Long parentId;
    private String orgName;
    private String orgCode;
    private String orgType;
    private String description;
    private List<OrgAttr> attrs;
}
