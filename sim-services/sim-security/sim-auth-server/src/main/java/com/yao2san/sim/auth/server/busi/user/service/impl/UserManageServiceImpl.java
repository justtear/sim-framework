package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.yao2san.sim.auth.common.UserUtil;
import com.yao2san.sim.auth.server.busi.constant.SystemConstant;
import com.yao2san.sim.auth.server.busi.user.bean.Permission;
import com.yao2san.sim.auth.server.busi.user.bean.Role;
import com.yao2san.sim.auth.server.busi.user.bean.User;
import com.yao2san.sim.auth.server.busi.user.bean.request.ResetPwdReq;
import com.yao2san.sim.auth.server.busi.user.service.UserManageService;
import com.yao2san.sim.auth.server.utils.EncodeUtil;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.storage.client.core.event.UploadResult;
import com.yao2san.sim.storage.client.core.uploader.Uploader;
import com.yao2san.sim.storage.client.exception.UploadErrorException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserManageServiceImpl extends BaseServiceImpl implements UserManageService {

    @Autowired
    private Uploader uploader;

    @Override
    public ResponseData<PageInfo<Map<String, Object>>> qryUserList(User user) {
        //用户信息
        PageInfo<Map<String, Object>> list = this.qryList("userManage.qryUserList", user);
        //角色
        list.getList().forEach(u -> {
            List<Role> roles = this.sqlSession.selectList("role.getUserRoles", MapUtils.getLong(u, "userId"));
            u.put("roles", roles.stream().map(Role::getRoleName).collect(Collectors.joining(",")));
        });
        return ResponseData.success(list);
    }

    @Override
    public ResponseData<PageInfo<Object>> qryUserPermissions(Permission permission) {
        PageInfo<Object> list;
        if ("1".equals(permission.getIsPermissions())) {
            list = this.qryList("userManage.qryUserPermissions", permission);
        } else {
            list = this.qryList("userManage.qryUserNotPermissions", permission);
        }
        return ResponseData.success(list);
    }

    @Override
    @Transactional
    public ResponseData<Void> addOrUpdateUserPermissions(Map<String, Object> params) {
        JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(params));
        JSONArray userIds = object.getJSONArray("userIds");
        JSONArray permissions = object.getJSONArray("permissions");
        //操作类型: 1 增加授权 2 取消授权
        String option = object.getString("option");
        if (userIds == null
                || permissions == null
                || userIds.size() == 0
                || permissions.size() == 0
                || userIds.get(0) == null
                || permissions.get(0) == null) {
            return ResponseData.businessError("权限和用户标识不能为空");
        }
        List<Map<String, Long>> list = new ArrayList<>();
        Long currUserId = UserUtil.getCurrUserId();
        for (Object userId : userIds) {
            Long uid = Long.valueOf(String.valueOf(userId));
            for (Object permission : permissions) {
                Long purviewId = Long.valueOf(String.valueOf(permission));
                Map<String, Long> map = new HashMap<>(2);
                map.put("userId", uid);
                map.put("purviewId", purviewId);
                map.put("createUser", currUserId);
                list.add(map);
            }
        }
        log.info("用户特殊授权变更(option={}):{}", option, list);
        //增加授权
        if ("1".equals(option)) {
            //先取消授权
            this.sqlSession.delete("userManage.delPurviewUserRel", list);
            //重新授权
            this.sqlSession.insert("userManage.addPurviewUserRel", list);
        } else if ("2".equals(option)) {
            //取消授权
            this.sqlSession.delete("userManage.delPurviewUserRel", list);
        } else {
            return ResponseData.businessError("不支持的操作类型: option=" + option);
        }

        return ResponseData.success(null, "操作成功");
    }

    @Override
    public ResponseData<PageInfo<Map>> qryUserRoles(User user) {
        PageInfo<Map> list = this.qryList("userManage.qryUserRoles", user);
        return ResponseData.success(list);
    }

    @Override
    @Transactional
    public ResponseData<Void> addOrUpdateUserRoles(Map<String, Object> params) {
        JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(params));
        JSONArray userIds = object.getJSONArray("userIds");
        JSONArray permissions = object.getJSONArray("roleIds");
        //操作类型: 1 增加授权 2 取消授权
        String option = object.getString("option");
        if (userIds == null
                || permissions == null
                || userIds.size() == 0
                || permissions.size() == 0
                || userIds.get(0) == null
                || permissions.get(0) == null) {
            return ResponseData.businessError("角色和用户标识不能为空");
        }
        List<Map<String, Long>> list = new ArrayList<>();
        Long currUserId = UserUtil.getCurrUserId();
        for (Object userId : userIds) {
            Long uid = Long.valueOf(String.valueOf(userId));
            for (Object role : permissions) {
                Long roleId = Long.valueOf(String.valueOf(role));
                Map<String, Long> map = new HashMap<>(2);
                map.put("userId", uid);
                map.put("roleId", roleId);
                map.put("createUser", currUserId);
                list.add(map);
            }
        }
        log.info("用户角色变更(option={}):{}", option, list);
        //增加授权
        if ("1".equals(option)) {
            //先取消授权
            this.sqlSession.delete("userManage.delUserRoleRel", list);
            //重新授权
            this.sqlSession.insert("userManage.addUserRoleRel", list);
        } else if ("2".equals(option)) {
            //取消授权
            this.sqlSession.delete("userManage.delUserRoleRel", list);
        } else {
            return ResponseData.businessError("不支持的操作类型: option=" + option);
        }

        return ResponseData.success(null, "操作成功");
    }

    @Override
    @Transactional
    public ResponseData<Void> addUser(User user) {

        String pwd = EncodeUtil.encodePwd(user.getUserName(), user.getPhone().substring(5, 11));
        user.setPassword(pwd);
        this.sqlSession.insert("userManage.addUser", user);
        addUserOrgRel(user);
        return ResponseData.success();
    }

    private void addUserOrgRel(User user) {
        if (user.getOrgId() != null) {
            this.sqlSession.delete("userManage.delUserOrgRel", user);
            this.sqlSession.insert("userManage.addUserOrgRel", user);
        }
    }

    @Override
    @Transactional
    public ResponseData<Void> updateUser(User user) {
        this.sqlSession.update("userManage.updateUser", user);
        addUserOrgRel(user);
        return ResponseData.success();
    }

    @Override
    public ResponseData<Map<String, Object>> detail(Long userId) {
        Map<String, Object> data = this.sqlSession.selectOne("userManage.qryUser", userId);
        //TODO 查询用户属性
        return ResponseData.success(data);
    }

    @Override
    public ResponseData<Void> resetPwd(ResetPwdReq req) {
        Map<String, Object> data = this.sqlSession.selectOne("userManage.qryUser", req.getUserId());
        String encodePwd = EncodeUtil.encodePwd(MapUtils.getString(data, "userName"), req.getPwd());
        Map<String, Object> param = new HashMap<>(2);
        param.put("userId", req.getUserId());
        param.put("pwd", encodePwd);
        this.sqlSession.update("userManage.updatePwd", param);
        return ResponseData.success();
    }

    @Override
    public ResponseData<String> uploadAvatar(MultipartFile file) {
        try {
            UploadResult uploadResult = uploader.upload(file, SystemConstant.USER_HEAD_IMAGE_PATH + UUID.randomUUID() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
            return ResponseData.success(uploadResult.getUrl());
        } catch (IOException | UploadErrorException e) {
            throw new BusiException("头像上传失败");
        }

    }
}
