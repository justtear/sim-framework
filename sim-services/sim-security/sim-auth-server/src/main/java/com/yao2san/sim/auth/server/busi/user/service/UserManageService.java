package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.auth.server.busi.user.bean.Permission;
import com.yao2san.sim.auth.server.busi.user.bean.User;
import com.yao2san.sim.auth.server.busi.user.bean.request.ResetPwdReq;
import com.yao2san.sim.framework.web.response.ResponseData;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface UserManageService {
    /**
     * 查询用户列表
     */
    ResponseData qryUserList(User user);

    ResponseData qryUserPermissions(Permission permission);

    ResponseData addOrUpdateUserPermissions(Map<String, Object> params);

    ResponseData qryUserRoles(User user);

    ResponseData addOrUpdateUserRoles(Map<String, Object> params);

    ResponseData<Void> addUser(User user);

    ResponseData updateUser(User user);

    ResponseData detail(Long userId);

    ResponseData<Void> resetPwd(ResetPwdReq req);
    ResponseData<String> uploadAvatar(MultipartFile file);
}
