package com.yao2san.sim.auth.server.busi.user.controller;

import com.yao2san.sim.auth.server.busi.user.bean.User;
import com.yao2san.sim.auth.server.busi.user.service.UserService;
import com.yao2san.sim.framework.web.response.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 获取用户基本信息
     *
     * @param token token进行验证
     * @param scope base/role/purview/all
     */
    @GetMapping("/token")
    public ResponseData<Object> getUserBaseInfo(String token, @RequestParam(required = false,defaultValue = "") String scope) {
        return userService.getUserInfo(token,scope);
    }

    @PatchMapping
    public ResponseData updateUserInfo(@RequestBody User user) {
        return userService.updateCurrUserInfo(user);
    }
}
