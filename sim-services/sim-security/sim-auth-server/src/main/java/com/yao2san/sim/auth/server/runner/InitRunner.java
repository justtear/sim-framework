package com.yao2san.sim.auth.server.runner;

import com.yao2san.sim.auth.server.busi.user.service.PermissionManageService;
import com.yao2san.sim.framework.utils.BeanContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class InitRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) {
        PermissionManageService service = BeanContextUtil.getBean(PermissionManageService.class);
        service.cachePermission();

    }
}
