package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.auth.common.UserPrincipal;

public interface AccountService {
    UserPrincipal login(String username, String password);
}
