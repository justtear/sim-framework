package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.yao2san.sim.auth.common.AuthProperties;
import com.yao2san.sim.auth.common.AuthType;
import com.yao2san.sim.auth.common.JwtUtil;
import com.yao2san.sim.auth.common.UserPrincipal;
import com.yao2san.sim.auth.server.busi.enums.LoginType;
import com.yao2san.sim.auth.server.busi.user.bean.User;
import com.yao2san.sim.auth.server.busi.user.bean.UserAuth;
import com.yao2san.sim.auth.server.busi.user.service.*;
import com.yao2san.sim.framework.utils.CommonUtil;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.auth.server.utils.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.BearerToken;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SecurityServiceImpl extends BaseServiceImpl implements SecurityService {
    @Autowired
    private UserService userService;

    @Autowired
    private AuthProperties properties;
    @Autowired
    private AccountService accountService;

    @Override
    @Transactional
    public ResponseData<Map<String, Object>> login(Map<String, Object> params) {
        final Integer loginType = MapUtils.getInteger(params, "type", -1);
        if (loginType == -1 || !LoginType.exist(loginType)) {
            return ResponseData.businessError("不支持的登陆方式");
        }
        String token = null;
        try {

            switch (loginType) {
                //用户名密码登陆
                case 0:
                    final String un = MapUtils.getString(params, "username");
                    final String pwd = MapUtils.getString(params, "password");
                    token = login(un, pwd, properties.getAuthType());
                    break;
                default:
                    return ResponseData.error("不支持的登陆方式");
            }

        } catch (UnknownAccountException e) {
            throw e;
        } catch (AccountException e) {
            return ResponseData.businessError(e.getMessage());
        }
        Map<String, Object> tokenMap = new HashMap<>();
        tokenMap.put("token", token);

        return ResponseData.success(tokenMap);
    }

    private String login(String username, String password, AuthType authType) {
        Subject subject = SecurityUtils.getSubject();
        if (authType == AuthType.SESSION) {
            subject.login(new UsernamePasswordToken(username, password));
            return String.valueOf(SecurityUtils.getSubject().getSession().getId());
        }
        if (authType == AuthType.JWT) {
            UserPrincipal user = accountService.login(username, password);
            String jwt = JwtUtil.create(JSONObject.parseObject(JSONObject.toJSONString(user), new TypeReference<Map<String, ?>>() {
            }));
            subject.login(new BearerToken(jwt));

            return jwt;
        }

        return "";
    }

    @Override
    public boolean checkAuthExists(String openid, LoginType loginType) {
        UserAuth userAuth = new UserAuth();
        userAuth.setOpenid(openid);
        userAuth.setLoginType(loginType.name());
        Integer count = this.sqlSession.selectOne("user.checkAuthExists", userAuth);
        return count > 0;
    }

    @Override
    @Transactional
    public UserAuth addUserAuth(UserAuth userAuth) {
        this.sqlSession.insert("user.addUserAuth", userAuth);
        return userAuth;
    }

    @Override
    @Transactional
    public void updateUserAuth(UserAuth userAuth) {
        this.sqlSession.update("user.updateUserAuth", userAuth);
    }

    @Override
    public UserAuth qryUserAuth(String openId, LoginType loginType) {
        UserAuth userAuth = new UserAuth();
        userAuth.setOpenid(openId);
        userAuth.setLoginType(loginType.name());
        return this.sqlSession.selectOne("user.qryUserAuth", userAuth);
    }

    private User addNewUser(String phone) {
        String defaultUsername = CommonUtil.getRandomString(8);
        User user = new User();
        user.setPhone(phone);
        user.setUserName(defaultUsername);
        this.sqlSession.insert("user.addUser", user);
        Map<String, List<Object>> attr = new HashMap<>();
        //attr.put("USER_BALANCE", Collections.singletonList(0));
        //attr.put("USER_WITHDRAWN_AMOUNT", Collections.singletonList(0));
        userService.upsertUserAttr(user.getUserId(), attr);

        //异步初始化其他用户信息
        ThreadUtil.submit(() -> {


        });

        return user;
    }
}
