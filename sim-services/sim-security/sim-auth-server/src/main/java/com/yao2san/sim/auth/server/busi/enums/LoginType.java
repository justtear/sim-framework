package com.yao2san.sim.auth.server.busi.enums;

import lombok.Getter;

public enum LoginType {
    /**
     * 用户名登录
     */
    UN(0),

    /**
     * 手机号登录
     */
    PHONE(2),

    /**
     * 邮箱登录
     */
    EMAIL(3),

    /**
     * 微信登录
     */
    WX(4),

    /**
     * 微信小程序登录
     */
    WX_XCX(5);
    @Getter
    private Integer value;


    LoginType(Integer value) {
        this.value = value;
    }

    public static boolean exist(Integer v) {
        boolean pass = false;
        for (LoginType type : LoginType.values()) {
            if (type.getValue().equals(v)) {
                pass = true;
            }
        }
        return pass;
    }
}
