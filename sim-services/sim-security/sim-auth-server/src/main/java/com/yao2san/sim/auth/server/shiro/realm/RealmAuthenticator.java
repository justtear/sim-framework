package com.yao2san.sim.auth.server.shiro.realm;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.authc.pam.UnsupportedTokenException;
import org.apache.shiro.realm.Realm;

import java.util.Collection;

public class RealmAuthenticator extends ModularRealmAuthenticator {

    @Override
    protected AuthenticationInfo doMultiRealmAuthentication(Collection<Realm> realms, AuthenticationToken token) {
        Realm realm = null;
        for (Realm r : realms) {
            if (r.supports(token)) {
                realm = r;
                break;
            }
        }
        if (realm == null) {
            throw new UnsupportedTokenException();
        }
        return realm.getAuthenticationInfo(token);
    }
}