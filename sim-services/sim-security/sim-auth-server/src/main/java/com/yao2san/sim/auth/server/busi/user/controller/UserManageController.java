package com.yao2san.sim.auth.server.busi.user.controller;

import com.yao2san.sim.auth.server.busi.user.bean.Permission;
import com.yao2san.sim.auth.server.busi.user.bean.User;
import com.yao2san.sim.auth.server.busi.user.bean.request.ResetPwdReq;
import com.yao2san.sim.auth.server.busi.user.service.UserManageService;
import com.yao2san.sim.framework.web.response.ResponseData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@RequestMapping("userManage")
@Api(tags = "用户管理")
public class UserManageController {
    @Autowired
    private UserManageService userManageService;

    @ApiOperation(value = "查询用户列表")
    @GetMapping
    public ResponseData userList(User user) {
        return userManageService.qryUserList(user);
    }

    @GetMapping("{userId}")
    public ResponseData detail(@PathVariable Long userId) {
        return userManageService.detail(userId);
    }

    @GetMapping("permissions")
    public ResponseData qryUserPermissions(Permission permission) {
        return userManageService.qryUserPermissions(permission);
    }

    @PostMapping("permissions")
    public ResponseData addOrUpdateUserPermissions(@RequestBody Map<String, Object> params) {
        return userManageService.addOrUpdateUserPermissions(params);
    }

    @GetMapping("roles")
    public ResponseData qryUserRoles(User user) {
        return userManageService.qryUserRoles(user);
    }

    @PostMapping("roles")
    public ResponseData addOrUpdateUserRoles(@RequestBody Map<String, Object> params) {
        return userManageService.addOrUpdateUserRoles(params);
    }

    /**
     * 新增用户
     */
    @PostMapping
    public ResponseData<Void> addUser(@RequestBody @Validated(User.Add.class) User user) {
        return userManageService.addUser(user);
    }

    /**
     * 修改用户
     */
    @PatchMapping
    public ResponseData updateUser(@RequestBody @Validated(User.Update.class) User user) {
        return userManageService.updateUser(user);
    }

    /**
     * 上传用户头像
     */
    @PostMapping("avatar")
    public ResponseData<String> uploadAvatar(@RequestPart("file") MultipartFile multipartFile) {
        return userManageService.uploadAvatar(multipartFile);
    }

    /**
     * 重置用户密码
     */
    @PatchMapping("/resetPwd")
    public ResponseData<Void> resetPwd(@RequestBody @Validated ResetPwdReq req) {
        userManageService.resetPwd(req);
        return ResponseData.success();
    }

}
