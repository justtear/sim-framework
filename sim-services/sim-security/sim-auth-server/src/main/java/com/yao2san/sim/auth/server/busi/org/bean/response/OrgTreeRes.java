package com.yao2san.sim.auth.server.busi.org.bean.response;

import cn.hutool.core.lang.tree.TreeNode;
import com.yao2san.sim.auth.server.busi.org.bean.request.OrgAttr;
import lombok.Data;

import java.util.List;

/**
 * @author wxg
 */
@Data
public class OrgTreeRes extends TreeNode {
    private Long orgId;
    private Long parentId;
    private String orgName;
    private String orgCode;
    private String orgType;
    private String description;

    private List<OrgAttr> attrs;
}
