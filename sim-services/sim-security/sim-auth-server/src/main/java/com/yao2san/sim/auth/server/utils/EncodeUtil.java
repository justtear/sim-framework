package com.yao2san.sim.auth.server.utils;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.util.ByteSource;

public class EncodeUtil {
    public static String encodePwd(String username, String pwd) {
        ByteSource salt = ByteSource.Util.bytes(username);
        Object result = new Sha256Hash(pwd, salt, 1024);
        return result.toString();
    }

    public static ByteSource getSalt(String username) {
        return ByteSource.Util.bytes(username);
    }
}
