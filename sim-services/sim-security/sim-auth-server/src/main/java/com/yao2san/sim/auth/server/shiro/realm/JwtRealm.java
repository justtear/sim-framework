package com.yao2san.sim.auth.server.shiro.realm;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.yao2san.sim.auth.common.JwtUtil;
import com.yao2san.sim.auth.common.UserPrincipal;
import com.yao2san.sim.framework.web.response.ResponseCode;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.utils.ServletUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtRealm extends AuthorizingRealm {
    private final static String REALM_NAME = "token";

    private final boolean isVerify;

    public JwtRealm(boolean isVerify) {
        this.isVerify = isVerify;
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof BearerToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //从认证主体中取出角色和权限信息
        UserPrincipal userPrincipal = (UserPrincipal) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(userPrincipal.getRoles());
        authorizationInfo.setStringPermissions(userPrincipal.getPermissions());
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        try {
            BearerToken token = (BearerToken) authenticationToken;

            String jwtToken = token.getToken();
            UserPrincipal user;
            if (isVerify) {
                DecodedJWT jwt = JwtUtil.verify(jwtToken);
                byte[] decode = Base64.getDecoder().decode(jwt.getPayload());
                user = JSONObject.parseObject(decode, UserPrincipal.class);
            } else {
                user = JSONObject.parseObject(JwtUtil.getPayload(jwtToken), UserPrincipal.class);
            }
            return new SimpleAuthenticationInfo(user, jwtToken, getName());
        } catch (ShiroException | JWTVerificationException e) {
            log.error("签名验证失败", e);
            PrintWriter writer;
            try {
                HttpServletResponse response = ServletUtil.getHttpServletResponse();
                response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
                writer = response.getWriter();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            writer.write(JSONObject.toJSONString(ResponseData.error(ResponseCode.ILLEGAL_ARGUMENT, "签名验证失败")));
        }

        return null;
    }

    @Override
    public String getAuthorizationCacheName() {
        return REALM_NAME;
    }

    public static void main(String[] args) {
        Map map = new HashMap();
        map.put("id", 123);
        map.put("userId", 123);
        System.out.println(JwtUtil.create(map, "DEFAULT_SECRET_KEY", 3600));
    }

    @Override
    public boolean isCachingEnabled() {
        return false;
    }
}
