package com.yao2san.sim.auth.server.busi.user.bean;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PermissionSimplify {
    private Long purviewId;
    private String purviewName;
    private String purviewCode;
    /**
     * 权限类型 1菜单权限 2接口权限 3页面元素
     */
    private String purviewType;
    private String url;
    private String icon;
    private String component;
    private Long  parentId;
    private String effDate;
    private String expDate;
    private Integer orderNum;
    private Boolean hidden;
    private String path;

}
