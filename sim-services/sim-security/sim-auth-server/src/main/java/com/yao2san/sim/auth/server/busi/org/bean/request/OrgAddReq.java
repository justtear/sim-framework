package com.yao2san.sim.auth.server.busi.org.bean.request;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrgAddReq extends BaseBean {
    @NotNull(message = "parentId不能为空")
    private Long parentId;
    @NotEmpty(message = "orgName不能为空")
    private String orgName;
    @NotEmpty(message = "orgName不能为空")
    private String orgCode;
    @NotEmpty(message = "orgCode不能为空")
    private String orgType;

    private String description;

    private List<OrgAttr> attrs;
}
