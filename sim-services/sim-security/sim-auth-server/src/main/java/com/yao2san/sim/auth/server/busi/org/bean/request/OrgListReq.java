package com.yao2san.sim.auth.server.busi.org.bean.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrgListReq extends Pagination {
    private Long parentId;
    private String status;
    private String orgType;
    private String filterText;
}
