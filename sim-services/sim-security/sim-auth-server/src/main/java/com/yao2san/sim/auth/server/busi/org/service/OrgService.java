package com.yao2san.sim.auth.server.busi.org.service;

import cn.hutool.core.lang.tree.Tree;
import com.github.pagehelper.PageInfo;
import com.yao2san.sim.auth.server.busi.org.bean.request.OrgAddReq;
import com.yao2san.sim.auth.server.busi.org.bean.request.OrgListReq;
import com.yao2san.sim.auth.server.busi.org.bean.request.OrgUpdateReq;
import com.yao2san.sim.auth.server.busi.org.bean.response.OrgDetailRes;
import com.yao2san.sim.auth.server.busi.org.bean.response.OrgListRes;
import com.yao2san.sim.framework.web.response.ResponseData;

import java.util.List;
/**
 * @author wxg
 */
public interface OrgService {
    /**
     * list of org
     */
    ResponseData<PageInfo<OrgListRes>> list(OrgListReq req);
    ResponseData<List<Tree<Long>>> tree(OrgListReq req);

    /**
     * detail of org
     */
    ResponseData<OrgDetailRes> detail(Long orgId);

    /**
     * add one org
     */
    ResponseData<Void> add(OrgAddReq req);

    /**
     * update an org
     */
    ResponseData<Void> update(OrgUpdateReq req);

    /**
     * delete one org
     */
    ResponseData<Void> delete(List<Long> orgIds);

}
