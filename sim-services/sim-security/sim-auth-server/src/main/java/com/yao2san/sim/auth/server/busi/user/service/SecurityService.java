package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.auth.server.busi.enums.LoginType;
import com.yao2san.sim.auth.server.busi.user.bean.UserAuth;
import com.yao2san.sim.framework.web.response.ResponseData;

import java.util.Map;

public interface SecurityService {

    /**
     * 用户登录
     *
     * @param params params
     * @return 登录成功返回token
     */
    ResponseData<Map<String, Object>> login(Map<String, Object> params);

    boolean checkAuthExists(String openid, LoginType loginType);

    UserAuth addUserAuth(UserAuth userAuth);

    void updateUserAuth(UserAuth userAuth);

    UserAuth qryUserAuth(String openId, LoginType loginType);
}
