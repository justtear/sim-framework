package com.yao2san.sim.auth.server.busi.org.bean.response;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrgDetailRes extends Pagination {
}
