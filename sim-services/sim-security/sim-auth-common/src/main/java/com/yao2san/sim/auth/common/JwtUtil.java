package com.yao2san.sim.auth.common;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Calendar;
import java.util.Map;
import java.util.UUID;

/**
 * @author wxg
 **/
@Component
public class JwtUtil {

    private static AuthProperties properties;

    @Autowired
    public void setProperties(AuthProperties properties) {
        JwtUtil.properties = properties;
    }

    public static String create(Map<String, ?> payload) {
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.SECOND, properties.getJwt().getTtl());
        JWTCreator.Builder builder = JWT.create().
                withPayload(payload).
                withExpiresAt(instance.getTime()).
                withJWTId(UUID.randomUUID().toString());
        return builder.sign(Algorithm.HMAC256(properties.getJwt().getSecret()));
    }

    public static String create(Map<String, ?> payload, String secret, int ttl) {
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.SECOND, ttl);
        JWTCreator.Builder builder = JWT.create()
                .withPayload(payload)
                .withExpiresAt(instance.getTime())
                .withJWTId(UUID.randomUUID().toString());
        return builder.sign(Algorithm.HMAC256(secret));
    }

    public static DecodedJWT verify(String token) {
        Algorithm algorithm = Algorithm.HMAC256(properties.getJwt().getSecret());
        JWTVerifier verifier = JWT.require(algorithm).build();
        return verifier.verify(token);
    }

    public static DecodedJWT verify(String token, String secret) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        JWTVerifier verifier = JWT.require(algorithm).build();
        return verifier.verify(token);
    }

    public static String getPayload(String jwtToken) {
        return new String(Base64.getDecoder().decode(JWT.decode(jwtToken).getPayload()));
    }
}
