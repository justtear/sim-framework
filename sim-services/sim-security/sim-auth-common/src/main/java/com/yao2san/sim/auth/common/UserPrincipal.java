package com.yao2san.sim.auth.common;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class UserPrincipal implements Serializable {
    private String id;
    private String userId;
    private String userName;
    private String avatar;
    private String type;
    private Set<String> roles;
    private Set<String> permissions;
}
