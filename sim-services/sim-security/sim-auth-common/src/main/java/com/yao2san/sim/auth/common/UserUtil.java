package com.yao2san.sim.auth.common;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;

@Slf4j
public class UserUtil {
    public static Long getCurrUserId() {
        return Long.valueOf(getCurrUser().getUserId());
    }

    public static UserPrincipal getCurrUser() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityUtils.getSubject().getPrincipal();
        if (userPrincipal != null) {
            return userPrincipal;
        }
        return (UserPrincipal) SecurityUtils.getSubject().getPrincipal();
    }
}
