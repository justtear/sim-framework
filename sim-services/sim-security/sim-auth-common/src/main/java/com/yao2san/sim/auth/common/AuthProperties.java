package com.yao2san.sim.auth.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * 程序配置
 */
@Configuration
@ConfigurationProperties(prefix = "sim.auth")
@Data
public class AuthProperties {
    /**
     * 是否启用鉴权 默认false 仅在网关服务端时才为true。其他程序集成时需置为false
     */
    private Boolean enable = false;

    private Model model = Model.CLIENT;

    private String loginUrl;

    private String unauthorizedUrl;

    private String successUrl;

    private AuthType authType = AuthType.SESSION;

    private List<String> whiteList = new ArrayList<>();

    private Jwt jwt = new AuthProperties.Jwt(DEFAULT_SECRET_KEY, 3600, false);

    private final static String DEFAULT_SECRET_KEY = "DEFAULT_SECRET_KEY";

    @Data
    @Configuration
    @NoArgsConstructor
    @AllArgsConstructor
    @ConfigurationProperties(prefix = "sim.auth.jwt")
    public static class Jwt {
        private String secret = DEFAULT_SECRET_KEY;
        private int ttl = 3600;
        private boolean verify = false;
    }

    public enum Model {
        CLIENT, SERVER
    }
}
