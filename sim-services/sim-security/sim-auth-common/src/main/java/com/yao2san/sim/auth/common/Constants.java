package com.yao2san.sim.auth.common;

/**
 * @author wxg
 **/
public class Constants {
    public static final String[] DEFAULT_WHITE_LIST = {
            "/service/**",
            "/",
            "/images/**",
            "/**/security/login",
            "/**/security/logout",
            "/**/user/token",
            "/**/wx/**",
            "/**/static/**",
            "/**/404",
            "/**/error",
            "/favicon.ico",
            "/**/*.js",
            "/**/*.css",
            "/**/oauth/authenticate",
            "/**/oauth/token",
            "/**/eureka/**",
            "/**/eureka-web/**",
            "/eureka/**",
            "/swagger-resources/**",
            "/**/storage/config",
            "/**/storage/callback",
            "/**/*.tff",
            "/**/*.woff",
            "/**/*.woff2"
    };

    /**
     * 访客过滤器(内置)
     */
    public final static String ANYONE_FILTER = "anon";

    /**
     * 认证过滤器(内置)
     */
    public final static String AUTH_FILTER = "authc";

    /**
     * 无会话状态过滤器
     */
    public final static String NO_SESSION_FILTER = "noSessionCreation";

    /**
     * Bearer Token过滤器(用于JWT)
     */
    public final static String BEARER_FILTER = "bearer";

    /**
     * 权限过滤器
     */
    public final static String PERMISSION_FILTER = "permission";

    public final static String JWT_FILTER = "jwt";
    public final static String JWT_WITH_NO_VERIFY_FILTER = "jwtWithNoVerify";


    public static final String CACHED_INTERFACE_PERMISSION_KEY = "auth:permission:interface";
    public static final String CACHED_ALL_PERMISSION_KEY = "auth:permission:all";

    public final static String TOKEN_KEY_PREFIX = "SIM:OAUTH:TOKEN:";

}
