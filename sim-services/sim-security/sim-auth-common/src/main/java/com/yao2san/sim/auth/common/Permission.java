package com.yao2san.sim.auth.common;

import lombok.Data;

@Data
public class Permission {

    private String purviewName;
    private String purviewCode;
    private String url;
}
