package com.yao2san.sim.auth.client.shiro.filter;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.yao2san.sim.auth.common.Permission;
import com.yao2san.sim.auth.common.UserPrincipal;
import com.yao2san.sim.framework.cache.utils.CacheUtil;
import com.yao2san.sim.framework.web.response.ResponseCode;
import com.yao2san.sim.framework.web.response.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.util.AntPathMatcher;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.yao2san.sim.auth.common.Constants.CACHED_INTERFACE_PERMISSION_KEY;

/**
 * 权限校验
 */
@Slf4j
public class PermissionFilter extends AuthorizationFilter {
    private static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        Subject subject = getSubject(request, response);

        UserPrincipal principal = (UserPrincipal) subject.getPrincipal();

        if (principal == null) {
            return false;
        }

        Set<String> permissions = principal.getPermissions();

        String requestURL = ((HttpServletRequest) request).getRequestURI();

        List<Permission> urlPermissions = new ArrayList<>();
        List<Permission> allInterfacePermissions = CacheUtil.get(CACHED_INTERFACE_PERMISSION_KEY);
        if (allInterfacePermissions == null) {
            allInterfacePermissions = Collections.emptyList();
        }
        boolean match;
        for (Permission permission : allInterfacePermissions) {
            match = PATH_MATCHER.match(permission.getUrl(), requestURL);
            if (match) {
                urlPermissions.add(permission);
                break;
            }
        }

        return permissions.containsAll(urlPermissions.stream().map(Permission::getPurviewCode).collect(Collectors.toSet()));
    }

    /**
     * When isAccessAllowed() = false execute
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
        HttpServletResponse httpServletResponse = WebUtils.toHttp(response);

        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json; charset=utf-8");
        try (PrintWriter out = httpServletResponse.getWriter()) {
            String message = JSONObject.toJSONString(ResponseData.error(ResponseCode.ACCESS_RESTRICTED, "Unauthorized access"));
            httpServletResponse.setStatus(403);
            out.append(message);
            log.error(message);
        } catch (IOException e) {
            log.error("", e);
        }
        return false;
    }
}
