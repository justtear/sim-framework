package com.yao2san.sim.auth.client.config;

import com.yao2san.sim.auth.client.shiro.filter.PermissionFilter;
import com.yao2san.sim.auth.client.shiro.realm.DefaultRealm;
import com.yao2san.sim.auth.client.shiro.realm.JwtRealm;
import com.yao2san.sim.auth.client.shiro.realm.RealmAuthenticator;
import com.yao2san.sim.auth.client.shiro.realm.UsernamePasswordRealm;
import com.yao2san.sim.auth.common.AuthProperties;
import com.yao2san.sim.auth.common.AuthType;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.Authenticator;
import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.mgt.*;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.MemorySessionDAO;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.authc.BearerHttpAuthenticationFilter;
import org.apache.shiro.web.filter.session.NoSessionCreationFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.DefaultWebSubjectFactory;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.cache.CacheType;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.servlet.Filter;
import java.util.*;

import static com.yao2san.sim.auth.common.Constants.*;

@Configuration
@Import({AuthProperties.class,CacheProperties.class})
@Slf4j
@ConditionalOnProperty(prefix = "sim.auth", name = "model", havingValue = "client", matchIfMissing = true)
public class ShiroConfig {

    private final AuthProperties properties;
    private final RedisSessionDAO redisSessionDAO;
    private final RedisCacheManager redisCacheManager;
    private final CacheProperties cacheProperties;

    public ShiroConfig(AuthProperties properties, RedisSessionDAO redisSessionDAO, RedisCacheManager redisCacheManager, CacheProperties cacheProperties) {
        this.properties = properties;
        this.redisSessionDAO = redisSessionDAO;
        this.redisCacheManager = redisCacheManager;
        this.cacheProperties = cacheProperties;
    }

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        //未登录时重定向url
        shiroFilterFactoryBean.setLoginUrl(properties.getLoginUrl());
        //未授权访问重定向url
        shiroFilterFactoryBean.setUnauthorizedUrl(properties.getUnauthorizedUrl());
        //登录成功重定向url
        shiroFilterFactoryBean.setSuccessUrl(properties.getSuccessUrl());
        //添加过滤器
        shiroFilterFactoryBean.setFilters(getFilters(properties.getAuthType()));
        //过滤器路径映射配置
        shiroFilterFactoryBean.setFilterChainDefinitionMap(getFilterChainDefinitionMap());

        return shiroFilterFactoryBean;

    }

    @Bean
    public Authenticator authenticator(List<Realm> realms) {
        RealmAuthenticator authenticator = new RealmAuthenticator();
        authenticator.setRealms(realms);
        authenticator.setAuthenticationStrategy(new AtLeastOneSuccessfulStrategy());
        return authenticator;
    }

    @Bean
    public SessionsSecurityManager securityManager(SessionManager sessionManager, List<Realm> realms, Authenticator authenticator, SubjectDAO subjectDAO) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setSessionManager(sessionManager);
        securityManager.setRealms(realms);
        securityManager.setAuthenticator(authenticator);

        AuthType authType = properties.getAuthType();
        if (authType == AuthType.SESSION) {
            securityManager.setCacheManager(redisCacheManager);
        } else if (authType == AuthType.JWT) {
            securityManager.setSubjectDAO(subjectDAO);
            securityManager.setRememberMeManager(null);
            securityManager.setCacheManager(null);
        }

        return securityManager;
    }

    private Map<String, Filter> getFilters(AuthType authType) {

        Map<String, Filter> filters = new LinkedHashMap<>();
        if (authType == AuthType.SESSION) {
            filters.put(PERMISSION_FILTER, new PermissionFilter());
        } else if (authType == AuthType.JWT) {
            filters.put(NO_SESSION_FILTER, new NoSessionCreationFilter());
            filters.put(BEARER_FILTER, new BearerHttpAuthenticationFilter());
            filters.put(PERMISSION_FILTER, new PermissionFilter());
        }
        return filters;
    }

    private Map<String, String> getFilterChainDefinitionMap() {
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        if (!properties.getEnable()) {
            filterChainDefinitionMap.put("/**", ANYONE_FILTER);
            return filterChainDefinitionMap;
        }
        //默认白名单 不做认证
        for (String s : DEFAULT_WHITE_LIST) {
            filterChainDefinitionMap.put(s, ANYONE_FILTER);
        }

        //自定义白名单 不做认证
        List<String> whiteList = properties.getWhiteList();
        Map<String, String> whiteMap = new HashMap<>();
        whiteList.forEach(v -> whiteMap.put(v, ANYONE_FILTER));
        filterChainDefinitionMap.putAll(whiteMap);

        //如果白名单配置包含*，则全部放行
        if (whiteList.contains("*")) {
            filterChainDefinitionMap.put("/**", ANYONE_FILTER);
            log.warn("The white-list config contain '*', all services will be accessible without authentication!");
        } else {
            AuthType authType = properties.getAuthType();
            if (authType == AuthType.SESSION) {
                filterChainDefinitionMap.put("/**", String.join(",", PERMISSION_FILTER));
            } else if (authType == AuthType.JWT) {
                filterChainDefinitionMap.put("/**", String.join(",", NO_SESSION_FILTER, BEARER_FILTER, PERMISSION_FILTER));
            } else {
                throw new IllegalArgumentException("Unsupported auth type:" + authType);
            }
        }

        return filterChainDefinitionMap;

    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "session", matchIfMissing = true)
    public UsernamePasswordRealm usernamePasswordRealm() {
        return new UsernamePasswordRealm();
    }


    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "jwt")
    public JwtRealm jwtRealm() {
        return new JwtRealm(properties.getJwt().isVerify());
    }

    @Bean
    @ConditionalOnMissingBean(Realm.class)
    public DefaultRealm defaultRealm() {
        return new DefaultRealm();
    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "jwt")
    public DefaultWebSubjectFactory subjectFactory() {
        return new NoSessionSubjectFactory();
    }


    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "jwt")
    protected SessionStorageEvaluator sessionStorageEvaluator() {
        DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
        defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
        return defaultSessionStorageEvaluator;
    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "jwt")
    public SubjectDAO subjectDAO(SessionStorageEvaluator sessionStorageEvaluator) {
        DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
        subjectDAO.setSessionStorageEvaluator(sessionStorageEvaluator);
        return subjectDAO;
    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "jwt")
    public SessionManager sessionManagerForJwt() {
        return new DefaultWebSessionManager();
    }

    @Bean
    @ConditionalOnProperty(prefix = "sim.auth", name = "auth-type", havingValue = "session", matchIfMissing = true)
    public SessionManager sessionManagerForSession() {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        if (CacheType.SIMPLE.equals(cacheProperties.getType())) {
            sessionManager.setSessionDAO(new MemorySessionDAO());
        } else {
            sessionManager.setGlobalSessionTimeout(-1);
            sessionManager.setSessionDAO(redisSessionDAO);
        }
        return sessionManager;
    }


}
