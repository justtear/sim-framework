package com.yao2san.sim.auth.client.config;

import com.yao2san.sim.auth.common.AuthProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
@Configuration
@Import(AuthProperties.class)
@ComponentScan({"com.yao2san.sim.auth.client","com.yao2san.sim.auth.common"})
public class AuthClientConfig {
}
