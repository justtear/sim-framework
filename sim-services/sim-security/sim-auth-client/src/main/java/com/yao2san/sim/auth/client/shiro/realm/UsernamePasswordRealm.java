package com.yao2san.sim.auth.client.shiro.realm;

import com.yao2san.sim.auth.common.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

@Slf4j
public class UsernamePasswordRealm extends AuthorizingRealm {
    private final static String REALM_NAME = "user";

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof UsernamePasswordToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //从认证主体中取出角色和权限信息
        UserPrincipal userPrincipal = (UserPrincipal) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(userPrincipal.getRoles());
        authorizationInfo.setStringPermissions(userPrincipal.getPermissions());
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //该方法作不会被调用
        //缓存中已经存在认证后的session信息，作为客户端集成时无需再次认证
        return null;
    }

    @Override
    public String getAuthorizationCacheName() {
        return REALM_NAME;
    }
}
