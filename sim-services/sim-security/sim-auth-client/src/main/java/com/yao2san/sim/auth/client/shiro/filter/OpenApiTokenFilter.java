package com.yao2san.sim.auth.client.shiro.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.util.AntPathMatcher;


@Slf4j
public class OpenApiTokenFilter extends AuthorizingRealm {
    private static final AntPathMatcher MATCHER = new AntPathMatcher();
    /**
     * 对外接口访问前缀 只有该路径下的请求才会被拦截
     */
    private static final String PATTERN = "/service/**";


    private static final String[] WHITE_LIST = {"/service/**/oauth/token"};

    @Override
    public boolean supports(AuthenticationToken token) {
        return super.supports(token);
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        return null;
    }
}
