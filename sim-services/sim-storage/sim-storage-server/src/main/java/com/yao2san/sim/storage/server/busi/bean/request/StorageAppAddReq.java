package com.yao2san.sim.storage.server.busi.bean.request;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

@Data
@EqualsAndHashCode(callSuper = true)
public class StorageAppAddReq extends BaseBean {
    private String openId;
    private String secretKey;
    @NotEmpty
    private String appName;
    @NotEmpty
    private String storageType;
    @NotEmpty
    private String endpoint;
    private String bucket;
    private String region;
    private String ak;
    private String sk;
    private String path;

}
