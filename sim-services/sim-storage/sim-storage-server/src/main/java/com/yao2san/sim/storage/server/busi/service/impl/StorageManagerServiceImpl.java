package com.yao2san.sim.storage.server.busi.service.impl;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.storage.server.busi.bean.request.FileUploadReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageObjectReq;
import com.yao2san.sim.storage.server.busi.bean.response.StorageConfigRes;
import com.yao2san.sim.storage.server.busi.bean.response.StorageObjectRes;
import com.yao2san.sim.storage.server.busi.service.StorageManagerService;
import com.yao2san.sim.storage.client.config.SimStorageClientProperties;
import com.yao2san.sim.storage.client.core.api.bean.response.GetStorageConfigRes;
import com.yao2san.sim.storage.client.core.auth.Credentials;
import com.yao2san.sim.storage.client.core.enums.StorageType;
import com.yao2san.sim.storage.client.core.integrate.IntegrateConfig;
import com.yao2san.sim.storage.client.core.integrate.IntegrateStorageManager;
import com.yao2san.sim.storage.client.core.integrate.IntegrateUploader;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wxg
 **/

@Service
public class StorageManagerServiceImpl extends BaseServiceImpl implements StorageManagerService {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public GetStorageConfigRes getConfig(String openId) {
        StorageConfigRes res = this.sqlSession.selectOne("openApi.qryStorageConfig", openId);
        if (res == null) {
            throw new BusiException("未找到应用,openId=" + openId);
        }
        StorageType storageType = StorageType.valueOf(StorageType.class, res.getStorageType());

        SimStorageClientProperties properties = new SimStorageClientProperties();
        properties.setType(storageType);

        IntegrateConfig integrateConfig = new IntegrateConfig();
        BeanUtils.copyProperties(res, integrateConfig);
        integrateConfig.setStorageType(storageType);

        IntegrateStorageManager manager = new IntegrateStorageManager(properties, integrateConfig);

        Credentials credentials = manager.credentials(res.getStsDurationSeconds());

        GetStorageConfigRes config = new GetStorageConfigRes();
        config.setStorageType(storageType);
        config.setBucket(res.getBucket());
        config.setRegion(res.getRegion());
        config.setEndpoint(res.getEndpoint());
        config.setPath(res.getPath());
        config.setCredentials(credentials);
        config.setOpenId(res.getOpenId());
        return config;
    }

    @Override
    public PageInfo<StorageObjectRes> listObject(StorageObjectReq req) {
        PageInfo<StorageObjectRes> pageInfo = this.qryList("storageObject.list", req);
        return pageInfo;
    }


    @Override
    public void upload(MultipartFile file, FileUploadReq req) {
        StorageConfigRes res = this.sqlSession.selectOne("openApi.qryStorageConfig", req.getOpenId());
        if (res == null) {
            throw new BusiException("未找到应用,openId=" + req.getOpenId());
        }
        StorageType storageType = StorageType.valueOf(StorageType.class, res.getStorageType());

        SimStorageClientProperties properties = new SimStorageClientProperties();
        properties.setType(storageType);

        IntegrateConfig integrateConfig = new IntegrateConfig();
        BeanUtils.copyProperties(res, integrateConfig);
        integrateConfig.setStorageType(storageType);

        IntegrateUploader uploader = new IntegrateUploader(properties,integrateConfig,applicationContext,null,null);

    }
/*    @EventListener
    public void onUpload() {

    }*/
}
