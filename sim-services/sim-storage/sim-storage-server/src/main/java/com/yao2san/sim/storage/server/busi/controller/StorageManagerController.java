package com.yao2san.sim.storage.server.busi.controller;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.storage.server.busi.bean.request.StorageObjectReq;
import com.yao2san.sim.storage.server.busi.bean.response.StorageObjectRes;
import com.yao2san.sim.storage.server.busi.service.OpenApiService;
import com.yao2san.sim.storage.server.busi.service.StorageManagerService;
import com.yao2san.sim.storage.client.core.api.bean.response.GetStorageConfigRes;
import com.yao2san.sim.storage.client.core.integrate.UploadNotify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;

/**
 * @author wxg
 **/

@RequestMapping("/manager")
@RestController
@CrossOrigin
public class StorageManagerController {
    @Autowired
    private StorageManagerService storageManagerService;

    @Autowired
    private OpenApiService openApiService;

    /**
     * 获取存储配置
     */
    @GetMapping("config")
    public ResponseData<GetStorageConfigRes> getConfig(@NotEmpty(message = "openId can not be empty") String openId) {
        return ResponseData.success(storageManagerService.getConfig(openId));
    }

    @PostMapping("notify")
    public ResponseData<UploadNotify> notify(@RequestBody UploadNotify req) {
        return ResponseData.success(openApiService.notify(req));
    }

    @GetMapping("object")
    public ResponseData<PageInfo<StorageObjectRes>> listObject(StorageObjectReq req) {
        PageInfo<StorageObjectRes> res = storageManagerService.listObject(req);
        return ResponseData.success(res);
    }
}
