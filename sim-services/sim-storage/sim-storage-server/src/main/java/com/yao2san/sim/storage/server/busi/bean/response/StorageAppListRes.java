package com.yao2san.sim.storage.server.busi.bean.response;

import lombok.Data;

/**
 * @author wxg
 **/
@Data
public class StorageAppListRes {
    private Long storageAppId;
    private String appName;
    private String openId;
    private String storageType;
    private String endpoint;
    private String bucket;
    private String path;
    private Long totalCapacity;
    private Long usedCapacity;
    private Long objectCount;
}
