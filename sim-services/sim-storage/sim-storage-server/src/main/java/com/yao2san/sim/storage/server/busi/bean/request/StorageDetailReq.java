package com.yao2san.sim.storage.server.busi.bean.request;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

/**
 * @author wxg
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class StorageDetailReq extends BaseBean {
    @NotEmpty
    private Long storageAppId;
}
