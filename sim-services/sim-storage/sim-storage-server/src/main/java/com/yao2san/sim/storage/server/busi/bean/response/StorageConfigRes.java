package com.yao2san.sim.storage.server.busi.bean.response;

import lombok.Data;

/**
 * @author wxg
 **/
@Data
public class StorageConfigRes {
    /**
     * 节点,如果是本地存储则为应用的地址
     */
    private String endpoint;
    /**
     * 区域
     */
    private String region;
    /**
     * 存储桶,如果是本地存储则为空
     */
    private String bucket;
    /**
     * 根路径,如果是本地存储则必填
     */
    private String path;
    /**
     * 存储类型,参见:SimStorageClientProperties.StorageType
     */
    private String storageType;
    /**
     * 第三方存储accessKey
     */
    private String ak;
    /**
     * 第三方存储secretKey
     */
    private String sk;
    /**
     * 临时凭证有效时间,单位:秒
     */
    private Long stsDurationSeconds = 1800L;

    /**
     * 角色,阿里OSS特有
     */
    private String roleArn;

    /**
     * 存储应用ID
     */
    private String openId;

}
