package com.yao2san.sim.storage.server.busi.service;

import com.yao2san.sim.storage.server.busi.bean.response.StorageConfigRes;
import com.yao2san.sim.storage.client.core.integrate.UploadNotify;

/**
 * @author wxg
 **/
public interface OpenApiService {
    /**
     * 获取存储配置
     *
     * @param openId    应用id
     * @param secretKey 应用密钥
     * @return 配置信息
     */
    StorageConfigRes getConfig(String openId, String secretKey);

    /**
     * 上传结果通知
     *
     * @param req 通知内容
     */
    UploadNotify notify(UploadNotify req);
}
