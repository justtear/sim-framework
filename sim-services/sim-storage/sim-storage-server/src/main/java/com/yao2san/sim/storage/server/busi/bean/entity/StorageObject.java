package com.yao2san.sim.storage.server.busi.bean.entity;

import lombok.Data;

/**
 * @author wxg
 **/
@Data
public class StorageObject {
    private Long storageObjectId;
    private String object;
    private String originalName;
    private Long size;
    private String url;
    private String contentType;
    private String suffix;
    private String openId;
    private String storageType;

}
