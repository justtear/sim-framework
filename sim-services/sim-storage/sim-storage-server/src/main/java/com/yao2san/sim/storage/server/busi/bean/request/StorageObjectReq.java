package com.yao2san.sim.storage.server.busi.bean.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

@Data
@EqualsAndHashCode(callSuper = true)
public class StorageObjectReq extends Pagination {
    @NotEmpty(message = "openId不能为空")
    private String openId;
}
