package com.yao2san.sim.storage.client.core.uploader;

import com.yao2san.sim.storage.client.core.event.UploadResult;
import com.yao2san.sim.storage.client.exception.UploadErrorException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface Uploader {
    UploadResult upload(String file, String dest) throws IOException, UploadErrorException;

    UploadResult upload(File file, String dest) throws IOException, UploadErrorException;

    UploadResult upload(MultipartFile file, String dest) throws IOException, UploadErrorException;

    UploadResult upload(InputStream stream, String dest) throws IOException, UploadErrorException;
    
    UploadResult upload(String file, String dest, UploadArgs args) throws IOException, UploadErrorException;

    UploadResult upload(File file, String dest, UploadArgs args) throws IOException, UploadErrorException;

    UploadResult upload(MultipartFile file, String dest, UploadArgs args) throws IOException, UploadErrorException;

    UploadResult upload(InputStream stream, String dest, UploadArgs args) throws IOException, UploadErrorException;

    String getBasePath();

    String getName();
}
