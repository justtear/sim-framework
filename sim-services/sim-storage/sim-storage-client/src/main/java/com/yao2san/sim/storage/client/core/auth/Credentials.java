package com.yao2san.sim.storage.client.core.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wxg
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Credentials {
    private String securityToken;
    private String accessKey;
    private String secretKey;
    private String expiration;
}
