package com.yao2san.sim.storage.client.core.policy;

import com.yao2san.sim.storage.client.core.integrate.UploadNotify;

/**
 * @author wxg
 **/
public interface UploadNotifyHandlerPolicy {
    /**
     * 通知到storage-server的前置处理
     * @param uploadNotify 通知参数
     * @return 暂无使用
     */
    Object before(UploadNotify uploadNotify);

    /**
     * 通知到storage-server的后置处理
     * @param uploadNotify 通知参数
     * @return 暂无使用
     */
    Object after(UploadNotify uploadNotify);
}
