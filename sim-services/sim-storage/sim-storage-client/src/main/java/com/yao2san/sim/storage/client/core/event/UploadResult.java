package com.yao2san.sim.storage.client.core.event;


import com.yao2san.sim.storage.client.core.uploader.UploadArgs;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@ToString
public class UploadResult {

    @Getter
    private boolean success;

    @Getter
    private String message;

    @Getter
    private String object;

    @Getter
    private Long size;

    @Getter
    private String url;

    @Getter
    private UploadArgs uploadArgs;

}
