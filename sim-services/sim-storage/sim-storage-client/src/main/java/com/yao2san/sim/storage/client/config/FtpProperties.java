package com.yao2san.sim.storage.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "sim.storage.ftp")
public class FtpProperties {
    private String host;
    private String port;
    private FtpType type;
    private String path;

    public enum FtpType {
        /**
         * 使用FTP
         */
        FTP,
        /**
         * 使用SFTP
         */
        SFTP
    }
}
