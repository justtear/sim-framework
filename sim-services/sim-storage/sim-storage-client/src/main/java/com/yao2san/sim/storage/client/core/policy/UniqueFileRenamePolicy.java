package com.yao2san.sim.storage.client.core.policy;

import org.apache.commons.io.FilenameUtils;

import java.util.UUID;

/**
 * @author wxg
 **/
public class UniqueFileRenamePolicy implements FileRenamePolicy {
    @Override
    public String rename(String name) {
        if (name == null) {
            return null;
        }
        String extension = FilenameUtils.getExtension(name);
        return UUID.randomUUID().toString().replace("-", "") + (extension.isEmpty() ? "" : ("." + extension));
    }

}
