package com.yao2san.sim.storage.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "sim.storage.minio")
public class MinioProperties {
    private String endpoint;
    private String bucket;
    private String path;
    private String accessKey;
    private String secretKey;
    private Boolean secure = false;
}
