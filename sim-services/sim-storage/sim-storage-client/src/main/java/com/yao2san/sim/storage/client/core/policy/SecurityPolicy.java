package com.yao2san.sim.storage.client.core.policy;

/**
 * @author wxg
 *
 * 安全策略
 **/
public interface SecurityPolicy {
    /**
     * 是否允许访问
     */
    boolean pass();
}
