package com.yao2san.sim.storage.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "sim.storage.integrate")
public class IntegrateProperties {
    /**
     * sim-storage-server地址
     */
    private String server;
    /**
     * 存储应用ID
     */
    private String openId;
    /**
     * 存储应用密钥
     */
    private String secretKey;

    /**
     * 客户端暴露的接口前缀,默认storage
     */
    private String prefix;

    /**
     * 自动获取配置的间隔时间,单位:秒. 默认-1即不自动更新
     */
    private int configFetchInterval = -1;


}
