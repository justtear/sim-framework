package com.yao2san.sim.storage.client.core.uploader;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.yao2san.sim.framework.utils.CommonUtil;
import com.yao2san.sim.storage.client.config.AliOssProperties;
import com.yao2san.sim.storage.client.core.enums.StorageType;
import com.yao2san.sim.storage.client.core.event.UploadEvent;
import com.yao2san.sim.storage.client.core.event.UploadResult;
import com.yao2san.sim.storage.client.core.listener.AliOssProgressListener;
import com.yao2san.sim.storage.client.core.policy.FolderPolicy;
import com.yao2san.sim.storage.client.exception.UploadErrorException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author wxg
 **/
public class AliOssUploader extends AbstractUploader {
    private final AliOssProperties aliOssProperties;
    private final OSS ossClient;
    private final ApplicationContext applicationContext;
    private final FolderPolicy folderPolicy;

    public AliOssUploader(OSS ossClient, AliOssProperties aliOssProperties, ApplicationContext applicationContext, FolderPolicy folderPolicy) {
        this.aliOssProperties = aliOssProperties;
        this.ossClient = ossClient;
        this.applicationContext = applicationContext;
        this.folderPolicy = folderPolicy;
    }

    @Override
    public UploadResult upload(InputStream stream, String dest, UploadArgs args) throws IOException, UploadErrorException {
        String object = getObject(dest, args);
        int size = stream.available();
        ObjectMetadata objectMetadata = new ObjectMetadata();
        if (args != null && args.size() > 0) {
            objectMetadata.setUserMetadata(args);
        }
        String endpoint = aliOssProperties.getEndpoint();
        String bucket = aliOssProperties.getBucket();
        PutObjectRequest request = new PutObjectRequest(bucket, object, stream, objectMetadata)
                .withProgressListener(new AliOssProgressListener());
        PutObjectResult putObjectResult = ossClient.putObject(request);
        //publish event
        UploadResult result = UploadResult.builder()
                .object(object)
                .size((long) size)
                .url(getUrl(endpoint, bucket, object))
                .uploadArgs(args)
                .success(true)
                .build();
        applicationContext.publishEvent(new UploadEvent(result));

        if (args != null && args.getCallback() != null) {
            args.getCallback().callback(result);
        }
        return result;
    }

    private String getObject(String dest, UploadArgs args) {
        String dir = this.folderPolicy.create();
        String name = FilenameUtils.getName(dest);
        String path = FilenameUtils.getPath(dest);
        if (args != null) {
            FolderPolicy fp = args.getFolderPolicy();
            if (fp != null) {
                dir = fp.create();
            }
        }
        String object = getBasePath() + "/" + dir + "/" + path + "/" + name;
        object = CommonUtil.formatPath(object);
        //OSS的object不能以/开头
        if (object.startsWith("/")) {
            object = object.substring(1);
        }
        return object;
    }

    private String getUrl(String endpoint, String bucket, String object) {
        return bucket + "." + endpoint + "/" + object;
    }

    @Override
    public String getBasePath() {
        return aliOssProperties.getPath();
    }

    @Override
    public String getName() {
        return StorageType.ALI_OSS.name();
    }
}
