package com.yao2san.sim.storage.client.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@ToString
@Configuration
@ConfigurationProperties(prefix = "sim.storage.tencent-cos")
public class TencentCosProperties {
    /**
     * 无需https
     */
    private String endpoint;
    private String region;
    private String bucket;
    private String path;
    private String accessKey;
    private String secretKey;
}
