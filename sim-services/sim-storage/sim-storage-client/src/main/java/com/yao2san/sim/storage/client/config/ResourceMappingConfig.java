package com.yao2san.sim.storage.client.config;

import com.yao2san.sim.framework.utils.CommonUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

@Configuration
@Import(LocalProperties.class)
@ConditionalOnProperty(prefix = "sim.storage", name = "type", havingValue = "local")
public class ResourceMappingConfig implements WebMvcConfigurer {

    private final LocalProperties localProperties;

    public ResourceMappingConfig(LocalProperties localProperties) {
        this.localProperties = localProperties;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(localProperties.getPrefix() + "/**")
                .addResourceLocations("file:" + CommonUtil.formatPath(localProperties.getBucket()) + File.separator);
    }
}