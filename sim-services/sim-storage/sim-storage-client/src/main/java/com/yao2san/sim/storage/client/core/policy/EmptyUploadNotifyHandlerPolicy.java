package com.yao2san.sim.storage.client.core.policy;

import com.yao2san.sim.storage.client.core.integrate.UploadNotify;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wxg
 **/
@Slf4j
public class EmptyUploadNotifyHandlerPolicy implements UploadNotifyHandlerPolicy {
    @Override
    public Object before(UploadNotify fileMeta) {
        log.debug("upload file callback:{}", fileMeta.toString());
        return null;
    }

    @Override
    public Object after(UploadNotify uploadNotify) {
        return null;
    }
}
