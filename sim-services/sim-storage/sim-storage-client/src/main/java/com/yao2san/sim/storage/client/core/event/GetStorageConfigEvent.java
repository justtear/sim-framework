package com.yao2san.sim.storage.client.core.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author wxg
 **/
public class GetStorageConfigEvent extends ApplicationEvent {


    public GetStorageConfigEvent(UploadResult source) {
        super(source);
    }

}