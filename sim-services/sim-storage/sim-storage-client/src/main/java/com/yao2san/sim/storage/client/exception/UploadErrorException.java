package com.yao2san.sim.storage.client.exception;

public class UploadErrorException extends Exception{
    public UploadErrorException() {
        super();
    }

    public UploadErrorException(String message) {
        super(message);
    }

    public UploadErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public UploadErrorException(Throwable cause) {
        super(cause);
    }

    protected UploadErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
