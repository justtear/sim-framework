package com.yao2san.sim.storage.client.core.event;

import org.springframework.context.ApplicationEvent;


public class UploadCallbackEvent extends ApplicationEvent {


    public UploadCallbackEvent(UploadResult source) {
        super(source);
    }

}