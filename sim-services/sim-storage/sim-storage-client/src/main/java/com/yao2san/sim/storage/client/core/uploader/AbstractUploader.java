package com.yao2san.sim.storage.client.core.uploader;


import com.yao2san.sim.storage.client.core.event.UploadResult;
import com.yao2san.sim.storage.client.exception.UploadErrorException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public abstract class AbstractUploader implements Uploader {


    @Override
    public UploadResult upload(String file, String dest) throws IOException, UploadErrorException {
        return upload(file, dest, null);
    }


    @Override
    public UploadResult upload(File file, String dest) throws IOException, UploadErrorException {
        return upload(file, dest, null);
    }


    @Override
    public UploadResult upload(MultipartFile file, String dest) throws IOException, UploadErrorException {
        return upload(file.getInputStream(), dest, null);
    }


    @Override
    public UploadResult upload(InputStream stream, String dest) throws IOException, UploadErrorException {
        return upload(stream, dest, null);
    }


    @Override
    public UploadResult upload(String file, String dest, UploadArgs args) throws IOException, UploadErrorException {
        return upload(new File(file), dest, args);
    }


    @Override
    public UploadResult upload(File file, String dest, UploadArgs args) throws IOException, UploadErrorException {
        return upload(Files.newInputStream(file.toPath()), dest, args);
    }


    @Override
    public UploadResult upload(MultipartFile file, String dest, UploadArgs args) throws IOException, UploadErrorException {
        return upload(file.getInputStream(), dest, args);
    }


    @Override
    public abstract UploadResult upload(InputStream stream, String dest, UploadArgs args) throws IOException, UploadErrorException;

    @Override
    public abstract String getBasePath();
}
