package com.yao2san.sim.storage.client.core.uploader;

import com.yao2san.sim.storage.client.core.event.UploadResult;

@FunctionalInterface
public interface UploadCallback {
    void callback(UploadResult result);
}
