package com.yao2san.sim.storage.client.core.uploader;

import com.yao2san.sim.storage.client.core.enums.StorageType;
import com.yao2san.sim.storage.client.core.event.UploadResult;
import com.yao2san.sim.storage.client.exception.UploadErrorException;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author wxg
 **/
public class QiNiuKodoUploader extends AbstractUploader{
    @Override
    public UploadResult upload(InputStream stream, String dest, UploadArgs args) throws IOException, UploadErrorException {
        return null;
    }

    @Override
    public String getBasePath() {
        return null;
    }

    @Override
    public String getName() {
        return StorageType.QI_NIU.name();
    }
}
