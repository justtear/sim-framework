package com.yao2san.sim.storage.client.core.policy;

import lombok.extern.slf4j.Slf4j;

/**
 * 文件夹生成策略:默认策略,无
 **/
@Slf4j
public class EmptyFolderPolicy implements FolderPolicy {
    @Override
    public String create() {
        return "";
    }
}
