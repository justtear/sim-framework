package com.yao2san.sim.storage.client.core.policy;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

/**
 * 文件夹生成策略:按天生成
 **/
@Slf4j
public class DayFolderPolicy implements FolderPolicy {
    private static final String YYYYMMDD = "yyyyMMdd";

    @Override
    public String create() {
        return "/" + DateFormatUtils.format(new Date(), YYYYMMDD)+"/";
    }
}
