package com.yao2san.sim.storage.client.config;

import java.util.TimeZone;

/**
 * @author wxg
 **/
public class Constants {
    public static final String API_GET_CONFIG = "/storage/server/config";
    public static final String API_NOTIFY = "/storage/server/notify";

    public static final TimeZone DEFAULT_TIME_ZONE = TimeZone.getTimeZone("Asia/Shanghai");
}
