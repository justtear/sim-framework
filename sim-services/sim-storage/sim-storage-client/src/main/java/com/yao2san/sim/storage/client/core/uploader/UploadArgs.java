package com.yao2san.sim.storage.client.core.uploader;

import com.yao2san.sim.storage.client.core.policy.FileRenamePolicy;
import com.yao2san.sim.storage.client.core.policy.FolderPolicy;
import io.minio.PutObjectBaseArgs;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
public class UploadArgs extends LinkedHashMap<String, String> {

    /**
     * Content type
     */
    private String contentType;

    /**
     * 上传后的回调
     */
    private transient UploadCallback callback;

    /**
     * 文件夹生成策略
     */
    private FolderPolicy folderPolicy;

    /**
     * 文件重命名策略
     */
    private FileRenamePolicy fileRenamePolicy;

    /**
     * 添加额外数据,如果存储类型为minio,则这些数据将会被放到userMetadata中
     */
    public UploadArgs add(String k, String v) {
        this.put(k, v);
        return this;
    }

    
}
