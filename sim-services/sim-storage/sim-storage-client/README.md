### 介绍
存储服务客户端，现集成Minio，支持上传至Minio和本地文件系统。持续完善中。

### 使用

- 依赖

```xml
<dependency>
    <groupId>com.yao2san</groupId>
    <artifactId>sim-storage-client</artifactId>
    <version>1.0.0</version>
</dependency>

<!--单独使用时，在pom中添加以下仓库地址-->
<repositories>
    <repository>
        <id>rdc-releases</id>
        <url>https://61c59941e74da01affdc9d7b:Swtu1HNF1phz@packages.aliyun.com/maven/repository/2170823-release-TpgdUY/</url>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
    </repository>
</repositories>

```

- 配置示例

```yaml
sim:
  storage:
    # 启用存储服务
    enable: true
    # 存储类型：Minio，可选值：local、minio、ftp
    type: minio
    # minio存储配置
    minio:
      # minio地址
      endpoint: http://localhost
      # 端口
      port: 9000
      # 存储桶
      bucket: test    
      # 根路径(可选,默认为空)
      path: /upload/
      # access-key
      access-key: U77bBCMYW1GVmow4
      # secret-key
      secret-key: tSiDkUNARZYhlARoUzT2STJlyYnezmG2
    # 腾讯COS配置
    tencent-cos:
      # 区域
      region: xxx
      # 服务地址:替换成你申请的bucket所在的endpoint
      endpoint: xxx
      # 在腾云云申请的accessKeyId(建议使用子账号)
      access-key: xxx
      # 在腾云云申请的secretKey(建议使用子账号)
      secret-key: xxx
      # 存储桶
      bucket: xxx
      # 根路径(可选)
      path: /test
    ali-oss:
      # 区域
      region: xxx
      # 服务地址:替换成你申请的bucket所在的endpoint(不要带http或https)
      endpoint: oss-cn-hongkong.aliyuncs.com
      # 在腾云云申请的accessKeyId(建议使用子账号)
      access-key: xxx
      # 在腾云云申请的secretKey(建议使用子账号)
      secret-key: xxx
      # 存储桶
      bucket: xxx
      # 根路径(可选)
      path:
      # 角色(用于生成临时凭证,具体请参照:https://help.aliyun.com/document_detail/100624.html)
      role-arn: acs:ram::1642592308587130:role/oss-manager    # 本地存储配置  
    local:
      # 本地文件存放根目录
      path: "D:\\temp\\"
      # web访问映射前缀
      prefix: static
      # 应用地址
      url: http://localhost:8080
    # ftp存储配置
    ftp:
      # 可选ftp、sftp
      type: ftp
      # ftp服务地址
      host: 127.0.0.1
      # ftp服务端口
      port: 21
      # 根目录
      path: /data/

```

- 存储类型

sim-storage-client支持以下存储类型：

| 类型             | 版本要求 | 说明                              |
| -------------- | ---- | ------------------------------- |
| 本地存储（LOCAL）    | 无    | 支持本地文件系统、NAS文件系统以及其它任意可挂载的文件系统。 |
| Minio存储（MINIO） | >8.3 | Minion是一个高性能的对象存储服务，集成了它的客户端。   |
| FTP存储（FTP）     | 无    | 待实现。                            |
| 阿里云对象存储(OSS)     | 无    | 接入了阿里云对象存储。                            |
| 腾讯云对象存储(COS)   | 无    | 接入了腾讯云对象存储。                            |



### 上传和下载

- **文件上传**

```java
@Autowired
private Uploader uploader;

@PostMapping("upload")
public void upload(MultipartFile file) throws IOException {

    String object = file.getOriginalFilename();

    UploadResult result = uploader.upload(file, object);

}

```

- **元数据**

```java
UploadArgs args = UploadArgs.builder().build();
args.add("user-id", "1");
UploadResult result = uploader.upload(file, object,args);
```

- **上传结果**`UploadResult`

| 属性         | 类型         | 说明                 |
| ---------- | ---------- | ------------------ |
| success    | boolean    | 上传成功/失败            |
| message    | String     | 上传失败时的失败原因         |
| object     | String     | 上传后的文件名（含路径，基于根路径） |
| isDir      | boolean    | 是否是目录              |
| size       | long       | 文件大小               |
| url        | String     | 文件访问/下载链接          |
| uploadArgs | UploadArgs | 上传时的参数             |

- **上传事件**

文件上传后，会发布一个`UploadEvent` 事件，您可以监听此事件做一些额外处理。

```java
@Configuration
@Slf4j
public class UploadEventListener {
    @EventListener
    public void handler(UploadEvent event) {
        log.info("Receive upload result event, time：{}, result:{}", event.getTimestamp(), JSONObject.toJSONString(event.getSource()));
    }
}
//输出：Receive upload result event, time：1663821454180, result:{"object":"images/jars.zip","size":10066282,"success":true,"uploadArgs":{"userId":"1"},"url":"http://localhost:9000/test/images/test.png"}

```

- **自动生成目录**

sim-storage-client支持目录生成策略，内置3种策略：

(1) `EmptyFolderPolicy` ：空策略，即不生成（默认）

(2) `DayFolderPolicy` ：按天生成，每天一个目录，如20220922。

(3) `MonthFolderPolicy` ：按月生成，每月一个目录，如202209。

您也可以自定义生成策略，只需要实现`FolderPolicy` 接口并将它放入Spring容器中即可：

```java
public class CustomerFolderPolicy implements FolderPolicy {
    @Override
    public String create() {
		//TODO your code
        return "";
    }
}

@Bean
public FolderPolicy folderPolicy() {
   return new CustomerFolderPolicy();
}

```

- **文件下载**

```java
@Autowired
private Downloader downloader;

@GetMapping("download")
public void download(String fileName, HttpServletResponse response) throws IOException {
    downloader.download(fileName, response.getOutputStream());
}
```

### 文件管理

`StorageManager` 提供了基础的文件管理功能，支持本地存储和Minio。

- 通过Spring容器获取`StorageManager`

```java
@Autowired
private StorageManager storageManager;
```

- 查询文件

```java
String prefix = "/images/";
//列出指定前缀的文件。如果以"/"结尾则表示列出其目录下的所有文件。
storageManager.list(prefix);

//列出指定前缀的且从/images/test.png开始之后(不包含)的文件，当文件量过多时，可基于startAfter分页。
String startAfter = "/images/test.png";
storageManager.list(prefix,startAfter);

```

- 删除文件

```java
String object= "/images/test.png";
storageManager.delete(object);
```

- 文件是否存在

```java
String object= "/images/test.png";
boolean exist = storageManager.exist(object)
```

- 重命名文件

```java
String source= "/images/test.png";
String target = "/images/test2.png";
storageManager.rename(source,target);
```

- 复制文件

```java
String source= "/images/test.png";
String target = "/images/test2.png";
storageManager.copy(source,target);
```

- 获取临时凭证(STS)

```java
//获取有效期为30分钟的临时凭证
storageManager.credentials(1800);
```
💡 注意：临时凭证目前仅支持阿里OSS和腾讯COS！