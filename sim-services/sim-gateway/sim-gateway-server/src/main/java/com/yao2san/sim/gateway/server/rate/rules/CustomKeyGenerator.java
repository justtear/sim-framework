package com.yao2san.sim.gateway.server.rate.rules;

import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.RateLimitKeyGenerator;
import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.RateLimitUtils;
import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.properties.RateLimitProperties;
import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.properties.RateLimitType;
import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.support.DefaultRateLimitKeyGenerator;
import com.yao2san.sim.auth.common.UserUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;
import java.util.StringJoiner;

@Configuration
@ConditionalOnProperty(name = "zuul.ratelimit.enabled",havingValue = "true")
public class CustomKeyGenerator {
    @Bean
    public RateLimitKeyGenerator ratelimitKeyGenerator(RateLimitProperties properties, RateLimitUtils rateLimitUtils) {
        return new DefaultRateLimitKeyGenerator(properties, rateLimitUtils) {
            @Override
            public String key(HttpServletRequest request, Route route, RateLimitProperties.Policy policy) {
                StringJoiner joiner = new StringJoiner(":");
                for (RateLimitProperties.Policy.MatchType type : policy.getType()) {
                    if (RateLimitType.USER.equals(type.getType())) {
                        joiner.add(properties.getKeyPrefix());
                        joiner.add(route.getId());
                        joiner.add("USER");
                        joiner.add(String.valueOf(UserUtil.getCurrUserId()));
                    } else {
                        String key = type.key(request, route, rateLimitUtils);
                        if (StringUtils.isNotEmpty(key)) {
                            joiner.add(key);
                        }
                    }
                }
                return joiner.toString();
            }
        };
    }

}
