package com.yao2san.sim.gateway.server.api.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.gateway.server.api.request.RouteReq;
import com.yao2san.sim.gateway.server.api.response.RouteRes;

public interface RouteManagerService {

    RouteRes detail(Long routeId);

    PageInfo<RouteRes> list(RouteReq routeReq);

    void add(RouteReq routeReq);

    void delete(Long routeId);

    void update(RouteReq routeReq);

    void online(Long routeId);

    void offline(Long routeId);
}
