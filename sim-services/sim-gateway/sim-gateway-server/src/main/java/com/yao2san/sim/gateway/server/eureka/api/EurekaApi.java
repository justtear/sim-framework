package com.yao2san.sim.gateway.server.eureka.api;

import com.alibaba.fastjson.JSONObject;
import com.yao2san.sim.framework.web.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/eureka-web/")
public class EurekaApi {
    @Autowired
    private EurekaApiService apiService;

    @GetMapping("apps")
    public ResponseData apps() {
        return ResponseData.success(JSONObject.parse(apiService.getApps()));
    }

    @GetMapping("apps/{name}")
    public ResponseData app(@PathVariable String name) {
        return ResponseData.success(JSONObject.parse(apiService.getApp(name)));
    }

    @PutMapping("apps/{name}/{instanceId}/status")
    public ResponseData updateStatus(@PathVariable String name,@PathVariable String instanceId, String value) {
        apiService.updateStatus(name, instanceId, value);
        return ResponseData.success();
    }

    @PutMapping("apps/{name}/{instanceId}")
    public ResponseData heartbeat(@PathVariable String name,@PathVariable String instanceId) {
        apiService.heartbeat(name, instanceId);
        return ResponseData.success();
    }
}
