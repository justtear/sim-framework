package com.yao2san.sim.gateway.server.route.fallback;

import com.netflix.hystrix.exception.HystrixTimeoutException;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;


@Component
public class ServiceFallback implements FallbackProvider {

    private final static String SERVICE_UNAVAILABLE_MESSAGE = "服务不可用";
    private final static String GATEWAY_TIMEOUT_MESSAGE = "请求超时";

    @Override
    public String getRoute() {
        //match all route
        return "*";
    }

    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        if (cause instanceof HystrixTimeoutException) {
            return response(HttpStatus.GATEWAY_TIMEOUT, GATEWAY_TIMEOUT_MESSAGE);
        } else {
            return response(HttpStatus.SERVICE_UNAVAILABLE, SERVICE_UNAVAILABLE_MESSAGE);
        }
    }

    private ClientHttpResponse response(final HttpStatus status, String msg) {
        return new GatewayClientHttpResponse(status, msg);
    }
}

