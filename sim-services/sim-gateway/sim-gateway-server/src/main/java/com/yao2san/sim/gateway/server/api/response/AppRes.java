package com.yao2san.sim.gateway.server.api.response;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AppRes extends BaseBean {
    private Long routeAppId;
    private String appName;
    private String appCode;
    private String status;
    private Integer routeCount;
}
