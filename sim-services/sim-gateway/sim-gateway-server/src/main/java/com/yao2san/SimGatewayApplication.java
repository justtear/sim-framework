package com.yao2san;

import com.yao2san.sim.gateway.server.rate.core.RateLimitHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableZuulProxy
@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
@EnableEurekaServer
@EnableFeignClients
@Slf4j
public class SimGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimGatewayApplication.class, args);

        log.info("sim-gateway-server start success!");

        RateLimitHelper.refresh();

    }
}
