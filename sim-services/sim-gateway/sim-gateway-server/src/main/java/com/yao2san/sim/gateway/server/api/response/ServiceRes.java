package com.yao2san.sim.gateway.server.api.response;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceRes extends Pagination {
    private Long serviceId;

    private Long serviceGroupId;

    private String name;

    private String url;

    private String type;

    private Long routeId;

    private String description;

    private String groupName;
}
