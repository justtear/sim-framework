package com.yao2san.sim.gateway.server.api.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.gateway.server.api.request.RouteRateLimitReq;
import com.yao2san.sim.gateway.server.api.response.RouteRateLimitRes;

public interface RouteRateLimitService {
    void refresh();

    void refresh(Long routeId);

    RouteRateLimitRes detail(Long routeRateLimitPolicyId);

    PageInfo<RouteRateLimitRes> list(RouteRateLimitReq routeRateLimit);

    void add(RouteRateLimitReq routeRateLimit);

    void delete(Long rateLimitPolicyId);

    void update(RouteRateLimitReq routeRateLimit);
    void updateStatus(RouteRateLimitReq routeRateLimit);
}
