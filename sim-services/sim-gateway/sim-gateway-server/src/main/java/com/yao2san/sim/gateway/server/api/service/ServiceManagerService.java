package com.yao2san.sim.gateway.server.api.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.gateway.server.api.request.ServiceReq;
import com.yao2san.sim.gateway.server.api.response.ServiceGroupRes;
import com.yao2san.sim.gateway.server.api.response.ServiceRes;

import java.util.List;

public interface ServiceManagerService {
    ResponseData<PageInfo<ServiceRes>> list(ServiceReq req);
    ResponseData<Void> add(ServiceReq req);
    ResponseData<ServiceRes> detail(Long req);
    ResponseData<Void> update(ServiceReq req);
    ResponseData<Void> delete(Long serviceId);
    ResponseData<Void> online(Long serviceId);
    ResponseData<Void> offline(Long serviceId);
    ResponseData<List<ServiceGroupRes>> group();
}
