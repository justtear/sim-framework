package com.yao2san.sim.gateway.server.route.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.yao2san.sim.gateway.server.route.core.AbstractRouteLocator;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;

/**
 * TODO 全局header
 */
@Component
public class PermissionFilter extends ZuulFilter {
    private final UrlPathHelper urlPathHelper;
    private final AbstractRouteLocator routeLocator;

    public PermissionFilter(UrlPathHelper urlPathHelper, AbstractRouteLocator routeLocator) {
        this.urlPathHelper = urlPathHelper;
        this.routeLocator = routeLocator;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String path = urlPathHelper.getPathWithinApplication(request);
        ZuulProperties.ZuulRoute zuulRoute = routeLocator.getZuulRoute(path);


        return null;
    }
}
