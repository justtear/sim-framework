package com.yao2san.sim.gateway.server.api.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.gateway.server.api.request.AppReq;
import com.yao2san.sim.gateway.server.api.response.AppRes;

public interface AppService {
    ResponseData<Void> add(AppReq req);
    ResponseData<Void> update(AppReq req);
    ResponseData<Void> remove(Long groupId);
    ResponseData<PageInfo<AppRes>> list(AppReq req);
}
