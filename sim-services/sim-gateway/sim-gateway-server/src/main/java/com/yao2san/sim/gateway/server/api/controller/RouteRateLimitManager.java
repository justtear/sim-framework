package com.yao2san.sim.gateway.server.api.controller;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.gateway.server.api.request.RouteRateLimitReq;
import com.yao2san.sim.gateway.server.api.response.RouteRateLimitRes;
import com.yao2san.sim.gateway.server.api.service.RouteRateLimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Rate manage api
 *
 * @author wxg
 */
@RestController
@RequestMapping("rateLimit")
public class RouteRateLimitManager {

    @SuppressWarnings("all")
    @Autowired
    private RouteRateLimitService ratelimitServiceRoute;

    @PostMapping("refresh")
    public ResponseData<Void> refresh() {
        ratelimitServiceRoute.refresh();
        return ResponseData.success(null, "success");
    }

    @PostMapping("refresh/{routeId}")
    public ResponseData<Void> refreshByRouteId(@PathVariable Long routeId) {
        ratelimitServiceRoute.refresh(routeId);
        return ResponseData.success(null, "success");
    }

    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated(RouteRateLimitReq.Add.class) RouteRateLimitReq routeRateLimit) {
        ratelimitServiceRoute.add(routeRateLimit);
        ratelimitServiceRoute.refresh(routeRateLimit.getRouteId());
        return ResponseData.success();
    }

    @DeleteMapping("{routeRateLimitPolicyId}")
    public ResponseData<Void> delete(@PathVariable("routeRateLimitPolicyId") Long routeRateLimitPolicyId) {
        ratelimitServiceRoute.delete(routeRateLimitPolicyId);
        return ResponseData.success();
    }

    @PatchMapping
    public ResponseData<Void> update(@RequestBody RouteRateLimitReq routeRateLimit) {
        ratelimitServiceRoute.update(routeRateLimit);
        ratelimitServiceRoute.refresh(routeRateLimit.getRouteId());
        return ResponseData.success();
    }

    @PatchMapping("status")
    public ResponseData<Void> updateStatus(@RequestBody RouteRateLimitReq routeRateLimit) {
        ratelimitServiceRoute.updateStatus(routeRateLimit);
        ratelimitServiceRoute.refresh(routeRateLimit.getRouteId());
        return ResponseData.success();
    }

    @GetMapping
    public ResponseData<PageInfo<RouteRateLimitRes>> list(RouteRateLimitReq routeRateLimit) {
        return ResponseData.success(ratelimitServiceRoute.list(routeRateLimit));
    }

    @GetMapping("{routeRateLimitPolicyId}")
    public ResponseData<RouteRateLimitRes> detail(@PathVariable("routeRateLimitPolicyId") Long routeRateLimitPolicyId) {
        return ResponseData.success(ratelimitServiceRoute.detail(routeRateLimitPolicyId));
    }

    @GetMapping("test")
    public ResponseData<String> test1(){
        return ResponseData.success("get");
    }
    @PostMapping("test")
    public ResponseData<String> test2(){
        return ResponseData.success("post");
    }
}
