package com.yao2san.sim.gateway.server.api.service.impl;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.gateway.server.api.request.AppReq;
import com.yao2san.sim.gateway.server.api.response.AppRes;
import com.yao2san.sim.gateway.server.api.service.AppService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AppServiceImpl extends BaseServiceImpl implements AppService {
    @Override
    @Transactional
    public ResponseData<Void> add(AppReq req) {
        req.setAppName(req.getAppName().trim());
        req.setAppCode(req.getAppCode().trim());
        checkName(req);
        checkCode(req);
        this.sqlSession.insert("app.add", req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> update(AppReq req) {
        req.setAppName(req.getAppName().trim());
        req.setAppCode(req.getAppCode().trim());
        checkName(req);
        this.sqlSession.insert("app.update", req);
        return ResponseData.success();
    }

    @Override
    public ResponseData<PageInfo<AppRes>> list(AppReq req) {
        PageInfo<AppRes> list = this.qryList("app.list", req);
        return ResponseData.success(list);
    }

    @Override
    public ResponseData<Void> remove(Long routeAppId) {
        AppReq req = new AppReq();
        req.setRouteAppId(routeAppId);
        Long count = this.sqlSession.selectOne("app.countRoute", req);
        if (count > 0) {
            throw new BusiException("请先删除路由后再删除应用");
        }
        this.sqlSession.delete("app.delete", req);
        return ResponseData.success();
    }

    private void checkName(AppReq req) {
        Long count = this.sqlSession.selectOne("countName", req);
        if (count > 0) {
            throw new BusiException("应用名称[" + req.getAppName() + "]已存在");
        }
    }

    private void checkCode(AppReq req) {
        Long count = this.sqlSession.selectOne("countCode", req);
        if (count > 0) {
            throw new BusiException("应用编码[" + req.getAppCode() + "]已存在");
        }
    }
}
