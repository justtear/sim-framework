package com.yao2san.sim.gateway.server.api.request;

import com.yao2san.sim.framework.web.bean.BaseBean;
import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AppReq extends Pagination {
    private Long routeAppId;
    private String appName;
    private String appCode;
}
