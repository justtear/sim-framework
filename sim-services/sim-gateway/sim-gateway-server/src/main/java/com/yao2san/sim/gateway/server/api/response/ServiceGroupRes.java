package com.yao2san.sim.gateway.server.api.response;

import lombok.Data;

@Data
public class ServiceGroupRes {
    private Long serviceGroupId;
    private String groupName;
}
