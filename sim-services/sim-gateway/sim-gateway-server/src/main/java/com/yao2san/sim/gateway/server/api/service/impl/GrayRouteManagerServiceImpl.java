package com.yao2san.sim.gateway.server.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageInfo;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.loadbalancer.IRule;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.gateway.server.api.request.GrayRouteReq;
import com.yao2san.sim.gateway.server.api.response.GrayRouteRes;
import com.yao2san.sim.gateway.server.api.response.RouteRes;
import com.yao2san.sim.gateway.server.api.service.GrayRouteManagerService;
import com.yao2san.sim.gateway.server.api.service.RouteManagerService;
import com.yao2san.sim.gateway.server.eureka.api.EurekaApiService;
import com.yao2san.sim.gateway.server.route.core.RouteHelper;
import com.yao2san.sim.gateway.server.route.gray.GrayRoute;
import com.yao2san.sim.gateway.server.route.gray.GrayRouteHelper;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;

@Service
@Slf4j
public class GrayRouteManagerServiceImpl extends BaseServiceImpl implements GrayRouteManagerService {
    @Autowired
    private RouteManagerService routeManagerService;

    @Autowired
    private EurekaApiService eurekaApiService;

    @Override
    public GrayRouteRes detail(Long routeGrayId) {
        GrayRouteRes res = this.sqlSession.selectOne("grayRoute.detail", routeGrayId);
        res.setHeaders(JSONObject.parseObject(res.getHeadersString(), new TypeReference<Map<String, Object>>() {
        }));
        res.setCookies(JSONObject.parseObject(res.getCookiesString(), new TypeReference<Map<String, Object>>() {
        }));
        res.setParams(JSONObject.parseObject(res.getParamsString(), new TypeReference<Map<String, Object>>() {
        }));
        return res;
    }

    @Override
    public PageInfo<RouteRes> list(GrayRouteReq grayRouteReq) {
        return this.qryList("grayRoute.list", grayRouteReq);
    }

    @Override
    @Transactional
    public void add(GrayRouteReq grayRouteReq) {
        grayRouteReq.setStatus("1200");
        this.convertMapToString(grayRouteReq);
        this.sqlSession.insert("grayRoute.add", grayRouteReq);
    }

    @Override
    public void delete(Long routeGrayId) {
        this.sqlSession.delete("grayRoute.delete", routeGrayId);
    }

    @Override
    @Transactional
    public void update(GrayRouteReq grayRouteReq) {
        this.convertMapToString(grayRouteReq);
        Set<String> serviceInstances = grayRouteReq.getServiceInstances();
        if (serviceInstances != null) {
            for (String instance : serviceInstances) {
                try {
                    eurekaApiService.heartbeat(grayRouteReq.getServiceId(), instance);
                } catch (FeignException.NotFound e) {
                    throw new BusiException("Service instance " + instance + " not found.");
                }
            }
        }
        this.sqlSession.update("grayRoute.update", grayRouteReq);
    }

    private void convertMapToString(GrayRouteReq grayRouteReq) {
        if (grayRouteReq.getHeaders() != null) {
            grayRouteReq.setHeadersString(JSONObject.toJSONString(grayRouteReq.getHeaders(), SerializerFeature.WriteMapNullValue));
        }
        if (grayRouteReq.getCookies() != null) {
            grayRouteReq.setCookiesString(JSONObject.toJSONString(grayRouteReq.getCookies(), SerializerFeature.WriteMapNullValue));
        }
        if (grayRouteReq.getParams() != null) {
            grayRouteReq.setParamsString(JSONObject.toJSONString(grayRouteReq.getParams(), SerializerFeature.WriteMapNullValue));
        }
    }

    @Override
    @Transactional
    public void online(Long routeGrayId) {
        //update service instances metadata
        GrayRouteRes grayRouteRes = this.detail(routeGrayId);
        Set<String> serviceInstances = grayRouteRes.getServiceInstances();

        //update service instance status
        serviceInstances.forEach(instance -> eurekaApiService.updateStatus(grayRouteRes.getServiceId(), instance, InstanceInfo.InstanceStatus.UP.name()));

        //update loadbalancer rule
        String ruleTypeName = grayRouteRes.getRuleType();
        GrayRoute.RuleType ruleType = GrayRoute.RuleType.valueOf(ruleTypeName);
        String serviceId = grayRouteRes.getServiceId();
        IRule rule = RouteHelper.getLbRuleWithRuleType(ruleType);
        RouteHelper.changeLbRule(serviceId, rule);
        //update status
        GrayRouteReq req = new GrayRouteReq();
        req.setRouteGrayId(routeGrayId);
        req.setStatus("1000");

        GrayRouteHelper.refresh();

        this.update(req);
    }

    @Override
    @Transactional
    public void offline(Long routeGrayId) {
        GrayRouteRes grayRouteRes = this.detail(routeGrayId);
        RouteRes routeRes = routeManagerService.detail(grayRouteRes.getRouteId());
        String serviceId = routeRes.getServiceId();
        Set<String> serviceInstances = grayRouteRes.getServiceInstances();

        //update service instance status from eureka
        try {
            serviceInstances.forEach(instance -> eurekaApiService.updateStatus(serviceId, instance, InstanceInfo.InstanceStatus.OUT_OF_SERVICE.name()));
        } catch (Exception e) {
            log.warn("Service offline warn: update service instance status fial");
        }

        //change loadbalancer rule to default rule
        RouteHelper.changeLbRule(serviceId, RouteHelper.getDefaultLbRule(serviceId));

        //update status
        GrayRouteReq req = new GrayRouteReq();
        req.setRouteGrayId(routeGrayId);
        req.setStatus("1300");
        this.update(req);

        GrayRouteHelper.refresh();
    }

    @Override
    @Transactional
    public void formal(Long routeGrayId) {
        GrayRouteReq grayRoute = new GrayRouteReq();
        grayRoute.setRouteGrayId(routeGrayId);
        GrayRouteRes grayRouteRes = this.detail(routeGrayId);
        String serviceId = grayRouteRes.getServiceId();

        //update service instance
        Set<String> serviceInstances = grayRouteRes.getServiceInstances();

        serviceInstances.forEach(instance -> {
            //check service instance status(send heartbeat)
            //note: maybe throw exception if instance not found
            eurekaApiService.heartbeat(serviceId, instance);
            //update service instance status from eureka
            eurekaApiService.updateStatus(serviceId, instance, InstanceInfo.InstanceStatus.UP.name());
        });

        //change loadbalancer rule to default rule
        RouteHelper.changeLbRule(serviceId, RouteHelper.getDefaultLbRule(serviceId));

        //update gray route to formal
        grayRoute.setStatus("1400");
        this.update(grayRoute);

        //refresh gray routes
        GrayRouteHelper.refresh();
    }

    @Override
    @Transactional
    public void copy(Long routeGrayId) {
        GrayRouteRes detail = this.detail(routeGrayId);
        GrayRouteReq req = new GrayRouteReq();
        BeanUtils.copyProperties(detail,req);
        this.add(req);
    }
}
