package com.yao2san.sim.gateway.server.api.controller;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.gateway.server.api.request.ServiceReq;
import com.yao2san.sim.gateway.server.api.response.ServiceGroupRes;
import com.yao2san.sim.gateway.server.api.response.ServiceRes;
import com.yao2san.sim.gateway.server.api.service.ServiceManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("service-manager")
public class ServiceManager {
    @Autowired
    private ServiceManagerService serviceManagerService;

    @GetMapping
    public ResponseData<PageInfo<ServiceRes>> list(ServiceReq req) {
        return serviceManagerService.list(req);
    }

    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated ServiceReq req) {
        return serviceManagerService.add(req);
    }

    @PatchMapping
    public ResponseData<Void> update(@RequestBody @Validated(ServiceReq.Update.class) ServiceReq req) {
        return serviceManagerService.update(req);
    }

    @GetMapping("{serviceId:^[0-9]*$}")
    public ResponseData<ServiceRes> detail(@PathVariable Long serviceId) {
        return serviceManagerService.detail(serviceId);
    }

    @DeleteMapping("{serviceId}")
    public ResponseData<Void> delete(@PathVariable Long serviceId) {
        return serviceManagerService.delete(serviceId);
    }

    @PatchMapping("offline/{serviceId}")
    public ResponseData<Void> offline(@PathVariable("serviceId") Long serviceId) {
        return serviceManagerService.offline(serviceId);
    }

    @PatchMapping("online/{serviceId}")
    public ResponseData<Void> online(@PathVariable("serviceId") Long serviceId) {
        return serviceManagerService.online(serviceId);
    }

    @GetMapping("group")
    public ResponseData<List<ServiceGroupRes>> group() {
        return serviceManagerService.group();
    }
}
