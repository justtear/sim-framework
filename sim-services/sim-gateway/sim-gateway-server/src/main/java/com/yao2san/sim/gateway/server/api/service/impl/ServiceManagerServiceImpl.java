package com.yao2san.sim.gateway.server.api.service.impl;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.auth.common.UserUtil;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.gateway.server.api.request.ServiceReq;
import com.yao2san.sim.gateway.server.api.response.ServiceGroupRes;
import com.yao2san.sim.gateway.server.api.response.ServiceRes;
import com.yao2san.sim.gateway.server.api.service.ServiceManagerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ServiceManagerServiceImpl extends BaseServiceImpl implements ServiceManagerService {
    @Override
    public ResponseData<PageInfo<ServiceRes>> list(ServiceReq req) {
        PageInfo<ServiceRes> list = this.qryList("service.list", req);
        return ResponseData.success(list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> add(ServiceReq req) {
        this.sqlSession.insert("service.add", req);
        return ResponseData.success();
    }

    @Override
    public ResponseData<ServiceRes> detail(Long serviceId) {
        ServiceReq req = new ServiceReq();
        req.setServiceId(serviceId);
        req.setCreateUser(UserUtil.getCurrUserId());

        ServiceRes res = this.sqlSession.selectOne("service.detail", req);
        return ResponseData.success(res);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> update(@Validated ServiceReq req) {
        this.sqlSession.insert("service.update", req);
        return ResponseData.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> delete(Long serviceId) {
        ServiceReq req = new ServiceReq();
        req.setServiceId(serviceId);
        this.sqlSession.insert("service.delete", req);
        return ResponseData.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> online(Long serviceId) {
        ServiceReq req = new ServiceReq();
        req.setServiceId(serviceId);
        req.setStatus("1000");
        this.sqlSession.update("service.update", req);
        return ResponseData.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> offline(Long serviceId) {
        ServiceReq param = new ServiceReq();
        param.setServiceId(serviceId);
        param.setStatus("1200");
        this.sqlSession.update("service.update", param);
        return ResponseData.success();
    }

    @Override
    public ResponseData<List<ServiceGroupRes>> group() {
        Map<String,Object> param = new HashMap<>();
        param.put("createUser",UserUtil.getCurrUserId());
        List<ServiceGroupRes> list = this.sqlSession.selectList("service.group", param);
        return ResponseData.success(list);
    }
}
