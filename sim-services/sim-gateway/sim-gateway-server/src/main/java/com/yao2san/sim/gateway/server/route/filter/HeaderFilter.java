package com.yao2san.sim.gateway.server.route.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO 全局header
 */
public class HeaderFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return false;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest req = RequestContext.getCurrentContext().getRequest();
        Map<String, String> headers = this.getHeaders();
        if (headers.size() > 0) {
            headers.forEach(requestContext::addZuulRequestHeader);
        }
        return null;
    }

    private Map<String, String> getHeaders() {
        //TODO 获取配置的全局header
        return new HashMap<>();
    }
}
