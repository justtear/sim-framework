package com.yao2san.sim.gateway.server.route.rule;

import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ZoneAwareLoadBalancer;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public abstract class AbstractMatcher extends AbstractLoadBalancerRule implements Matcher {
    protected AtomicInteger nextServerCyclicCounter;

    public AbstractMatcher() {
        nextServerCyclicCounter = new AtomicInteger(0);
    }

    protected int incrementAndGetModulo(int modulo) {
        for (; ; ) {
            int current = nextServerCyclicCounter.get();
            int next = (current + 1) % modulo;
            if (nextServerCyclicCounter.compareAndSet(current, next))
                return next;
        }
    }

    protected Server choose(ILoadBalancer lb, Object key, List<Server> servers) {
        if (lb == null) {
            log.warn("no load balancer");
            return null;
        }

        Server server = null;
        int count = 0;
        while (count++ < 10) {

            int serverCount = servers.size();

            int size = servers.size();
            if (size == 0) {
                log.warn("No up servers available from load balancer: " + lb);
                return null;
            }

            int nextServerIndex = incrementAndGetModulo(serverCount);
            server = servers.get(nextServerIndex);

            if (server == null) {
                Thread.yield();
                continue;
            }

            if (server.isAlive() && (server.isReadyToServe())) {
                return (server);
            }

            server = null;
        }

        if (count >= 10) {
            log.warn("No available alive servers after 10 tries from load balancer: "
                    + lb);
        }
        return server;
    }
    public String getName(){
        return ((ZoneAwareLoadBalancer) getLoadBalancer()).getName();
    }
    @Override
    public abstract boolean isMatch();
}
