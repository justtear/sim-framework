package com.yao2san.sim.gateway.server.api.controller;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.gateway.server.api.request.AppReq;
import com.yao2san.sim.gateway.server.api.response.AppRes;
import com.yao2san.sim.gateway.server.api.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("app")
public class AppManager {

    @Autowired
    private AppService appService;
    @PostMapping
    public ResponseData<Void> add(@RequestBody AppReq req) {
        return appService.add(req);
    }

    @GetMapping
    public ResponseData<PageInfo<AppRes>> list(AppReq req) {
        return appService.list(req);
    }


    @PatchMapping
    public ResponseData<Void> update(@RequestBody AppReq req) {
        return appService.update(req);
    }

    @DeleteMapping("{routeAppId}")
    public ResponseData<Void> remove(@PathVariable("routeAppId") Long routeAppId) {
        return appService.remove(routeAppId);
    }
}
