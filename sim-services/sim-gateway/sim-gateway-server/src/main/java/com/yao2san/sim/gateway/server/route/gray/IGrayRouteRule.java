package com.yao2san.sim.gateway.server.route.gray;
@Deprecated
public interface IGrayRouteRule {
    GrayRoute matchingGrayRoute();

    boolean isHit(GrayRoute grayRoute);
}
