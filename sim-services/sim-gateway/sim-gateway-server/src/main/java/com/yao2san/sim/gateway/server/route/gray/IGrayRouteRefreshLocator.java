package com.yao2san.sim.gateway.server.route.gray;

@Deprecated
public interface IGrayRouteRefreshLocator extends IGrayRouteLocator{

    void refresh();
}
