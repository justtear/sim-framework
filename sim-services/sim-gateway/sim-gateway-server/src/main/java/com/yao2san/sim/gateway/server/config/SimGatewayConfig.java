package com.yao2san.sim.gateway.server.config;

import com.codahale.metrics.MetricRegistry;
import com.yao2san.sim.gateway.server.monitor.reporter.DatabaseReporter;
import com.yao2san.sim.gateway.server.route.core.DatabaseRouteLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletPath;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * @author wxg
 **/
@Configuration
public class SimGatewayConfig {

    @Autowired
    private ZuulProperties zuulProperties;
    @Autowired
    private DispatcherServletPath dispatcherServletPath;

    @Bean
    public DatabaseRouteLocator databaseRouteLocator( ) {
        return new DatabaseRouteLocator(dispatcherServletPath.getPath(), zuulProperties);
    }

    @Bean
    public MetricRegistry metricRegistry() {
        return new MetricRegistry();
    }

    @Bean
    @ConditionalOnProperty(prefix = "zuul.reporter",name = "enabled",havingValue = "true")
    public DatabaseReporter databaseReporter(MetricRegistry metricRegistry) {
        return DatabaseReporter.forRegistry(metricRegistry).build();
    }
}
