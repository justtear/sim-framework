package com.yao2san.sim.gateway.server.eureka.api;

import com.yao2san.sim.framework.web.exception.BusiException;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EurekaApiService {

    @Autowired
    private EurekaFeignClient client;

    public String getApps() {
        return client.apps();
    }

    public String getApp(String name) {
        return client.apps(name);
    }

    public String updateStatus(String name,String instanceId,String status) {
        try {
            return client.status(name,instanceId,status);
        }catch (FeignException.NotFound e){
            throw new BusiException("Service instance "+instanceId+" not found.");
        }
    }

    public String heartbeat(String name,String instanceId) {
        return client.heartbeat(name,instanceId);
    }
}
