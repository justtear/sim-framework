package com.yao2san.sim.gateway.server.eureka.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * eureka rest api
 */
@FeignClient(name = "sim-gateway-server", path = "eureka")
public interface EurekaFeignClient {
    @GetMapping(value = "apps", produces = "application/json", consumes = "application/json")
    String apps();

    @GetMapping(value = "apps/{name}", produces = "application/json", consumes = "application/json")
    String apps(@PathVariable String name);

    @PutMapping(value = "apps/{name}/{instanceId}/status", produces = "application/json", consumes = "application/json")
    String status(@PathVariable String name,@PathVariable String instanceId, @RequestParam String value);

    @PutMapping(value = "apps/{name}/{instanceId}", produces = "application/json", consumes = "application/json")
    String heartbeat(@PathVariable String name,@PathVariable String instanceId);

}
