package com.yao2san.sim.gateway.server.route.rule;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import com.netflix.zuul.context.RequestContext;
import com.yao2san.sim.gateway.server.config.GatewayProperties;
import com.yao2san.sim.gateway.server.route.gray.GrayRouteHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Loadbalancer rule for cookie
 */
@Slf4j
public class CookieMatchRule extends AbstractMatcher {
    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {
    }

    @Override
    public Server choose(Object key) {
        ILoadBalancer lb = getLoadBalancer();

        Server server;
        if (!this.isMatch()) {
            List<Server> notGrayServers = new ArrayList<>(lb.getReachableServers());
            notGrayServers.removeAll(GrayRouteHelper.getGrayServer(lb));
            server = choose(lb, key, notGrayServers);
            log.debug("Not hit gray service, current instance:{}", server);
        } else {
            server = choose(lb, key, GrayRouteHelper.getGrayServer(lb));
            log.debug("Hit gray service,use rule: CookieMatchRule, current instance:{}", server);
        }
        return server;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public boolean isMatch() {
        Map<String, GatewayProperties.GrayRoute> all = GrayRouteHelper.getAll();
        GatewayProperties.GrayRoute grayRoute = all.get(getName());
        if (grayRoute == null || grayRoute.getServiceInstances().size() == 0) {
            return false;
        }
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        Map<String, Object> cookiesMap = grayRoute.getCookies();
        Cookie[] cookies = request.getCookies();
        if(cookies==null){
            return false;
        }
        int count = 0;

        for (Cookie cookie : cookies) {
            String s = cookie.getName();
            if (StringUtils.equals(cookie.getValue(), String.valueOf(cookiesMap.get(s)))) {
                count++;
            }
        }
        return count == cookiesMap.size();
    }
}
