package com.yao2san.sim.gateway.server.route.gray;

import cn.hutool.core.lang.WeightRandom;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Sets;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ZoneAwareLoadBalancer;
import com.netflix.niws.loadbalancer.DiscoveryEnabledServer;
import com.yao2san.sim.gateway.server.config.GatewayProperties;
import com.yao2san.sim.gateway.server.eureka.api.EurekaApiService;
import com.yao2san.sim.gateway.server.route.core.RouteHelper;
import com.yao2san.sim.gateway.server.utils.CacheUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
@DependsOn({"routeHelper", "serviceUtil"})
@Slf4j
public class GrayRouteHelper {
    public static final String GRAY_ROUTE_CACHE_KEY = "GRAY_ROUTE";

    public static WeightRandom<String> random = RandomUtil.weightRandom(Collections.EMPTY_LIST);

    private static SqlSession sqlSession;


    private static EurekaApiService eurekaApiService;
    /*   private static IGrayRouteRefreshLocator grayRouteRefreshLocator;*/

    @Autowired
    public void setSqlSession(SqlSession sqlSession) {
        GrayRouteHelper.sqlSession = sqlSession;
    }

    @Autowired
    public void setEurekaApiService(EurekaApiService eurekaApiService) {
        GrayRouteHelper.eurekaApiService = eurekaApiService;
    }

    /**
     * set cache for gray route
     */
    @SuppressWarnings("all")
    private static void cacheAll() {
        List<Map<String, Object>> list = sqlSession.selectList("route.loadGrayRoutes");
        Map<String, GatewayProperties.GrayRoute> grayRoutes = new HashMap<>();
        list.forEach(item -> {
            GatewayProperties.GrayRoute grayRoute = new GatewayProperties.GrayRoute();
            grayRoute.setPath(MapUtils.getString(item, "path"));
            grayRoute.setPath(MapUtils.getString(item, "path"));
            grayRoute.setRuleType(GrayRoute.RuleType.valueOf(MapUtils.getString(item, "ruleType")));
            grayRoute.setMainVersion(MapUtils.getString(item, "mainVersion"));
            grayRoute.setGrayVersion(MapUtils.getString(item, "grayVersion"));
            grayRoute.setWeight(MapUtils.getDouble(item, "weight"));
            grayRoute.setHeaders(JSONObject.parseObject(MapUtils.getString(item, "headers"), Map.class));
            grayRoute.setParams(JSONObject.parseObject(MapUtils.getString(item, "params"), Map.class));
            grayRoute.setCookies(JSONObject.parseObject(MapUtils.getString(item, "cookies"), Map.class));
            grayRoute.setServiceInstances(Sets.newHashSet(MapUtils.getString(item, "serviceInstances").split(",")));
            grayRoutes.put(MapUtils.getString(item, "serviceId"), grayRoute);
        });
        CacheUtil.put(GRAY_ROUTE_CACHE_KEY, grayRoutes);
    }

    @SuppressWarnings("all")
    public static Map<String, GatewayProperties.GrayRoute> getAll() {
        Object value = CacheUtil.get(GRAY_ROUTE_CACHE_KEY);
        if (value instanceof Optional) {
            return Collections.emptyMap();
        }
        Map<String, GatewayProperties.GrayRoute> routeMap = (Map<String, GatewayProperties.GrayRoute>) value;
        return routeMap;
    }
    public static  List<Server> getGrayServer(ILoadBalancer lb) {
        String name = ((ZoneAwareLoadBalancer) lb).getName();
        Map<String, GatewayProperties.GrayRoute> all = GrayRouteHelper.getAll();
        GatewayProperties.GrayRoute grayRoute = all.get(name);
        if (grayRoute == null) {
            return Collections.emptyList();
        }

        Set<String> grayInstances = grayRoute.getServiceInstances();

        if (grayInstances.size() == 0) {
            return Collections.emptyList();
        }

        List<Server> upList = lb.getReachableServers();
        List<Server> grayServers = new LinkedList<>();

        for (Server s : upList) {
            DiscoveryEnabledServer ss = (DiscoveryEnabledServer) s;
            if (grayInstances.contains(ss.getInstanceInfo().getInstanceId())) {
                grayServers.add(s);
            }
        }

        return grayServers;
    }
    @PostConstruct
    public static void refresh() {

        cacheAll();

        Map<String, GatewayProperties.GrayRoute> all = getAll();

        log.info("Find {} gray route service",all.size());

        if (all.size() > 0) {
            all.forEach((k, v) -> {
                RouteHelper.changeLbRule(k,RouteHelper.getLbRuleWithRuleType(v.getRuleType()));
                log.info("Change loadbalancer rule of {}, rule type: {}",k,v.getRuleType());
            });
        }
    }

    @Component
    public static class AfterInit implements ApplicationRunner {
        @Override
        public void run(ApplicationArguments args) {
            new Thread(new UpdateInstanceStatusThread()).start();
        }
    }

    public static class UpdateInstanceStatusThread implements Runnable{
        private static final int MAX_RETRY = 100;
        private static final int RETRY_SECONDS = 10;
        @SneakyThrows
        @Override
        public void run() {
            Map<String, GatewayProperties.GrayRoute> all = getAll();

            if (all.size() > 0) {
                int retry = 0;
                while (retry<MAX_RETRY){
                    try {
                        all.forEach((k, v) -> {
                            for (String instance : v.getServiceInstances()) {
                                eurekaApiService.updateStatus(k,instance,"UP");
                            }
                        });
                        log.info("Update gray service instance status success");
                    }catch (Exception e){
                        log.warn("Update gray service instance status fail: Retry {}",(retry+1));
                        TimeUnit.SECONDS.sleep(RETRY_SECONDS);
                    }finally {
                        retry++;
                    }
                }
            }
            log.info("All gray route service instance initialization complete");
        }
    }

}
