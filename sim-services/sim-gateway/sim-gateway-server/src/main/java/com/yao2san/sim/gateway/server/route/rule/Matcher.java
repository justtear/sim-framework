package com.yao2san.sim.gateway.server.route.rule;

public interface Matcher {
    String CACHE_KEY_PREFIX = "GRAY_ROUTE_INSTANCE:";

    boolean isMatch();
}
