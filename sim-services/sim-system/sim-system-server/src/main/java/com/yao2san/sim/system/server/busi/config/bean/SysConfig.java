package com.yao2san.sim.system.server.busi.config.bean;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SysConfig extends Pagination {
    private final static String DEFAULT_LABEL = "latest";
    private Long configId;
    @NotEmpty(groups = {Update.class, Add.class})
    private String key;
    private String value;
    private String newValue;
    @NotEmpty(groups = {Update.class, Add.class})
    private String application;
    @NotEmpty(groups = {Update.class, Add.class})
    private String profile;
    @NotEmpty(groups = {Update.class, Add.class})
    private String label = DEFAULT_LABEL;
    private String version;
    private String description;
    private String remark;
    private String status;

    private String dataType = DataType.LIST.name();

    private Boolean isEncrypt = false;

    /**
     * 搜搜内容
     */
    private String filterText;

    public interface Add {
    }

    public interface Update {
    }

    public enum DataType {
        LIST,
        JSON,
        YAML,
        PROPERTIES
    }
}
