package com.yao2san.sim.system.server.busi.config.bean.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class SysConfigRollbackReq {
    @NotEmpty(message = "application can not be empty")
    private String application;
    @NotEmpty(message = "profile can not be empty")
    private String profile;
    @NotEmpty(message = "version can not be empty")
    private String version;
}
