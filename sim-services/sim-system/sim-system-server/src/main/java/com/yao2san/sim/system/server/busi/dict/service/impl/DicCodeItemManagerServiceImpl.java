package com.yao2san.sim.system.server.busi.dict.service.impl;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.system.server.busi.dict.bean.request.DicCodeItemReq;
import com.yao2san.sim.system.server.busi.dict.bean.response.DicCodeItemRes;
import com.yao2san.sim.system.server.busi.dict.service.DicCodeItemManagerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DicCodeItemManagerServiceImpl extends BaseServiceImpl implements DicCodeItemManagerService {

    public ResponseData<PageInfo<DicCodeItemRes>> qryCode(DicCodeItemReq req) {
        PageInfo<DicCodeItemRes> list = this.qryList("dicCodeItem.qryDicCode", req);
        return ResponseData.success(list);
    }

    @Override
    @Transactional
    public ResponseData<Void> addCode(DicCodeItemReq req) {
        req.setIsShow(false);
        this.sqlSession.insert("dicCodeItem.addDicCode", req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> updateCode(DicCodeItemReq req) {
        this.sqlSession.update("dicCodeItem.updateDicCode", req);
        return ResponseData.success();
    }

    @Override
    public ResponseData<Boolean> codeExist(String dicCode) {
        Long count = this.sqlSession.selectOne("dicCodeItem.countDicCode", dicCode);

        return ResponseData.success(count > 0);
    }

    @Override
    public ResponseData<PageInfo<DicCodeItemRes>> qryItem(DicCodeItemReq req) {
        PageInfo<DicCodeItemRes> list = this.qryList("dicCodeItem.qryDicItem", req);
        return ResponseData.success(list);
    }

    @Override
    @Transactional
    public ResponseData<Void> addItem(DicCodeItemReq req) {
        this.sqlSession.insert("dicCodeItem.addDicItem", req);
        return ResponseData.success();
    }

    @Override
    public ResponseData<Void> updateItem(DicCodeItemReq req) {
        if (req.getSort() == null) {
            req.setSort(0);
        }
        this.sqlSession.update("dicCodeItem.updateDicItem", req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> deleteCode(String dicCode) {
        this.sqlSession.delete("deleteDicCode", dicCode);
        this.sqlSession.delete("deleteAllDicItem", dicCode);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> deleteItem(String dicCode, String itemCode) {
        DicCodeItemReq param = new DicCodeItemReq();
        param.setDicCode(dicCode);
        param.setItemCode(itemCode);
        this.sqlSession.delete("deleteDicItem", param);
        return ResponseData.success();
    }

    @Override
    public ResponseData<List<String>> groups() {
        return ResponseData.success(this.sqlSession.selectList("dicCodeItem.qryGroups"));
    }
}
