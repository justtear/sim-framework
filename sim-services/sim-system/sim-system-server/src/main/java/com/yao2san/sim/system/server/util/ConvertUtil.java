package com.yao2san.sim.system.server.util;

import com.alibaba.fastjson.JSONObject;
import org.yaml.snakeyaml.Yaml;
import pl.jalokim.propertiestojson.util.PropertiesToJsonConverter;

import java.util.*;

public class ConvertUtil {

    public static String convertToJson(Map<String, String> properties) {
        return new PropertiesToJsonConverter().convertToJson(properties);
    }

    public static String convertToJson(Properties properties) {
        return new PropertiesToJsonConverter().convertToJson(properties);
    }

    public static String convertToYaml(Map<String, String> properties) {
        Yaml yaml = new Yaml();
        String s = convertToJson(properties);
        return yaml.dumpAsMap(JSONObject.parseObject(s,LinkedHashMap.class));
    }

    public static String convertToYaml(Properties properties) {
        return new PropertiesToJsonConverter().convertToJson(properties);
    }

    public static void main(String[] args) {
        Map<String,String> m = new HashMap<>();
        m.put("spring.application.name","asds");
        m.put("spring.application.aa","11");
        System.out.println(convertToYaml(m));
    }
}
