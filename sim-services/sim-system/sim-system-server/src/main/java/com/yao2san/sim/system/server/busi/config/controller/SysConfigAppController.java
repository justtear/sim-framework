package com.yao2san.sim.system.server.busi.config.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.system.server.busi.config.bean.SysConfigApp;
import com.yao2san.sim.system.server.busi.config.service.SysConfigAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典编码/字典项管理
 */
@RestController
@RequestMapping("config/app")
public class SysConfigAppController {
    @Autowired
    private SysConfigAppService service;


    @GetMapping
    public ResponseData<List<SysConfigApp>> list(SysConfigApp req) {
        return ResponseData.success(service.list(req));
    }

    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated(SysConfigApp.Add.class) SysConfigApp req) {
        service.add(req);
        return ResponseData.success();
    }

    @DeleteMapping("{appCode}")
    public ResponseData<Void> delete(@PathVariable("appCode") String appCode) {
        service.delete(appCode);
        return ResponseData.success();
    }
}
