package com.yao2san.sim.system.server.busi.config.service;

import com.yao2san.sim.system.server.busi.config.bean.SysConfigApp;

import java.util.List;

public interface SysConfigAppService {
    List<SysConfigApp> list(SysConfigApp sysConfigApp);

    void add(SysConfigApp sysConfigApp);

    void delete(String appCode);


}
