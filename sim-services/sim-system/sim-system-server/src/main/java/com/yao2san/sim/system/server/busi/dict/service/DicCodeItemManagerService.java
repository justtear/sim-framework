package com.yao2san.sim.system.server.busi.dict.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.system.server.busi.dict.bean.request.DicCodeItemReq;
import com.yao2san.sim.system.server.busi.dict.bean.response.DicCodeItemRes;

import java.util.List;

public interface DicCodeItemManagerService {
    ResponseData<PageInfo<DicCodeItemRes>> qryCode(DicCodeItemReq req);

    ResponseData<Void> addCode(DicCodeItemReq req);

    ResponseData<Void> updateCode(DicCodeItemReq req);

    ResponseData<Boolean> codeExist(String dicCode);

    ResponseData<PageInfo<DicCodeItemRes>> qryItem(DicCodeItemReq req);
    ResponseData<Void> addItem(DicCodeItemReq req);

    ResponseData<Void> updateItem(DicCodeItemReq req);

    ResponseData<Void> deleteCode(String dicCode);

    ResponseData<Void> deleteItem(String dicCode, String itemCode);

    ResponseData<List<String>> groups();
}
