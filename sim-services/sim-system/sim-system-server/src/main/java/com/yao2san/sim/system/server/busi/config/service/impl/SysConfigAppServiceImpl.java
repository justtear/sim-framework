package com.yao2san.sim.system.server.busi.config.service.impl;

import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.system.server.busi.config.bean.SysConfig;
import com.yao2san.sim.system.server.busi.config.bean.SysConfigApp;
import com.yao2san.sim.system.server.busi.config.service.SysConfigService;
import com.yao2san.sim.system.server.busi.config.service.SysConfigAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
public class SysConfigAppServiceImpl extends BaseServiceImpl implements SysConfigAppService {
    @Autowired
    private SysConfigService configService;

    @Override
    public List<SysConfigApp> list(SysConfigApp sysConfigApp) {
        return this.sqlSession.selectList("sysConfigApp.list", null);
    }

    @Override
    @Transactional
    public void add(SysConfigApp sysConfigApp) {
        Integer count = this.sqlSession.selectOne("sysConfigApp.countCode", sysConfigApp);
        if (count > 0) {
            throw new BusiException("应用编码[" + sysConfigApp.getAppCode() + "]已存在");
        }

        this.sqlSession.insert("sysConfigApp.add", sysConfigApp);

        //添加默认配置
        /*getDefaultConfig(sysConfigApp).forEach(config -> {
            configService.add(config);
        });*/
    }

    private List<SysConfig> getDefaultConfig(SysConfigApp sysConfigApp) {
        SysConfig name = SysConfig.builder()
                .application(sysConfigApp.getAppCode())
                .profile("default")
                .label("latest")
                .key("spring.application.name")
                .value(sysConfigApp.getAppCode())
                .description("默认配置")
                .build();
        SysConfig profile = SysConfig.builder()
                .application(sysConfigApp.getAppCode())
                .profile("default")
                .label("latest")
                .key("spring.profiles.active")
                .value("default")
                .description("默认配置")
                .build();

        return Arrays.asList(name, profile);
    }

    @Override
    @Transactional
    public void delete(String appCode) {
        this.sqlSession.delete("sysConfigApp.deleteAllConfig", appCode);
        this.sqlSession.delete("sysConfigApp.deleteAllConfigHis", appCode);
        this.sqlSession.delete("sysConfigApp.delete", appCode);
    }
}
