package com.yao2san.sim.system.server.busi.dict.controller;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.system.server.busi.dict.bean.request.DicCodeItemReq;
import com.yao2san.sim.system.server.busi.dict.bean.response.DicCodeItemRes;
import com.yao2san.sim.system.server.busi.dict.service.DicCodeItemManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典编码/字典项管理
 */
@RestController
@RequestMapping("dicCodeItem")
public class DicCodeItemManagerController {
    @Autowired
    private DicCodeItemManagerService service;

    /**
     * 查询字典编码
     */
    @GetMapping("code")
    public ResponseData<PageInfo<DicCodeItemRes>> qryCode(DicCodeItemReq req) {
        return service.qryCode(req);
    }

    @GetMapping("item")
    public ResponseData<PageInfo<DicCodeItemRes>> qryItem(DicCodeItemReq req) {
        return service.qryItem(req);
    }


    @PostMapping("code")
    public ResponseData<Void> addCode(@RequestBody DicCodeItemReq req) {
        return service.addCode(req);
    }

    @PatchMapping("code")
    ResponseData<Void> updateCode(@RequestBody DicCodeItemReq req) {
        return service.updateCode(req);
    }

    @PostMapping("item")
    public ResponseData<Void> addItem(@RequestBody DicCodeItemReq req) {
        return service.addItem(req);
    }

    @PatchMapping("item")
    ResponseData<Void> updateItem(@RequestBody DicCodeItemReq req) {
        return service.updateItem(req);
    }

    @DeleteMapping("code")
    public ResponseData<Void> deleteCode(String dicCode) {
        return service.deleteCode(dicCode);
    }

    @DeleteMapping("item")
    public ResponseData<Void> req(String dicCode, String itemCode) {
        return service.deleteItem(dicCode, itemCode);
    }

    @GetMapping("groups")
    public ResponseData<List<String>> groups() {
        return service.groups();
    }

    @GetMapping("codeExist")
    public ResponseData<Boolean> codeExist(String dicCode) {
        return service.codeExist(dicCode);
    }
}
