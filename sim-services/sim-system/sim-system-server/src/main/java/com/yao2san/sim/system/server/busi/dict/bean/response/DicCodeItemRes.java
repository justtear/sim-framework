package com.yao2san.sim.system.server.busi.dict.bean.response;

import com.yao2san.sim.system.server.busi.dict.bean.request.DicCodeItemReq;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DicCodeItemRes extends DicCodeItemReq {

}
