package com.yao2san.sim.system.server.util;

import com.yao2san.sim.framework.utils.BeanContextUtil;
import org.springframework.cloud.config.server.encryption.EncryptionController;
import org.springframework.http.MediaType;

public class EncryptUtil {
    public static String encrypt(String application, String profiles, String data) {
        EncryptionController encryption = BeanContextUtil.getBean(EncryptionController.class);
        return encryption.encrypt(application, profiles, data, MediaType.APPLICATION_JSON);
    }

}
