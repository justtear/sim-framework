package com.yao2san.sim.system.server.busi.config.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.system.server.busi.config.bean.SysConfig;
import com.yao2san.sim.system.server.busi.config.bean.request.SysConfigPublishReq;
import com.yao2san.sim.system.server.busi.config.bean.request.SysConfigRollbackReq;

import java.util.List;

public interface SysConfigService {
    PageInfo<SysConfig> list(SysConfig sysConfig);

    void add(SysConfig sysConfig);

    void update(SysConfig sysConfig);

    void delete(SysConfig sysConfig);

    void restore(SysConfig sysConfig);

    String content(SysConfig sysConfig);

    List<String> versions(String application,String profile);

    void publish(SysConfigPublishReq req);

    void rollback(SysConfigRollbackReq req);
}
