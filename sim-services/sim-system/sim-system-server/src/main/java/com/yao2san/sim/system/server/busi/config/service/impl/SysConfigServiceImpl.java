package com.yao2san.sim.system.server.busi.config.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.cache.utils.CacheUtil;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.system.server.busi.config.bean.SysConfig;
import com.yao2san.sim.system.server.busi.config.bean.request.SysConfigPublishReq;
import com.yao2san.sim.system.server.busi.config.bean.request.SysConfigRollbackReq;
import com.yao2san.sim.system.server.busi.config.service.SysConfigService;
import com.yao2san.sim.system.server.enums.ConfigEnum;
import com.yao2san.sim.system.server.util.ConvertUtil;
import com.yao2san.sim.system.server.util.EncryptUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Service
public class SysConfigServiceImpl extends BaseServiceImpl implements SysConfigService {

    private static final String CACHE_KEY_PREFIX = "sim:config:refresher";
    private static final String COMMON_APPLICATION = "common";
    private static final String ENCRYPT_PREFIX = "{cipher}";

    @Override
    public PageInfo<SysConfig> list(SysConfig sysConfig) {
        PageInfo<SysConfig> list = this.qryList("sysConfig.list", sysConfig);
        return list;
    }

    @Override
    @Transactional
    public void add(SysConfig sysConfig) {
        Integer count = sqlSession.selectOne("sysConfig.count", sysConfig);
        if (count > 0) {
            throw new BusiException("配置项[" + sysConfig.getKey() + "]已存在");
        }
        sysConfig.setStatus(ConfigEnum.ConfigStatus.ADDED.getValue());
        sysConfig.setVersion("");

        if(sysConfig.getIsEncrypt()){
            sysConfig.setValue(ENCRYPT_PREFIX + EncryptUtil.encrypt(sysConfig.getApplication(),sysConfig.getProfile(),sysConfig.getValue()));
        }
        sqlSession.insert("sysConfig.add", sysConfig);
    }

    private String getVersion(String application, String profile) {
        return UUID.randomUUID().toString().substring(0,8);
    }


    @Override
    @Transactional
    public void update(SysConfig sysConfig) {
        if(sysConfig.getIsEncrypt() && !StringUtils.startsWith(sysConfig.getValue(), ENCRYPT_PREFIX)){
            sysConfig.setValue(ENCRYPT_PREFIX + EncryptUtil.encrypt(sysConfig.getApplication(),sysConfig.getProfile(),sysConfig.getValue()));
        }
        if (ConfigEnum.ConfigStatus.ADDED.getValue().equals(sysConfig.getStatus())) {
            sqlSession.update("sysConfig.updateValueForNew", sysConfig);
        } else {
            sqlSession.update("sysConfig.update", sysConfig);
        }
    }

    @Override
    public void delete(SysConfig sysConfig) {
        if (ConfigEnum.ConfigStatus.ADDED.getValue().equals(sysConfig.getStatus())) {
            //仅逻辑删除 标记状态为"3"(待删除)
            sqlSession.delete("sysConfig.delete", sysConfig);
        } else {
            //仅逻辑删除 标记状态为"3"(待删除)
            sqlSession.delete("sysConfig.updateToDelete", sysConfig);
        }
    }

    @Override
    @Transactional
    public void restore(SysConfig sysConfig) {
        this.sqlSession.update("sysConfig.restoreValue", sysConfig);
    }

    @Override
    public String content(SysConfig sysConfig) {
        sysConfig.setPageNum(1);
        sysConfig.setPageSize(Integer.MAX_VALUE);
        List<SysConfig> list = this.sqlSession.selectList("sysConfig.list", sysConfig);
        Map<String, String> properties = new LinkedHashMap<>();
        list.forEach(v -> properties.put(v.getKey(), v.getValue()));

        String dataType = sysConfig.getDataType().toUpperCase();

        if (SysConfig.DataType.LIST.name().equals(dataType)) {
            return JSONObject.toJSONString(list);
        }
        if (SysConfig.DataType.JSON.name().equals(dataType)) {
            return ConvertUtil.convertToJson(properties);
        }
        if (SysConfig.DataType.YAML.name().equals(dataType)) {
            return ConvertUtil.convertToYaml(properties);
        }
        if (SysConfig.DataType.PROPERTIES.name().equals(dataType)) {
            return JSONObject.toJSONString(properties);
        }
        return null;
    }


    @Override
    public List<String> versions(String application, String profile) {
        SysConfig sysConfig = new SysConfig();
        sysConfig.setApplication(application);
        sysConfig.setProfile(profile);
        return this.sqlSession.selectList("sysConfig.qrVersion", sysConfig);
    }

    @Override
    @Transactional
    public void publish(SysConfigPublishReq req) {
        //1.生成版本号
        String version = getVersion(req.getApplication(), req.getProfile());
        //2.更新修改后的值
        this.sqlSession.update("sysConfig.updateValue", req);
        //3.清理修改后的值
        this.sqlSession.update("sysConfig.cleanNewValue", req);
        //4.清理标记为删除的配置
        this.sqlSession.update("sysConfig.cleanDeletedConfig", req);
        //5.更新状态为有效 并更新版本号
        req.setVersion(version);
        req.setStatus(ConfigEnum.ConfigStatus.PUBLISHED.getValue());
        this.sqlSession.update("sysConfig.updateStatusAndVersion", req);
        //6.写入历史表
        this.sqlSession.insert("sysConfig.copyToHis", req);
        //7.更新缓存
        CacheUtil.hset(CACHE_KEY_PREFIX, req.getApplication(), version);

        //更新公共配置时 同步更新所有应用
        if (COMMON_APPLICATION.equals(req.getApplication())) {
            List<String> allApplications = this.sqlSession.selectList("sysConfig.qryAllApplication");
            allApplications.forEach(v -> CacheUtil.hset(CACHE_KEY_PREFIX, v, version));
        }
    }

    @Override
    @Transactional
    public void rollback(SysConfigRollbackReq req) {
        this.sqlSession.delete("sysConfig.cleanConfig", req);
        this.sqlSession.insert("sysConfig.rollback", req);
        CacheUtil.hset(CACHE_KEY_PREFIX, req.getApplication(), req.getVersion());

        //更新公共配置时 同步更新所有应用
        if (COMMON_APPLICATION.equals(req.getApplication())) {
            List<String> allApplications = this.sqlSession.selectList("sysConfig.qryAllApplication");
            //改变version触发客户端重新拉取配置
            allApplications.forEach(v -> CacheUtil.hset(CACHE_KEY_PREFIX, v, req.getVersion()));
        }
    }

}
