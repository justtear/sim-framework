package com.yao2san;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@Slf4j
@EnableConfigServer
public class SimSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimSystemApplication.class, args);
        log.info("sim-system-server start success!");
    }
}
