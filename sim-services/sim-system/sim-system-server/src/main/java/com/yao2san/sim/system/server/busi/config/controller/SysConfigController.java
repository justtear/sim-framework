package com.yao2san.sim.system.server.busi.config.controller;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.system.server.busi.config.bean.SysConfig;
import com.yao2san.sim.system.server.busi.config.bean.request.SysConfigPublishReq;
import com.yao2san.sim.system.server.busi.config.bean.request.SysConfigRollbackReq;
import com.yao2san.sim.system.server.busi.config.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典编码/字典项管理
 */
@RestController
@RequestMapping("config")
public class SysConfigController {
    @Autowired
    private SysConfigService service;


    /**
     * 查询配置列表
     */
    @GetMapping
    public ResponseData<PageInfo<SysConfig>> list(SysConfig req) {
        return ResponseData.success(service.list(req));
    }

    /**
     * 新增一个配置
     */
    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated(SysConfig.Add.class) SysConfig req) {
        service.add(req);
        return ResponseData.success();
    }

    /**
     * 修改配置
     */
    @PatchMapping
    public ResponseData<Void> update(@RequestBody @Validated(SysConfig.Update.class) SysConfig req) {
        service.update(req);
        return ResponseData.success();
    }

    /**
     * 删除一个配置
     */
    @DeleteMapping
    public ResponseData<Void> delete(@Validated(SysConfig.Update.class) SysConfig req) {
        service.delete(req);
        return ResponseData.success();
    }

    /**
     * 还原一个配置
     */
    @PatchMapping("restore")
    public ResponseData<Void> restore(@RequestBody SysConfig req) {
        service.restore(req);
        return ResponseData.success();
    }

    @GetMapping("content")
    public ResponseData<String> content(SysConfig req) {
        return ResponseData.success(service.content(req));
    }

  /*  @PatchMapping("content")
    public ResponseData<String> updateConfig(String configJson) {
        service.updateConfig(configJson);
        return ResponseData.success();
    }*/


    /**
     * 获取历史版本号
     */
    @GetMapping("versions")
    public ResponseData<List<String>> versions(String application, String profile) {
        return ResponseData.success(service.versions(application, profile));
    }

    /**
     * 发布配置
     */
    @PostMapping("publish")
    public ResponseData<Void> publish(@RequestBody SysConfigPublishReq req) {
        service.publish(req);
        return ResponseData.success();
    }

    /**
     * 配置回滚
     */
    @PostMapping("rollback")
    public ResponseData<Void> rollback(@RequestBody SysConfigRollbackReq req) {
        service.rollback(req);
        return ResponseData.success();
    }
}
