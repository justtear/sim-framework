package com.yao2san.sim.system.config.client;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.yao2san.sim.system.config.client")
public class SimConfigClientConfig {
}
