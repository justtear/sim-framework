package com.yao2san.sim.system.config.client;

import com.yao2san.sim.framework.cache.utils.CacheUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class ConfigRefresher {

    @Value("${spring.application.name:}")
    private String applicationName;
    @Autowired
    private ContextRefresher refresher;
    private static String VERSION;

    private ScheduledExecutorService executorService;

    private static final String CACHE_KEY_PREFIX = "sim:config:refresher";
    private static final long REFRESH_INTERVAL_SECONDS = 10;

    @PostConstruct
    public void init() {
        executorService = Executors.newScheduledThreadPool(1);

        this.start();
    }

    public void start() {
        if (StringUtils.isEmpty(applicationName)) {
            log.warn("The config spring.application.name is not set, will be not refresh config!");
            return;
        }
        log.info("Stating refresh config thread...");
        executorService.scheduleWithFixedDelay(() -> {
            String v = CacheUtil.hget(CACHE_KEY_PREFIX, applicationName);

            if (v == null || v.equals(VERSION)) {
                return;
            }

            log.info("Stating refresh config.new version:{},old version:{}", v, VERSION);
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            refresher.refresh();
            stopWatch.stop();
            log.info("Config refreshed, use {} ms", stopWatch.getTotalTimeMillis());

            VERSION = v;

        }, REFRESH_INTERVAL_SECONDS, REFRESH_INTERVAL_SECONDS, TimeUnit.SECONDS);
        log.info("Refresh config thread started.");
    }


}
