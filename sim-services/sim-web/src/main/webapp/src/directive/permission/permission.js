import store from '@/store'

//{code:['xxx','yyy'],type:'disabled']} or {code:'xxx',type:'disabled']}
function checkPermission(el, binding) {
  const {value} = binding;
  if (!value) {
    return false;
  }
  const permissions = store.getters && store.getters.permissions.map(v => v.purviewCode);

  let hasPermission = false;
  if (typeof (value) == 'string') {
    hasPermission = permissions.some(p => p === value);
    if (!hasPermission) {
      el.parentNode && el.parentNode.removeChild(el)
    }
  } else if (value instanceof Object) {
    //交集
    let intersection = (value.code || value).filter(v => permissions.includes(v))
    let olen = (value.code || value).length
    let len = intersection.length
    if (value instanceof Array || value.code instanceof Array) {
      hasPermission = olen > 0 && len > 0 && olen === len
    } else {
      hasPermission = permissions.some(p => p === value.code);
    }
    if (!hasPermission) {
      if (value.type === 'disabled') {
        el.disabled = 'disabled';
        el.className += ' is-disabled';
      } else {
        el.parentNode && el.parentNode.removeChild(el)
      }
    }
  }
}

export default {
  inserted(el, binding) {
    checkPermission(el, binding)
  },
  update(el, binding) {
    checkPermission(el, binding)
  }
}
