import {R} from './R'
import {v4 as uuidv4} from 'uuid';
import {Message} from 'element-ui'
import SimStorageClient from "../../packages/sim-storage-js-sdk/client";

const OSS = require('ali-oss')
const COS = require('cos-js-sdk-v5')

const GET_CONFIG_API = '/api/storage/manager/config'
const NOTIFY_API = '/api/storage/manager/notify'

//全局任务队列
let task = {}

function getClient(config) {
  const {credentials, bucket, endpoint, storageType, region, path} = config
  const {accessKey, secretKey, securityToken, expiration} = credentials

  let client
  if (storageType === 'ALI_OSS') {
    client = new OSS({
      endpoint: endpoint,
      region: region,
      accessKeyId: accessKey,
      accessKeySecret: secretKey,
      stsToken: securityToken,
      refreshSTSToken: async () => {
        return {
          accessKeyId: accessKey,
          accessKeySecret: secretKey,
          stsToken: securityToken,
        }
      },
      refreshSTSTokenInterval: -1,
      bucket: bucket,
      timeout: 600000,
      secure: true
    })
  }
  if (storageType === 'TENCENT_COS') {
    client = new COS({
      getAuthorization: async (options, callback) => {
        callback({
          TmpSecretId: accessKey,
          TmpSecretKey: secretKey,
          SecurityToken: securityToken,
          StartTime: parseInt(Date.now() / 1000 + ''),
          ExpiredTime: parseInt(Date.parse(expiration) / 1000 + ''),
        });
      }
    })
  }
  if (storageType === 'LOCAL') {
    client = new SimStorageClient({
      endpoint: endpoint,
      accessKeyId: accessKey,
      accessKeySecret: secretKey,
      stsToken: securityToken,
    });
  }
  return {
    type: storageType,
    client: client
  }
}

/**
 * 通知storage-server保存文件信息
 * @param param
 */
function notify(param) {
  return R.postJson(NOTIFY_API, param, {ignoreRepeatable: true})
}

export const uploader = {
  /**
   * 上传文件
   * @param uploadOptions
   * @returns {Promise<void>}
   */
  async upload(uploadOptions) {
    const data = await R.get(GET_CONFIG_API + '?openId=' + uploadOptions.openId)
    const config = data.data;
    const {path, bucket, region, endpoint} = config
    const dir = uploadOptions.dir
    const file = uploadOptions.file
    const uuid = uuidv4();
    const object = ((path || '/') + '/' + (dir || '/') + '/' + (uploadOptions.object || (uuid + '_' + file.name))).replace(/\/+/g, '/')
    const key = file.name

    if (!task[key] || !uploadOptions.isResume) {
      task[key] = {
        options: uploadOptions,
        storageClient: getClient(config)
      }
    }

    const storageClient = task[key].storageClient
    const client = storageClient.client
    const type = storageClient.type

    if (type === 'ALI_OSS') {
      try {
        await client.multipartUpload(object, file, {
          progress: (p, cpt, res) => {
            //checkpoint
            task[key].options.checkpoint = cpt
            if (uploadOptions.progress) {
              uploadOptions.progress(p, cpt)
            }
          },
          checkpoint: uploadOptions.checkpoint || 0
        });
      } catch (e) {
        throw e
      }
      notify({
        content: {
          object: object,
          size: file.size,
          lastModified: file.lastModified,
          originalName: file.name,
          contentType: file.type
        },
        meta: {
          openId: uploadOptions.openId,
          notifyType: 'UPLOAD',
          storageType: type
        }
      })
    }
    if (type === 'TENCENT_COS') {
      await client.uploadFile({
        Bucket: bucket,
        Region: region,
        Key: object,
        Body: file,
        SliceSize: 1024 * 1024 * 10, //超出10M 分片上传
        onTaskReady: (taskId) => {
          task[key].options.taskId = taskId;
        },
        onProgress: (progressData) => {
          const {loaded, speed, percent} = progressData;
          if (uploadOptions.progress) {
            uploadOptions.progress(percent, task[key].options.taskId)
          }
        },
        onFileFinish: (err, data, options) => {
          if (err) {
            Message.error(err)
          } else {
            notify({
              content: {
                object: object,
                size: file.size,
                lastModified: file.lastModified,
                originalName: file.name,
                contentType: file.type,
                url: data.Location
              },
              meta: {
                openId: uploadOptions.openId,
                notifyType: 'UPLOAD',
                storageType: type
              }
            })
          }
        },
      }, function (err, data) {
        if (err) {
          throw err;
        }
      })
    }


    if (type === 'LOCAL') {
      //这里只取用户传入的文路径+文件名
      const f = ((dir || '/') + '/' + (uploadOptions.object || (uuid + '_' + file.name))).replace(/\/+/g, '/')
      await client.upload({
        file: file,
        object: f,
        onProgress: (evt) => {
          if (uploadOptions.progress) {
            const {loaded, total} = evt;
            const percent = loaded / total
            uploadOptions.progress(percent, task[key].options.taskId)
          }
        }
      })
      notify({
        content: {
          object: object,
          size: file.size,
          lastModified: file.lastModified,
          originalName: file.name,
          contentType: file.type
        },
        meta: {
          openId: uploadOptions.openId,
          notifyType: 'UPLOAD',
          storageType: type
        }
      })
    }
  },
  /**
   * 暂停上传
   * @param fileName
   */
  pause(fileName) {
    const t = task[fileName]
    const options = t.options
    const storageClient = t.storageClient
    const type = storageClient.type
    const client = storageClient.client

    options.isResume = false

    if (type === 'ALI_OSS') {
      client.cancel()
    }
    if (type === 'TENCENT_COS') {
      client.pauseTask(options.taskId)
    }
  },
  /**
   * 取消上传
   * @param fileName
   */
  cancel(fileName) {

  },
  /**
   * 恢复上传
   * @param fileName
   */
  resume(fileName) {
    const t = task[fileName]
    const options = t.options
    const storageClient = t.storageClient
    const type = storageClient.type
    const client = storageClient.client
    if (type === 'ALI_OSS') {
      t.options.isResume = true
      this.upload(t.options)
    }
    if (type === 'TENCENT_COS') {
      client.restartTask(options.taskId)
    }
  },
  /**
   * 清除上传任务
   */
  cleanTask() {
    task = {};
  }
}

export const downloader = {
  async download(options) {
    const data = await R.get(GET_CONFIG_API + '?openId=' + options.openId)
    const config = data.data
    const {path, bucket, region, endpoint} = config
    const storageClient = getClient(config)
    const type = storageClient.type
    const client = storageClient.client
    const object = options.object
    const originalName = options.originalName
    if (type === 'ALI_OSS') {
      const response = {
        'content-disposition': `attachment; filename=${encodeURIComponent(originalName)}`
      }
      const url = client.signatureUrl(object, {response});
      window.location.href = (url);
    }
    if (type === 'TENCENT_COS') {
      client.getObjectUrl({
        Bucket: bucket, /* 填入您自己的存储桶，必须字段 */
        Region: region,  /* 存储桶所在地域，例如ap-beijing，必须字段 */
        Key: object,  /* 存储在桶里的对象键（例如1.jpg，a/b/test.txt），必须字段 */
        Sign: true,
      }, function (err, data) {
        if (err) return console.error(err);
        const url = data.Url + (data.Url.indexOf('?') > -1 ? '&' : '?') + `response-content-disposition=attachment; filename=${encodeURIComponent(originalName)}`; // 补充强制下载的参数
        window.location.href = (url);
      });
    }
    if (type === 'LOCAL') {
      const url = client.signatureUrl({
        endpoint,
        object: object,
        response: {
          'content-disposition': `attachment; filename=${encodeURIComponent(originalName)}`
        }
      });
      window.location.href = (url);
    }
  }
}

export const manager = {
  async del(options) {
    const data = await R.get(GET_CONFIG_API + '?openId=' + options.openId)
    const config = data.data
    const {path, bucket, region, endpoint} = config
    const storageClient = getClient(config)
    const type = storageClient.type
    const client = storageClient.client
    const object = options.object
    if (type === 'ALI_OSS') {
      await client.delete(object, {
        quiet: true
      })
      await notify({
        content: {
          object: object,
        },
        meta: {
          openId: options.openId,
          notifyType: 'DELETE',
          storageType: type
        }
      })
      if (options.callback) {
        options.callback()
      }

    }
    if (type === 'TENCENT_COS') {
      await client.deleteObject({
        Bucket: bucket,
        Region: region,
        Key: object,
      }, async function (err, data) {
        if (err) {
          console.error(err)
          return
        }
        await notify({
          content: {
            object: object,
          },
          meta: {
            openId: options.openId,
            notifyType: 'DELETE',
            storageType: type
          }
        })
        if (options.callback) {
          options.callback()
        }
      });
    }
    if (type === 'LOCAL') {
      await client.delete({
        object
      });
      await notify({
        content: {
          object: object,
        },
        meta: {
          openId: options.openId,
          notifyType: 'DELETE',
          storageType: type
        }
      })
      if (options.callback) {
        options.callback()
      }
    }

  }
}
export default {
  uploader, downloader, manager
}
