/**
 * Sim Framework 存储客户端
 */

import {v4 as uuidv4} from 'uuid';
import axios from 'axios'

import SimStorageClient from "./client";

import OSS from 'ali-oss'
import COS from 'cos-js-sdk-v5'

/**
 * 存储客户端
 * @param options
 * @constructor
 */
function SimStorage(options) {
  this._options = options
}

const proto = SimStorage.prototype;

/**
 * 初始化
 * @returns {Promise<SimStorage>}
 */
proto.init = async function () {
  this._server = this._options.server
  this._task = {}
  this._config = await this._getConfig()
  return this
}

/**
 * 获取存储配置
 * @returns {Promise<*>}
 * @private
 */
proto._getConfig = async function () {
  if (this._options.config) {
    return this._options.config
  }
  const data = await axios.get(this._server + '/config')
  return data.data.data
}

/**
 * 上传文件
 * @param uploadOptions
 * @returns {Promise<void|*>}
 */
proto.upload = async function (uploadOptions) {
  const {file} = uploadOptions

  const task = this._getTask(file, uploadOptions, this._config)
  const storageClient = task.storageClient
  const storageType = storageClient.type

  switch (storageType) {
    case 'ALI_OSS':
      return await this._uploadOSS(uploadOptions, storageClient.client);
    case 'TENCENT_COS':
      return await this._uploadCOS(uploadOptions, storageClient.client);
    case 'LOCAL':
      return await this._uploadLocal(uploadOptions, storageClient.client);
    default:
      throw new Error('Unsupported storage type')
  }
}

/**
 * 获取上传任务
 * @param file 待上传的文件
 * @param uploadOptions 上传参数
 * @param config 存储配置
 * @returns {*}
 * @private
 */
proto._getTask = function (file, uploadOptions, config) {
  if (!file) {
    throw new Error('file is null')
  }
  const key = file.name
  const task = this._task
  if (!task[key] || !uploadOptions._isResume) {
    task[key] = {
      options: uploadOptions,
      storageClient: this._getStorageClient(config)
    }
  }
  return task[key]
}

/**
 * 根据存储类型获取存储客户端
 * @param config 存储配置
 * @returns {{client: SimStorageClient, type: *}}
 * @private
 */
proto._getStorageClient = function (config) {
  let storageType = config.storageType
  let client
  switch (storageType) {
    case 'ALI_OSS':
      client = this._getOSSClient(config);
      break
    case 'TENCENT_COS':
      client = this._getCOSClient(config);
      break
    case 'LOCAL':
      client = this._getLocalClient(config);
      break
    default:
      throw new Error('Unsupported storage type')
  }
  return {
    type: storageType,
    client: client
  }
}

/**
 * 获取OSS client
 * @param config
 * @returns {*}
 * @private
 */
proto._getOSSClient = function (config) {
  return new OSS({
    endpoint: config.endpoint,
    region: config.region,
    accessKeyId: config.credentials.accessKey,
    accessKeySecret: config.credentials.secretKey,
    stsToken: config.credentials.securityToken,
    refreshSTSToken: async () => {
      return {
        accessKeyId: config.credentials.accessKey,
        accessKeySecret: config.credentials.secretKey,
        stsToken: config.credentials.securityToken,
      }
    },
    refreshSTSTokenInterval: -1,
    bucket: config.bucket,
    timeout: 600000,
    secure: true
  })
}

/**
 * 获取COS client
 * @param config
 * @returns {*}
 * @private
 */
proto._getCOSClient = function (config) {
  return new COS({
    getAuthorization: async (options, callback) => {
      callback({
        TmpSecretId: config.credentials.accessKey,
        TmpSecretKey: config.credentials.secretKey,
        SecurityToken: config.credentials.securityToken,
        StartTime: parseInt(Date.now() / 1000 + ''),
        ExpiredTime: parseInt(Date.parse(config.credentials.expiration) / 1000 + ''),
      });
    }
  })
}

/**
 * 获取本地存储client
 * @param config
 * @returns {SimStorageClient}
 * @private
 */
proto._getLocalClient = function (config) {
  return new SimStorageClient({
    endpoint: config.endpoint,
    accessKeyId: config.credentials.accessKey,
    accessKeySecret: config.credentials.secretKey,
    stsToken: config.credentials.securityToken,
  });
}

/**
 * 上传到OSS
 * @param uploadOptions
 * @param client
 * @returns {Promise<*>}
 * @private
 */
proto._uploadOSS = async function (uploadOptions, client) {
  const {file, path, dir} = uploadOptions
  const object = getObject(path, dir, uploadOptions.object, file.name)
  try {
    await client.multipartUpload(object, file, {
      progress: (p, cpt, res) => {
        //checkpoint
        uploadOptions._checkpoint = cpt
        if (uploadOptions.progress) {
          uploadOptions.progress(p, cpt)
        }
      },
      checkpoint: uploadOptions._checkpoint || 0
    });
  } catch (e) {
    throw e
  }
  await this._notify({
    content: {
      object: object,
      size: file.size,
      lastModified: file.lastModified,
      originalName: file.name,
      contentType: file.type
    },
    meta: {
      openId: this._config.openId,
      notifyType: 'UPLOAD',
      storageType: 'ALI_OSS'
    }
  })
  this._cleanTask(file.name)
  return object
}

/**
 * 上传到COS
 * @param uploadOptions
 * @param client
 * @returns {Promise<*>}
 * @private
 */
proto._uploadCOS = async function (uploadOptions, client) {
  const {bucket, region} = this._config
  const {file, path, dir} = uploadOptions
  const object = getObject(path, dir, uploadOptions.object, file.name)
  await client.uploadFile({
    Bucket: bucket,
    Region: region,
    Key: object,
    Body: file,
    SliceSize: 1024 * 1024 * 10, //超出10M 分片上传
    onTaskReady: (taskId) => {
      uploadOptions._taskId = taskId
    },
    onProgress: (progressData) => {
      const {loaded, speed, percent} = progressData;
      if (uploadOptions.progress) {
        uploadOptions.progress(percent, uploadOptions._taskId)
      }
    },
    onFileFinish: (err, data, options) => {
      if (err) {
        console.error(err)
      } else {
        this._notify({
          content: {
            object: object,
            size: file.size,
            lastModified: file.lastModified,
            originalName: file.name,
            contentType: file.type,
            url: data.Location
          },
          meta: {
            openId: this._config.openId,
            notifyType: 'UPLOAD',
            storageType: 'TENCENT_COS'
          }
        })
      }
      this._cleanTask(file.name)
    },
  }, (err, data) => {
    if (err) {
      this._cleanTask(file.name)
      throw err;
    }
  })
  return object
}

/**
 * 上传到自定义存储
 * @param uploadOptions
 * @param client
 * @returns {Promise<*>}
 * @private
 */
proto._uploadLocal = async function (uploadOptions, client) {
  const {endpoint, path} = this._config
  const {dir, file} = uploadOptions
  const obj = getObject(null, dir, uploadOptions.object, file.name);//((dir || '/') + '/' + (uploadOptions.object || (uuid + '_' + file.name))).replace(/\/+/g, '/')
  await client.upload({
    endpoint: endpoint,
    file: file,
    object: obj,
    onProgress: (evt) => {
      if (uploadOptions.progress) {
        const {loaded, total} = evt;
        const percent = loaded / total
        uploadOptions.progress(percent)
      }
    }
  })

  await this._notify({
    content: {
      object: formatPath(path + '/' + obj),
      size: file.size,
      lastModified: file.lastModified,
      originalName: file.name,
      contentType: file.type
    },
    meta: {
      openId: this._config.openId,
      notifyType: 'UPLOAD',
      storageType: 'LOCAL'
    }
  })
  this._cleanTask(file.name)

  return obj
}

/**
 * 清除上传任务
 * @param key key为文件名
 * @private
 */
proto._cleanTask = function (key) {
  if (this._task && this._task[key]) {
    delete this._task[key]
  }
}

/**
 * 通知sim-storage-server
 * @param param
 * @returns {Promise<void>}
 * @private
 */
proto._notify = async function (param) {
  if (this._options.notify) {
    return await this._options.notify(param)
  }
  const url = (this._server || '') + '/notify'
  return await axios.post(url, param)
}

/**
 * 暂停上传
 * @param fileName
 */
proto.pause = function (fileName) {
  const t = this._task[fileName]
  const options = t.options
  const storageClient = t.storageClient
  const type = storageClient.type
  const client = storageClient.client

  options._isResume = false

  switch (type) {
    case 'ALI_OSS':
      client.cancel()
      break
    case 'TENCENT_COS':
      client.pauseTask(options._taskId)
      break
    case 'LOCAL':
      break
  }

}

/**
 * 恢复上传
 * @param fileName
 */
proto.resume = function (fileName) {
  const t = this._task[fileName]
  const options = t.options
  const storageClient = t.storageClient
  const type = storageClient.type
  const client = storageClient.client
  switch (type) {
    case 'ALI_OSS':
      t.options._isResume = true
      this._uploadOSS(t.options, this._config)
      break
    case 'TENCENT_COS':
      client.restartTask(options._taskId)
  }
}

/**
 * 文件下载
 * @param key 文件在对象存储中的唯一key
 * @param name 指定下载的文件名,可选
 */
proto.download = function (key, name) {
  const storageClient = this._getStorageClient(this._config)
  const type = storageClient.type
  const client = storageClient.client
  const downloadName = name || getNameFromPath(key).substring(37)
  switch (type) {
    case 'ALI_OSS':
      this._downloadOSS(key, downloadName, client);
      break
    case 'TENCENT_COS':
      this._downloadCOS(key, downloadName, client);
      break
    case 'LOCAL':
      this._downloadLocal(key, downloadName, client);
      break
  }
}

/**
 * OSS文件下载
 * @param object 对象名
 * @param downloadName 指定下载的文件名
 * @param client oss client
 * @private
 */
proto._downloadOSS = function (object, downloadName, client) {
  const response = {
    'content-disposition': `attachment; filename=${encodeURIComponent(downloadName)}`
  }
  const url = client.signatureUrl(object, {response});
  download(url)
}

/**
 * COS文件下载
 * @param object 对象名
 * @param downloadName 指定下载的文件名
 * @param client cos client
 * @private
 */
proto._downloadCOS = function (object, downloadName, client) {
  const {bucket, region} = this._config
  client.getObjectUrl({
    Bucket: bucket,
    Region: region,
    Key: object,
    Sign: true,
  }, function (err, data) {
    if (err) return console.error(err);
    const url = data.Url + (data.Url.indexOf('?') > -1 ? '&' : '?') + `response-content-disposition=attachment; filename=${encodeURIComponent(downloadName)}`;
    download(url)
  });
}

/**
 * 自定义存储文件下载
 * @param object 对象名
 * @param downloadName 指定下载的文件名
 * @param client local client
 * @private
 */
proto._downloadLocal = function (object, downloadName, client) {
  const {endpoint} = this._config
  const url = client.signatureUrl({
    endpoint,
    object: object,
    response: {
      'content-disposition': `attachment; filename=${encodeURIComponent(downloadName)}`
    }
  });
  download(url)
}

/**
 * 删除文件
 * @param key
 * @returns {Promise<void>}
 */
proto.delete = async function (key) {
  const storageClient = this._getStorageClient(this._config)
  const type = storageClient.type
  const client = storageClient.client
  switch (type) {
    case 'ALI_OSS':
      await this._deleteOSS(key, client)
      break
    case 'TENCENT_COS':
      await this._deleteCOS(key, client)
      break
    case 'LOCAL':
      await this._deleteLocal(key, client);
      break
  }
}

/**
 * 删除OSS文件
 * @param key
 * @param client
 * @returns {Promise<void>}
 * @private
 */
proto._deleteOSS = async function (key, client) {
  await client.delete(key, {
    quiet: true
  })
  await this._notify({
    content: {
      object: key,
    },
    meta: {
      openId: this._config.openId,
      notifyType: 'DELETE',
      storageType: 'OSS'
    }
  })
}

/**
 * 删除COS文件
 * @param key
 * @param client
 * @returns {Promise<void>}
 * @private
 */
proto._deleteCOS = async function (key, client) {
  const {bucket, region} = this._config
  await client.deleteObject({
    Bucket: bucket,
    Region: region,
    Key: key,
  }, async (err, data) => {
    if (err) {
      console.error(err)
      return
    }
    await this._notify({
      content: {
        object: key,
      },
      meta: {
        openId: this._config.openId,
        notifyType: 'DELETE',
        storageType: 'COS'
      }
    })
  });
}

/**
 * 删除自定义存储文件
 * @param key
 * @param client
 * @returns {Promise<void>}
 * @private
 */
proto._deleteLocal = async function (key, client) {
  await client.delete({
    object: key
  });
  await this._notify({
    content: {
      object: key,
    },
    meta: {
      openId: this._config.openId,
      notifyType: 'DELETE',
      storageType: 'LOCAL'
    }
  })
}


function getObject(basePath, dir, object, fileName) {
  if (object) {
    return object
  }
  const obj = ((basePath || '/') + '/' + (dir || '/') + '/' + (object || (uuidv4().replaceAll('-', '') + '_' + fileName)))
  return formatPath(obj);
}

function formatPath(path) {
  return path.replace(/(\/+|\\+)+/g, "/")
}

function getNameFromPath(path) {
  return path.substring(path.lastIndexOf('/') + 1)
}

function download(url) {
  const iframe = document.createElement("iframe");
  iframe.style.display = "none";
  iframe.style.height = 0;
  iframe.src = url;
  document.body.appendChild(iframe);
  setTimeout(() => {
    iframe.remove();
  }, 1000);
}

export default SimStorage
