#### 简介：

基于 element ui的搜索组件，支持radio、checkbox、select、日期、input等条件的选择和输入，

![示例图片](https://gitee.com/xgpxg/sim-framework/raw/sim-framework-1.0.2/images/5.png)


使用示例：

    <Search v-model.sync="searchParam"
                    :options="searchOptions"
                    size="small"
                    show-add-button
                    :inline="true"
                    @search="search"
                    @add="add">
    </Search>
    
      import Search from "sim-el-search";
    
      export default {
        name: "index",
        components: {Search},
        data() {
          return {
            searchOptions:[
              {
                type: 'select',
                prop: 'statusCd',
                label: '角色状态',
                code:'USER_STATUS',
                items:[{itemCode:'1',itemText:'有效'}]
              }
            ]
          }
        },
        methods: {
          search(){
            //Your code
          },
          add(){
            //Your code
          }
      }