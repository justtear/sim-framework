package com.yao2san.sim.open.server.busi.app.controller;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.open.server.busi.app.request.ServicePurviewReq;
import com.yao2san.sim.open.server.busi.app.response.ServicePurviewRes;
import com.yao2san.sim.open.server.busi.app.service.ServicePurviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("service-purview")
public class ServicePurviewController {

    @Autowired
    private ServicePurviewService servicePurviewService;

    @GetMapping("/service")
    public ResponseData<PageInfo<ServicePurviewRes>> services(@Validated ServicePurviewReq req) {
        return ResponseData.success(servicePurviewService.services(req));
    }

    /**
     * 服务授权or解除授权
     */
    @PostMapping("/service")
    public ResponseData<Void> serviceAuthorization(@RequestBody ServicePurviewReq req) {
        servicePurviewService.serviceAuthorization(req);
        return ResponseData.success();
    }

}
