package com.yao2san.sim.open.server.busi.app.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.open.server.busi.app.request.AppInstReq;
import com.yao2san.sim.open.server.busi.app.response.AppInstRes;
import com.yao2san.sim.open.server.busi.app.service.AppInstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("app-inst")
public class AppInstController {
    @Autowired
    private AppInstService appInstService;

    @GetMapping
    public ResponseData<List<AppInstRes>> list(AppInstReq req) {
        return appInstService.list(req);
    }

    @PostMapping
    public ResponseData<Void> add(@RequestBody AppInstReq req) {
        return appInstService.add(req);
    }
    @GetMapping("{appInstId}")
    public ResponseData<AppInstRes> detail(@PathVariable("appInstId") Long appInstId) {
        return appInstService.detail(appInstId);
    }
    @PatchMapping
    public ResponseData<Void> update(@RequestBody AppInstReq req) {
        return appInstService.update(req);
    }

    @DeleteMapping
    public ResponseData<Void> delete(AppInstReq req) {
        return appInstService.delete(req);
    }

    @PatchMapping("restSecretKey")
    public ResponseData<Void> restSecretKey(@RequestBody AppInstReq req) {
        appInstService.restSecretKey(req);
        return ResponseData.success();
    }

    @GetMapping("openId/{openId}")
    public ResponseData<AppInstRes> detailByOpenId(@PathVariable("openId") String openId) {
        appInstService.detailByOpenId(openId);
        return ResponseData.success(appInstService.detailByOpenId(openId));
    }
}
