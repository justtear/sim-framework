package com.yao2san.sim.open.server.busi.app.response;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ServicePurviewRes extends BaseBean {
    private Long servicePurviewId;
    private Long serviceId;
    private String objectType;
    private Long objectId;
    private String expDate;
    private String effDate;
    private String serviceName;
    private String serviceType;
    private String isAuth;
    private String url;
    private String description;
}
