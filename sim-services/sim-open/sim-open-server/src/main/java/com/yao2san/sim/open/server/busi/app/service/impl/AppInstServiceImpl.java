package com.yao2san.sim.open.server.busi.app.service.impl;

import com.yao2san.sim.auth.common.UserUtil;
import com.yao2san.sim.framework.utils.CommonUtil;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.open.server.busi.app.request.AppInstReq;
import com.yao2san.sim.open.server.busi.app.response.AppInstRes;
import com.yao2san.sim.open.server.busi.app.response.ServicePurviewRes;
import com.yao2san.sim.open.server.busi.app.service.AppInstService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class AppInstServiceImpl extends BaseServiceImpl implements AppInstService {
    @Override
    @Transactional
    public ResponseData<Void> add(AppInstReq req) {
        req.setOpenId(makeKey());
        req.setSecretKey(makeKey());
        this.sqlSession.insert("appInst.add", req);

        return ResponseData.success();
    }

    @Override
    public ResponseData<List<AppInstRes>> list(AppInstReq req) {
        List<AppInstRes> list = this.sqlSession.selectList("appInst.list", req);
        return ResponseData.success(list);
    }

    @Override
    @Transactional
    public ResponseData<Void> delete(AppInstReq req) {
        Map<String, Object> param = new HashMap<>();
        param.put("appInstId", req.getAppInstId());
        param.put("objectType", "app");
        param.put("createUser", UserUtil.getCurrUserId());

        this.sqlSession.delete("appInst.delete", param);
        this.sqlSession.delete("appInst.deleteServicePurviewWithObjectId", param);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> update(AppInstReq req) {
        if (req.getAppInstId() == null) {
            throw new BusiException("appInstId can not be null");
        }
        this.sqlSession.update("appInst.update", req);

        return ResponseData.success();
    }

    @Override
    public ResponseData<AppInstRes> detail(Long appInstId) {
        AppInstReq req = new AppInstReq();
        req.setAppInstId(appInstId);
        if (req.getAppInstId() == null) {
            throw new BusiException("appInstId can not be null");
        }
        AppInstRes appInst = this.sqlSession.selectOne("appInst.detail", req);
        List<ServicePurviewRes> purviewServices = this.sqlSession.selectList("appInst.qryServicePurview", req);
        appInst.setServices(purviewServices);

        return ResponseData.success(appInst);
    }

    @Override
    @Transactional
    public void restSecretKey(AppInstReq req) {
        req.setSecretKey(makeKey());
        this.sqlSession.update("appInst.updateSecretKey", req);
    }

    @Override
    public AppInstRes detailByOpenId(String openId) {
        AppInstRes appInst = this.sqlSession.selectOne("appInst.detailByOpenId", openId);
        if (appInst != null) {
            appInst.setSecretKey(null);
        }
        return appInst;
    }

    private String makeKey() {
        return CommonUtil.md5(UUID.randomUUID().toString());
    }
}
