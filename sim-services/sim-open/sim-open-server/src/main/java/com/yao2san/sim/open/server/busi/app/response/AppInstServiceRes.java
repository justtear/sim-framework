package com.yao2san.sim.open.server.busi.app.response;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AppInstServiceRes extends BaseBean {
    private Long servicePurviewId;
    private Long serviceId;
    private Long userId;
    private String serviceName;
    private String serviceType;
    private String isAuth;
    private String url;
    private String effDate;

}
