package com.yao2san.sim.open.server.busi.app.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.open.server.busi.app.request.ServicePurviewReq;
import com.yao2san.sim.open.server.busi.app.response.ServicePurviewRes;

public interface ServicePurviewService {
    void serviceAuthorization(ServicePurviewReq req);

    PageInfo<ServicePurviewRes> services(ServicePurviewReq req);
}
