package com.yao2san.sim.open.server.busi.app.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class AppInstReq extends BaseBean {
    private Long appInstId;
    @NotEmpty(message = "appName not be null")
    private String appName;
    private Set<Long> serviceIds;
    private String openId;
    @JsonIgnore
    private String secretKey;
}
