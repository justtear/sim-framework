package com.yao2san.sim.open.server.busi.auth.service;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.open.server.busi.auth.request.AuthenticationReq;
import com.yao2san.sim.open.server.busi.auth.response.AuthenticationRes;

public interface OauthService {
    /**
     * 租户信息认证
     */
    ResponseData<AuthenticationRes> authenticate(AuthenticationReq authenticationReq);
}
