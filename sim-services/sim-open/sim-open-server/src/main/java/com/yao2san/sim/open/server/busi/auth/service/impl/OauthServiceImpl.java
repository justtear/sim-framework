package com.yao2san.sim.open.server.busi.auth.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.yao2san.sim.framework.cache.utils.CacheUtil;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.open.server.busi.auth.response.AuthenticationRes;
import com.yao2san.sim.open.server.entity.AppInst;
import com.yao2san.sim.open.server.busi.auth.bean.Constant;
import com.yao2san.sim.open.server.busi.auth.request.AuthenticationReq;
import com.yao2san.sim.open.server.busi.auth.response.OpenToken;
import com.yao2san.sim.open.server.busi.auth.service.OauthService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.yao2san.sim.auth.common.Constants.TOKEN_KEY_PREFIX;

@Service
@Slf4j
public class OauthServiceImpl extends BaseServiceImpl implements OauthService {

    private static final Integer TOKEN_EXPIRE_TIME = 24;


    @Override
    public ResponseData<AuthenticationRes> authenticate(AuthenticationReq authenticationReq) {
        if (Constant.AuthType.APP.equals(authenticationReq.getScope())) {
            return authForApp(authenticationReq);
        } else if (Constant.AuthType.USER.equals(authenticationReq.getScope())) {
            return ResponseData.error("Not support, will be open!");
        } else {
            return ResponseData.error("Unsupported auth scope,usage: app|user");
        }
    }

    /**
     * 应用认证
     */
    private ResponseData<AuthenticationRes> authForApp(AuthenticationReq authenticationReq) {
        AppInst appInst = getAppInst(authenticationReq);
        //生成token
        String token = this.createToken();
        //缓存
        this.cache(appInst, token);
        //过期时间
        DateTime expireDate = DateUtil.offsetHour(new Date(), TOKEN_EXPIRE_TIME);
        AuthenticationRes res = new AuthenticationRes(token, DateUtil.formatDateTime(expireDate));

        return ResponseData.success(res);

    }

    private AppInst getAppInst(AuthenticationReq authenticationReq){
        AppInst appInst = this.sqlSession.selectOne("oauth.qryAppByOpenId", authenticationReq.getOpenId());
        if (appInst == null) {
            throw new BusiException("App does not exist");
        }
        //TODO 解密
        if (!StringUtils.equalsIgnoreCase(authenticationReq.getSecretKey(), appInst.getSecretKey())) {
            throw new BusiException("Authentication failed");
        }
        return appInst;
    }


    private void cache(AppInst appInst,String token) {
        OpenToken openToken = new OpenToken();
        //获取已授权服务
        Map<String,Object> param = new HashMap<>();
        param.put("objectId",appInst.getAppInstId());
        param.put("objectType",Constant.AuthType.APP.toString().toLowerCase());
        List<Map<String, Object>> serviceList = this.sqlSession.selectList("oauth.qryPermissionService", param);
        serviceList.forEach(s -> {
            OpenToken.Service service = new OpenToken.Service();
            service.setServiceId(MapUtils.getLong(s, "serviceId"));
            service.setName(MapUtils.getString(s, "name"));
            service.setUrl(MapUtils.getString(s, "url"));
            openToken.getServices().add(service);
        });

        openToken.setUserId(appInst.getCreateUser());
        openToken.setOpenId(appInst.getOpenId());
        //额外属性
        openToken.setAttr(new LinkedHashMap<>());

        String value = JSONObject.toJSONString(openToken, SerializerFeature.WriteNullStringAsEmpty);

        log.debug("授权信息:{}", value);
        CacheUtil.set(TOKEN_KEY_PREFIX + token, value, TOKEN_EXPIRE_TIME, TimeUnit.HOURS);

        //TODO 保存到数据库
        //TODO 当授权信息变更时 更新数据库中的token 从redis中删除旧的token 强制token失效 重新认证
    }

    private String createToken() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}
