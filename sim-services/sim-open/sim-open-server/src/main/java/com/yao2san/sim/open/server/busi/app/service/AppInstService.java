package com.yao2san.sim.open.server.busi.app.service;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.open.server.busi.app.response.AppInstRes;
import com.yao2san.sim.open.server.busi.app.request.AppInstReq;

import java.util.List;

public interface AppInstService {
    ResponseData<Void> add(AppInstReq req);
    ResponseData<List<AppInstRes>> list(AppInstReq req);
    ResponseData<Void> delete(AppInstReq req);

    ResponseData<Void> update(AppInstReq req);

    ResponseData<AppInstRes> detail(Long appInstId);

    void restSecretKey(AppInstReq req);

    AppInstRes detailByOpenId(String openId);
}
