package com.yao2san.sim.open.server.busi.auth.bean;

/**
 * @author wxg
 **/
public class Constant {
    public enum AuthType {
        APP, USER;

        public boolean equals(String name) {
            return this.name().equalsIgnoreCase(name);
        }
    }
}
