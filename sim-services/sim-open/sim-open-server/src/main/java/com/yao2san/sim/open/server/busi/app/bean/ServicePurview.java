package com.yao2san.sim.open.server.busi.app.bean;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ServicePurview extends BaseBean {
    private Long servicePurviewId;
    private Long serviceId;
    private String objectType;
    private Long objectId;
    private String expDate;
    private String effDate;
}
