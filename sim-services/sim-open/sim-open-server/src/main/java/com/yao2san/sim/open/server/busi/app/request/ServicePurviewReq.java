package com.yao2san.sim.open.server.busi.app.request;

import com.yao2san.sim.framework.web.bean.BaseBean;
import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ServicePurviewReq extends Pagination {
    private Long objectId;
    private String objectType;
    private String serviceType;
    private String serviceName;
    private List<Long> services;

    /**
     * 是否已授权 0未授权 1已授权
     */
    private String isAuth;
    /**
     * 操作类型: 1新增授权 2取消授权
     */
    private String option;
}
