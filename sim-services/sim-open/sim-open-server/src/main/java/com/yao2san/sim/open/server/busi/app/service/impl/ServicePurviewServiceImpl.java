package com.yao2san.sim.open.server.busi.app.service.impl;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.open.server.busi.app.request.ServicePurviewReq;
import com.yao2san.sim.open.server.busi.app.response.ServicePurviewRes;
import com.yao2san.sim.open.server.busi.app.service.ServicePurviewService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ServicePurviewServiceImpl extends BaseServiceImpl implements ServicePurviewService {

    @Override
    @Transactional
    public void serviceAuthorization(ServicePurviewReq req) {
        //新增授权
        if ("1".equals(req.getOption())) {
            //先取消授权
            this.sqlSession.delete("servicePurview.delServicePurview", req);
            this.sqlSession.insert("servicePurview.addServicePurview", req);
        }
        //解除授权
        else if ("2".equals(req.getOption())) {
            this.sqlSession.delete("servicePurview.delServicePurview", req);
        } else {
            throw new BusiException("不支持的服务授权查询类型(期望值:1 or 2，实际值:" + req.getOption() + ")");
        }

    }
    @Override
    public PageInfo<ServicePurviewRes> services(ServicePurviewReq req) {
        if ("1".equals(req.getIsAuth())) {
            return this.qryList("servicePurview.qryAuthServices", req);
        } else if ("0".equals(req.getIsAuth())) {
            return this.qryList("servicePurview.qryNotAuthServices", req);
        } else {
            throw new BusiException("不支持的服务授权查询类型(期望值:0 or 1，实际值:" + req.getIsAuth() + ")");
        }
    }
}
