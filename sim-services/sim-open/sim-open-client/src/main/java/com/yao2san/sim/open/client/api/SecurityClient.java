package com.yao2san.sim.open.client.api;

import com.yao2san.sim.framework.web.response.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "sim-auth-server", path = "user")
public interface SecurityClient {
    ResponseData<Void> login();
}
