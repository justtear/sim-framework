package com.yao2san.sim.framework.annotations;

import com.yao2san.sim.framework.config.SimOpenServerAutoConfig;
import com.yao2san.sim.framework.config.SimSystemServerAutoConfig;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SimOpenServerAutoConfig.class})
@EnableEurekaServer
@EnableZuulProxy
public @interface SimOpenServer {
}
