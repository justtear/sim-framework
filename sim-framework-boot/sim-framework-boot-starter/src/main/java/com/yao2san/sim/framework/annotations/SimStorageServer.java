package com.yao2san.sim.framework.annotations;

import com.yao2san.sim.framework.config.SimStorageServerAutoConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SimStorageServerAutoConfig.class})
public @interface SimStorageServer {
}
