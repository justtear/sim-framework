
package com.yao2san.sim.framework.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.yao2san.sim.web.server"})
public class SimWebServerAutoConfig {
}
