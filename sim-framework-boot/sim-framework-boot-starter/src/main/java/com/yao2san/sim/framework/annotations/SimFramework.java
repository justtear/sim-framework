package com.yao2san.sim.framework.annotations;

import com.yao2san.sim.framework.config.SimFrameworkAutoConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(SimFrameworkAutoConfig.class)
public @interface SimFramework {
}
