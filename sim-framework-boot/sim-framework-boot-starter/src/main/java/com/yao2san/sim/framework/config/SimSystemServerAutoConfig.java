
package com.yao2san.sim.framework.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.yao2san.sim.system.server"})
public class SimSystemServerAutoConfig {
}
