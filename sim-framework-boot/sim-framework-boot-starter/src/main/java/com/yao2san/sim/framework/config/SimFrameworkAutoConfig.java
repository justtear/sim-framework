package com.yao2san.sim.framework.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.yao2san.sim.framework.web",
        "com.yao2san.sim.framework.utils",
        "com.yao2san.sim.framework.log",
        "com.yao2san.sim.framework.cloud",
        "com.yao2san.sim.framework.cache"})
public class SimFrameworkAutoConfig {
}
