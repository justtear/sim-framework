package com.yao2san.sim.framework.annotations;

import com.yao2san.sim.framework.config.SimAuthServerAutoConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SimAuthServerAutoConfig.class})
public @interface SimAuthServer {
}
