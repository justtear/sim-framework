package com.yao2san.sim.framework.annotations;


import com.yao2san.sim.auth.client.config.AuthClientConfig;
import com.yao2san.sim.gateway.client.config.SimGatewayClientConfig;
import com.yao2san.sim.storage.client.config.SimStorageClientConfig;
import com.yao2san.sim.system.config.client.SimConfigClientConfig;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@EnableZuulProxy
@Import({SimGatewayClientConfig.class, AuthClientConfig.class, SimStorageClientConfig.class, SimConfigClientConfig.class})
public @interface SimService {
}
