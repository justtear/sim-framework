#!/usr/bin/env bash
LINE="-------------------------------------------------------"

print_bar(){
echo ${LINE}
COUNT=0
while  [ $# -gt 0 ]
do
    echo $1
    shift
    let COUNT=COUNT+1
done
echo ${LINE}
}

SIM_HOME=$(cd "$(dirname "$0")";pwd)
SIM_HOME="${SIM_HOME/"/bin"/}"

print_bar "start build..." "base path is: $SIM_HOME"
#dependency:purge-local-repository
mvn ${RELOAD_PACKAGES}  -U -f ${SIM_HOME}/build/package/pom.xml clean package -Dmaven.test.skip=true

echo "copy files..."

cp -r ${SIM_HOME}/bin ${SIM_HOME}/target/

print_bar "build finished,package path: $SIM_HOME/target"