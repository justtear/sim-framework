
#### sim-framework

日常积累到的东西，包含一些工具及小框架

基于SpringBoot 2.3.12，集成了大部分web开发常用的依赖、cloud环境（Spring Cloud Hoxton.SR12）以及nacos服务注册发现等服务，可基于此进行快速二次开发。

演示地址：http://47.106.129.186:8010/ （带宽有限，可能有点慢）

单体应用参见：https://gitee.com/xgpxg/sim-app

#### 系统架构

![输入图片说明](images/jg.png)

#### 如何使用

在pom.xml中添加仓库：

    <repositories>
        <repository>
            <id>rdc-releases</id>
            <url>https://61c59941e74da01affdc9d7b:Swtu1HNF1phz@packages.aliyun.com/maven/repository/2170823-release-TpgdUY/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>


按需引入依赖：

    <dependency>
      <groupId>com.yao2san</groupId>
      <artifactId>sim-framework-web</artifactId>
      <version>1.0.2</version>
    </dependency>

    <dependency>
      <groupId>com.yao2san</groupId>
      <artifactId>sim-framework-utils</artifactId>
      <version>1.0.2</version>
    </dependency>
    
    <dependency>
      <groupId>com.yao2san</groupId>
      <artifactId>sim-framework-cloud</artifactId>
      <version>1.0.2</version>
    </dependency>
    
#### 启动：

##### Docker启动

项目根目录下执行：

    docker-compose up

访问地址: http://127.0.0.1:8080

##### 本地启动

需准备环境：

- jdk 1.8+
- redis 5.0+
- mysql 5.7+

初始化脚本位于：/docker/db/init/init_db.sql

项目根目录下执行：
 
    sh ./bin/build.sh
    sh ./bin/service.sh start all
    

访问地址: http://127.0.0.1:8010

#### 目前可用内容：

#### 功能

| 功能           | 描述                                                                                                                                                                                      |
|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ☑ 模拟API    | CS模式，客户端sim-api-client扫描所有controller，注册到服务端sim-api-server，服务端进行api管理。支持模拟api的开启/关闭，自定义模拟数据，动态修改模拟数据，新增接口等功能。 <a href="https://gitee.com/xgpxg/sim-framework/tree/master/sim-api">详情</a> |
| ☑ 命令及脚本调用    | 支持本地/远程执行命令/脚本，简化调用方式。<a href="https://gitee.com/xgpxg/sim-framework/tree/master/sim-jvoke">详情</a>                                                                                      |
| ☑ 定时任务管理     | 一个很轻量的定时任务工具。  <a href="https://gitee.com/xgpxg/sim-framework/tree/master/sim-task">详情</a>                                                                                              |
| ☑ 数据库差异对比工具  | 一个数据库差异（表结构、字段等）对比小工具，支持mysql和oracle （计划重构中）                                                                                                                                            |
| ☑ redis可视化查询 | 一个简单的redis查询工具。封装了大部分redis原生命令，提供Restful的redis连接和查询。 <a href="https://gitee.com/xgpxg/sim-framework/tree/sim-framework-20200621/sim-cache/sim-cache-server">详情</a>                      |
| ☑ vue组件      | 积累的一些vue组件，持续更新中。<br/> 表格组件sim-el-table-plus：https://www.npmjs.com/package/sim-el-table-plus <br/> 搜素组件sim-el-search：https://www.npmjs.com/package/sim-el-search                        | 
| ☑ API文档      | 基于SpringFox3.0的接口文档工具。<a href="https://gitee.com/xgpxg/sim-framework/tree/sim-framework-1.0.2/sim-components/sim-api-v2">详情</a>                                                         |
| ☑ 存储客户端      | 对象存储集成，统一上传/下载接口，让文件管理变得更简单。支持本地、MINIO、阿里OSS、腾讯COS等多种对象存储。<a href="https://gitee.com/xgpxg/sim-framework/tree/sim-framework-1.0.2/sim-services/sim-storage/sim-storage-client">详情</a>   |




------

#### 各模块说明：

- sim-framework-base: 基础依赖

- sim-framework-web: web基础依赖

- sim-framework-utils: 通用工具包

- sim-framework-cloud: cloud环境支持（集成naocs）

- sim-flink:

  - flink-nacos-support: flink集成nacos

- sim-cache:

  - sim-cache-server：redis可视化查询的服务端

- sim-web：web页面集成

- sim-gateway: 网关模块，正在开发中...，基于Zuul，已实现动态路由、动态限流、及灰度路由等功能

  - sim-gateway-server: 网关服务端
  
  - sim-gateway-client: 网关客户端

- sim-security: 安全模块，包含用户认证、用户管理等功能

  - sim-auth-server: 认证服务端，提供用户登录和认证，以及菜单、角色、权限、组织机构管理等功能

  - sim-auth-client: 认证客户端，用于应用集成，获取用户信息

----- 


部分功能截图：

![输入图片说明](images/1.png.png)

![输入图片说明](images/3.png)

![输入图片说明](images/API%E6%96%87%E6%A1%A31.png)
（API文档工具，一个新轮子，但是更好的轮子，正在开发中，基于SpringFox3.0，增加API版本管理，应用管理，以及自定义模板导出API文档等功能 => [详情](https://gitee.com/xgpxg/sim-framework/tree/sim-framework-1.0.2/sim-components/sim-api-v2)）

![输入图片说明](images/storage-app.png)
集成存储：统一上传、下载接口，让文件管理变得更简单，支持本地存储、阿里OSS、腾讯COS和MinIO

![QQ群](images/2.png.png)

如有疑问，欢迎扫一扫加群哦~

