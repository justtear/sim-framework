package com.yao2san.sim.api.server.busi.register.bean;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Objects;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ApiSchema extends BaseBean {
    private Long apiSchemaId;
    private Long apiAppId;
    private String name;
    private String title;
    private String path;
    private String description;
    private String content;

    public boolean equalsValue(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ApiSchema)) {
            return false;
        }
        ApiSchema apiSchema = (ApiSchema) obj;

        int h1 = Objects.hash(name, title, path, description, content);
        int h2 = Objects.hash(apiSchema.name, apiSchema.title, apiSchema.name, apiSchema.path, apiSchema.description, apiSchema.content);
        return h1 == h2;
    }
}
