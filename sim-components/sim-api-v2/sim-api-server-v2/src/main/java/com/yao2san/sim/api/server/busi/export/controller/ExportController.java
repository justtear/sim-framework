package com.yao2san.sim.api.server.busi.export.controller;

import com.yao2san.sim.api.server.busi.export.service.ExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wxg
 **/
@RestController
public class ExportController {
    @Autowired
    private ExportService exportService;
    @GetMapping("export")
    public void export(Long apiAppId,String exportType) {
        exportService.export(apiAppId,exportType);
    }
}
