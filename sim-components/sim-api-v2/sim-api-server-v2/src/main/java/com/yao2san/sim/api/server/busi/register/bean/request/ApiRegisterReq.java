package com.yao2san.sim.api.server.busi.register.bean.request;

import lombok.Data;
import springfox.documentation.service.Documentation;

import java.util.Map;

/**
 * @author wxg
 **/
@Data
public class ApiRegisterReq {
    private Map<String, Documentation> documents;
    //private Map<String, String> versions;
}
