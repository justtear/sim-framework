package com.yao2san.sim.api.server.busi.export.service;

/**
 * @author wxg
 **/
public interface ExportService {
    void export(Long apiAppId,String exportType);
}
