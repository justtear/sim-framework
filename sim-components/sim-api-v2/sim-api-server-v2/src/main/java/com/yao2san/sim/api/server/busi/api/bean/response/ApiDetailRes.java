package com.yao2san.sim.api.server.busi.api.bean.response;


import com.alibaba.fastjson.JSONObject;
import com.yao2san.sim.api.server.busi.register.bean.ApiApp;
import com.yao2san.sim.api.server.busi.register.bean.ApiInfo;
import com.yao2san.sim.api.server.busi.register.bean.ApiSchema;
import com.yao2san.sim.api.server.busi.register.bean.ApiServer;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponses;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * @author wxg
 **/
@Data
public class ApiDetailRes {
    private ApiInfo apiInfo;
    private ApiApp apiApp;
    private List<ApiServer> apiServers;
    private List<ApiSchema> apiSchemas;
    private Original original;

    @Data
    public static class Original {
        private List<JSONObject> parameters;
        private JSONObject requestBody;
        private JSONObject responses;
        private List<JSONObject> schemas;
    }
  /*  @Data
    public static class Parsed {
        private List<Parameter> parameters;
        private RequestBody requestBody;
        private ApiResponses responses;
        private List<Schema> schemas;

        public static class Parameter{

        }
        public static class RequestBody{

        }
        public static class ApiResponses{

        }
    }*/

}
