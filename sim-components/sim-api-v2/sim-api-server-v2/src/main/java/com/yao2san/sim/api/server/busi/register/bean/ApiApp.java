package com.yao2san.sim.api.server.busi.register.bean;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ApiApp  extends BaseBean{
    private Long apiAppId;

    private String name;

    private String code;

    private String description;

    private String openapiJson;
}
