package com.yao2san.sim.api.server.busi.api.bean.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ApiInfoListReq extends Pagination {
    private Long apiAppId;
    private String tag;
    private String filterText;
    private String version;
}
