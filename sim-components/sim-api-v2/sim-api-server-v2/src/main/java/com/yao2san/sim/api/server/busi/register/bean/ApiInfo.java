package com.yao2san.sim.api.server.busi.register.bean;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Objects;

@Data
@EqualsAndHashCode(callSuper = true)
public class ApiInfo extends BaseBean {

    private Long apiId;

    private Long apiAppId;

    private String operationId;

    private String httpMethod;

    private String name;

    private String path;

    private String tag;

    private String summary;

    private String description;

    private String requestBody;

    private String response;

    private String parameter;

    private String extensions;

    private String version;

    public boolean equalsValue(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ApiInfo)) {
            return false;
        }
        ApiInfo apiInfo = (ApiInfo) obj;

        int h1 = Objects.hash(operationId, httpMethod, name, path, summary, description, requestBody, response, parameter, extensions,version);
        int h2 = Objects.hash(apiInfo.operationId, apiInfo.httpMethod, apiInfo.name, apiInfo.path, apiInfo.summary, apiInfo.description, apiInfo.requestBody, apiInfo.response, apiInfo.parameter, apiInfo.extensions,apiInfo.version);
        return h1==h2;

    }

}
