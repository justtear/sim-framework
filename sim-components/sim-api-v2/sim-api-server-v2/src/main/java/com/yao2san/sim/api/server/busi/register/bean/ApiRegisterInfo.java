package com.yao2san.sim.api.server.busi.register.bean;

import lombok.Data;

import java.util.List;

@Data
public class ApiRegisterInfo {
    private ApiApp apiApp;

    private List<ApiServer> apiServers;

}
