# API文档组件

## 简介

基于SpringFox3.0，自动注册API信息到服务端，可查看API文档、调试等。

## 功能

| 功能         | 描述                  |
|------------|---------------------|
| ☑ API注册    | 注册API到服务端           |
| ☑ API统一管理  | 由服务端统一管理API         |
| ☑ API文档    | API文档查看             |
| ☑ 泛型参数推断   | 生成的API文档中可展示具体的泛型类型 |
| ☑ API版本管理  | 支持同一API多版本共存        |
| ☑ API状态管理  | 支持修改API状态管理         |
| ☑ API调试    | 调试接口                |
| ☐ 调试环境切换   | 支持应用调试环境切换          |
| ☐ 全局参数配置   | 配置全局参数，如Header等     |
| ☑ 导出word文档 | 导出word文档（参考自：https://github.com/JMCuixy/swagger2word ）            |

## Maven依赖


```
   <dependency>
        <groupId>com.yao2san</groupId>
        <artifactId>sim-api-client-v2</artifactId>
        <version>1.0.2</version>
   </dependency>

```


## 示例配置


```
sim:
  api:
    client:
      #要扫描的接口所在包
      base-package: com.yao2san
      #项目地址
      url: "http://127.0.0.1:8080"
      #项目名称
      name: xxx项目
      #项目分组(唯一)
      group: ${spring.application.name}
      description: xxx项目
      #多环境
      servers:
        - url: "http://127.0.0.1:8080"
          description: "开发环境"
        - url: "http://192.168.1.101:8081"
          description: "测试环境"
    #接口注册地址
    server:
      url: "http://127.0.0.1:8081/registry"

```


## 部分功能截图

![输入图片说明](../../images/api-1.png)

![输入图片说明](../../api-2.png)

![输入图片说明](../../images/api-3.png)

![输入图片说明](../../images/api-4.png)