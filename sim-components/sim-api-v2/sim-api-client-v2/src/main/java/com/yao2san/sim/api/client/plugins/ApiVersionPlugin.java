package com.yao2san.sim.api.client.plugins;

import com.yao2san.sim.api.client.annotation.ApiVersion;
import com.yao2san.sim.api.client.config.SimApiClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.StringVendorExtension;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.ParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author wxg
 **/
@Configuration
public class ApiVersionPlugin implements OperationBuilderPlugin {
    private final static String KEY = "version";

    @Autowired
    private SimApiClientProperties clientProperties;

    @Override
    public void apply(OperationContext context) {
        Optional<ApiVersion> optional = context.findAnnotation(ApiVersion.class);
        boolean present = optional.isPresent();
        String version;

        if (present) {
            ApiVersion apiVersion = optional.get();
            version = apiVersion.value();
        } else {
            version = clientProperties.getVersion();
        }
        if (version != null) {
            List<VendorExtension> extensions = new ArrayList<>();
            StringVendorExtension extension = new StringVendorExtension(KEY, version);
            extensions.add(extension);
            context.operationBuilder().extensions(extensions);
        }
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}
