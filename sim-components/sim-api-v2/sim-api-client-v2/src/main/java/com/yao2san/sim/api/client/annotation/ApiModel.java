package com.yao2san.sim.api.client.annotation;

public @interface ApiModel {
    String value() default "";

    String description() default "";

    Class<?> parent() default Void.class;

    Class<?>[] subTypes() default {};

    String reference() default "";
}
