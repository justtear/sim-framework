package com.yao2san.sim.api.client.annotation;


import com.yao2san.sim.api.client.config.SimApiClientAutoConfiguration;
import com.yao2san.sim.api.client.config.SimApiClientProperties;
import com.yao2san.sim.api.client.config.SimApiServerAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({SimApiClientAutoConfiguration.class, SimApiServerAutoConfiguration.class})
public @interface EnabledSimApiClient {
    boolean enabled() default true;

    String[] scanBasePackages() default {};
}
