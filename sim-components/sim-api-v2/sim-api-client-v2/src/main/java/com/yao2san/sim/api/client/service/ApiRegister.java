package com.yao2san.sim.api.client.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yao2san.sim.api.client.annotation.EnabledSimApiClient;
import com.yao2san.sim.api.client.bean.ApiApp;
import com.yao2san.sim.api.client.bean.ApiRegisterInfo;
import com.yao2san.sim.api.client.bean.ApiServer;
import com.yao2san.sim.api.client.config.SimApiClientProperties;
import com.yao2san.sim.api.client.config.SimApiServerProperties;
import com.yao2san.sim.api.client.utils.ClassUtil;
import com.yao2san.sim.framework.utils.IpUtil;
import com.yao2san.sim.framework.utils.RequestUtil;
import com.yao2san.sim.framework.web.response.ResponseCode;
import com.yao2san.sim.framework.web.response.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.oas.mappers.ServiceModelToOpenApiMapper;
import springfox.documentation.oas.web.SpecGeneration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wxg
 **/
@Configuration
@Slf4j
public class ApiRegister implements ApplicationRunner {
    @Autowired
    private SimApiServerProperties server;
    @Autowired
    private SimApiClientProperties client;

    @Autowired
    private ServiceModelToOpenApiMapper mapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${server.servlet.context-path:}")
    private String contentPath;
    @Value("${server.port:8080}")
    private Integer port;
    @Value(SpecGeneration.OPEN_API_SPECIFICATION_PATH)
    private String openApiSpecificationPath;

    private static final String LOCAL_HOST = "http://127.0.0.1";

    @Override
    public void run(ApplicationArguments args) {
        EnabledSimApiClient annotation = ClassUtil.getMainClass().getAnnotation(EnabledSimApiClient.class);
        if (annotation != null) {
            init();
        }
    }

    public void init() {
        registry();
    }

    @SuppressWarnings("all")
    private void registry() {
        String openApi = getOpenApi(client.getGroup());

        ApiApp apiApp = new ApiApp(client.getName(), client.getGroup(), client.getDescription(), openApi);
        List<ApiServer> apiServers = getApiServers();

        ApiRegisterInfo info = new ApiRegisterInfo();
        info.setApiApp(apiApp);
        info.setApiServers(apiServers);


        String serviceUrl = server.getUrl();
        ResponseData<String> res = RequestUtil.postForObject(serviceUrl, info, ResponseData.class);

        if (res.getCode() == ResponseCode.SUCCESS.getCode()) {
            log.info(res.getData());
        } else {
            log.error(res.getMsg());
        }
    }

    private String getOpenApi(String group) {
        String path = LOCAL_HOST + ":" + port + this.contentPath + openApiSpecificationPath + "?group=" + group;
        return RequestUtil.getForObject(path, String.class);
    }

    private List<ApiServer> getApiServers() {

        ApiServer defaultServer = new ApiServer();


        defaultServer.setUrl(client.getUrl());
        defaultServer.setDescription(client.getDescription());
        defaultServer.setIsDefault(true);

        if (client.getUrl() == null) {
            defaultServer.setUrl("http://" + IpUtil.getLocalHost() + ":" + port + this.contentPath);
        }
        List<ApiServer> servers = new ArrayList<>();
        servers.add(defaultServer);
        List<ApiServer> clientApiServers = client.getServers();
        if (clientApiServers != null) {
            servers.addAll(clientApiServers);
        }

        return servers;

    }

}
