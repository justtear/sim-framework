package com.yao2san.sim.api.client.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ApiModelProperty {
    String value() default "";

    String name() default "";

    String allowableValues() default "";

    String description() default "";

    boolean required() default false;

    int sort() default 0;

    boolean hidden() default false;

    String example() default "";

    String defaultValue() default "";

}
