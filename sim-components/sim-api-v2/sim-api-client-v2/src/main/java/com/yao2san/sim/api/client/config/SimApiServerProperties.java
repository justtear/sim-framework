package com.yao2san.sim.api.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wxg
 **/
@ConfigurationProperties(prefix = "sim.api.server")
@Configuration
@Data
public class SimApiServerProperties {
    private String url;
}
