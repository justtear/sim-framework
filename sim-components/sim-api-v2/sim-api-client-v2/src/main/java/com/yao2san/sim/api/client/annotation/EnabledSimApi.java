package com.yao2san.sim.api.client.annotation;

import com.yao2san.sim.api.client.config.SimApiServerAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * @author wxg
 **/
@Import(SimApiServerAutoConfiguration.class)
public @interface EnabledSimApi {
}
