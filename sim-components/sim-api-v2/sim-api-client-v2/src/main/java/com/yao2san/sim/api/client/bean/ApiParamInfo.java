package com.yao2san.sim.api.client.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

/**
 * @author wxg
 */
@Data
@Accessors(chain = true)
@Deprecated
public class ApiParamInfo {
    /**
     * 参数名称
     */
    private String name;
    /**
     * 参数位置: header | query | body | path
     */
    private String position;
    /**
     * 参数类型：request请求参数、response响应参数
     */
    private String paramType;
    /**
     * 数据类型
     */
    private String dataType;
    /**
     * 泛型数据类型
     */
    private String genericsDataType;
    /**
     * 是否必须
     */
    private boolean required;

    private String defaultValue;

    private String example;

    private String description;

    private Boolean hidden;

    private Set<ApiParamInfo> params;

    private transient ApiParamInfo last;

    private transient Class<?> clazz;

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
