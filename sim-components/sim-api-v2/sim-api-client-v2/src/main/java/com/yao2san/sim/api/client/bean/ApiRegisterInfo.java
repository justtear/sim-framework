package com.yao2san.sim.api.client.bean;

import lombok.Data;

import java.util.List;

@Data
public class ApiRegisterInfo {
    private ApiApp apiApp;

    private List<ApiServer> apiServers;

}
