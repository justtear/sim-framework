package com.yao2san.sim.im.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class CopyThread extends Thread {

    private static StringRedisTemplate redisTemplate;

    @Autowired
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        CopyThread.redisTemplate = redisTemplate;
    }

    @Override
    public void run() {
        log.info("redis队列复制线程启动");
        while (true) {
            ListOperations<String, String> ops = redisTemplate.opsForList();
            ops.rightPopAndLeftPush("messages", "messages_" + Objects.hash(Thread.currentThread().getName()),30, TimeUnit.SECONDS);
        }
    }

}
