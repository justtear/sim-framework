package com.yao2san.sim.im.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;


@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@Slf4j
@EnableWebSocket
public class SimImClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimImClientApplication.class, args);
        log.info("sim-storage-server start success!");

        start();
    }

    public static void start(){
        new CopyThread().start();
        new CopyThread().start();
        new CopyThread().start();
    }
}
