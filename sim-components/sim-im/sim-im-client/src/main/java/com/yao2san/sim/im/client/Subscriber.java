package com.yao2san.sim.im.client;

public interface Subscriber {
    void handleMessage(String message);

    String getTopic();
}
