#!/usr/bin/env bash
echo "Starting sim-database..."
HOME=$(dirname $0)
APP_NAME=sim-database
VERSION=1.0.2
java -jar ${HOME}/target/${APP_NAME}-${VERSION}.jar > log.log 2>&1 &
echo
