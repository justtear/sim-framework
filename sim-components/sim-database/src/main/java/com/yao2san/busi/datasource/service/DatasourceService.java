package com.yao2san.busi.datasource.service;

import com.yao2san.busi.datasource.bean.request.DatasourceReq;
import com.yao2san.sim.framework.web.response.ResponseData;

public interface DatasourceService {
    ResponseData add(DatasourceReq req);
    ResponseData list(DatasourceReq req);
    ResponseData qryDatasourceAttrCode(String datasourceType);
}
