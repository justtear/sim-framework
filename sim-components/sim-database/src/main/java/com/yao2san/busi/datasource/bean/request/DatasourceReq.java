package com.yao2san.busi.datasource.bean.request;

import com.yao2san.busi.datasource.bean.entity.Datasource;
import com.yao2san.busi.datasource.bean.entity.DatasourceAttr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class DatasourceReq extends Datasource {
    @NotNull
    private List<DatasourceAttr> attrs;
}
