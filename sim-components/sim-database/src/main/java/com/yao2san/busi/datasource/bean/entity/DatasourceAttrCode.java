package com.yao2san.busi.datasource.bean.entity;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class DatasourceAttrCode  extends BaseBean {
    private Long datasourceAttrCodeId;
    private String datasourceType;
    private String attrCode;
    private String attrName;
    private String attrValueType;
    private String attrValueCode;
    private String isRequired;
}
