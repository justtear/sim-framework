package com.yao2san.busi.datasource.service.impl;

import com.github.pagehelper.PageInfo;
import com.yao2san.busi.datasource.bean.entity.DatasourceAttrCode;
import com.yao2san.busi.datasource.bean.request.DatasourceReq;
import com.yao2san.busi.datasource.bean.response.DatasourceRes;
import com.yao2san.busi.datasource.service.DatasourceService;
import com.yao2san.sim.auth.common.UserUtil;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DatasourceServiceImpl extends BaseServiceImpl implements DatasourceService {
    @Override
    @Transactional
    public ResponseData add(DatasourceReq req) {
        req.setCreateUser(UserUtil.getCurrUserId());

        this.sqlSession.insert("datasource.addDatasource", req);
        req.getAttrs().forEach(item -> {
            item.setDatasourceId(req.getDatasourceId());
            this.sqlSession.insert("datasource.addDatasourceAttr", item);
        });

        return ResponseData.success(null, "新增成功");
    }

    @Override
    public ResponseData list(DatasourceReq req) {
        PageInfo<DatasourceRes> list = this.qryList("datasource.qryDatasourceList", req);
        return ResponseData.success(list);
    }

    @Override
    public ResponseData qryDatasourceAttrCode(String datasourceType) {
        List<DatasourceAttrCode> list = this.sqlSession.selectList("datasource.qryDatasourceAttrCode", datasourceType);
        return ResponseData.success(list);
    }
}
